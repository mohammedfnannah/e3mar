-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 08, 2018 at 01:18 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e3mar`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `facility_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`id`, `user_id`, `service_id`, `city_id`, `facility_id`, `address`, `text`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 1, 2, NULL, 'غزة - الشجاعية - شارع النزاز', 'طلب اصلاح الغسالة', 1, '2018-02-07 17:48:09', '2018-02-07 17:48:09', NULL),
(2, 4, 1, 2, NULL, 'غزة - الرمال', 'طلب اصلاح مدفأة', 1, '2018-02-07 18:05:15', '2018-02-07 18:11:05', NULL),
(3, 4, 1, 2, NULL, 'الصبرة', 'طلب اصلاح لابتوب', 2, '2018-02-07 18:06:02', '2018-02-07 18:06:02', NULL),
(4, 4, 1, 2, NULL, 'التفاح', 'طلب تركيب شبابيك', 0, '2018-02-07 18:38:42', '2018-02-07 18:38:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `name`, `image`, `link`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'البنر الأول', '1516722092.png', '', 1, '2018-01-23 11:18:05', '2018-01-23 13:41:49', NULL),
(2, 'البنر الثاني', '1516722122.png', '', 1, '2018-01-23 13:42:02', '2018-01-23 13:42:02', NULL),
(3, 'البنر الثالث', '1516722135.png', '', 1, '2018-01-23 13:42:15', '2018-01-23 13:42:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'الرياض', 1, '2018-02-03 20:41:42', NULL, NULL),
(2, 'مكة المكرمة', 1, '2018-02-03 20:41:42', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `contract_id`, `text`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'شرط ١', '2018-02-07 21:10:26', '2018-02-07 21:10:26', NULL),
(2, 1, 'شرط ٢', '2018-02-07 21:10:26', '2018-02-07 21:10:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `consultation`
--

CREATE TABLE `consultation` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_id` int(11) DEFAULT NULL,
  `question` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consultation`
--

INSERT INTO `consultation` (`id`, `user_id`, `name`, `email`, `mobile`, `facility_id`, `question`, `answer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, NULL, NULL, NULL, NULL, 'تجربة', NULL, '2018-02-04 19:12:34', '2018-02-04 19:12:34', NULL),
(2, NULL, 'محمد فنانه', 'abood@gmail.com', '٠٥٩٧٢٣٣٦٢٠', NULL, 'تجربة مستشارك', NULL, '2018-02-04 19:13:40', '2018-02-04 19:13:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`id`, `name`, `email`, `mobile`, `subject`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'محمد علي فنانه', 'mafnannah@gmail.com', '0597233620', 'رسالة تجريبية', 'نص الرسالة نص الرسالة نص الرسالة نص الرسالة نص الرسالة نص الرسالة نص الرسالة نص الرسالة نص الرسالة نص الرسالة ', '2018-01-21 20:59:13', '2018-01-21 19:04:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE `contracts` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '2',
  `facility_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work` longtext COLLATE utf8mb4_unicode_ci,
  `city_id` int(11) NOT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contracts`
--

INSERT INTO `contracts` (`id`, `type`, `facility_id`, `user_id`, `name`, `email`, `mobile`, `identification`, `work`, `city_id`, `district`, `location`, `start_date`, `end_date`, `employee_name`, `employee_mobile`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 10, 4, 'محمد علي فنانه', 'mfnannah@gmail.com', '0597233620', '١٢٣٤', 'طلب تركيب شبابيك', 2, 'الشجاعية', 'النزاز', '١٠-١٠-٢٠١٤', '١٢-١٠-٢٠١٤', 'احمد سليمان', '٠٠٩٦٦', '2018-02-07 21:10:26', '2018-02-07 21:10:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_work`
--

CREATE TABLE `evaluation_work` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `facility_id` int(11) NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `evaluation_work`
--

INSERT INTO `evaluation_work` (`id`, `user_id`, `name`, `email`, `mobile`, `city_id`, `facility_id`, `text`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, NULL, NULL, NULL, 2, 1, 'الرجاء تقيم المنشأة في برج الصلاح', '2018-02-04 19:47:52', '2018-02-04 19:47:52', NULL),
(2, NULL, 'محمد فنانه', 'abood@gmail.com', '0593333333', 2, 1, 'تست تست تست', '2018-02-04 19:48:46', '2018-02-04 19:48:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `commercial_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_tel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_box` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `projects` text COLLATE utf8mb4_unicode_ci,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_day` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_calender_work` int(1) NOT NULL DEFAULT '0',
  `is_consulting` int(1) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `name`, `sub_title`, `image`, `user_id`, `city_id`, `service_id`, `commercial_number`, `mobile`, `other_mobile`, `tel`, `other_tel`, `fax`, `postal_code`, `mail_box`, `district`, `street`, `whatsapp`, `twitter`, `instagram`, `projects`, `location`, `work_time`, `work_day`, `is_calender_work`, `is_consulting`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'المنشأة الأولى التجريبية', 'منتج تجريبي', '1518020660.png', 10, 2, 1, '١٢٣٤٥٦٧', '٠٥٩٧٢٣٣٦٢٠', '', '2803864', '', '', '9722', 'gaza', 'الشجاعية', 'النزاز', '00972597233620', '1992Mohf', 'mfnannah', 'مشروع اعمار', 'الرمال', '١٠ صباحا حتى ٤ مساءا', '٨ ساعات', 1, 0, 1, '2018-02-03 19:19:06', '2018-02-03 19:19:06', NULL),
(2, 'مدرسة الملك عبدالعزيز للصناعة', 'منشأة تجارية', '1518020660.png', 11, 1, 1, '١٢٣٥٤٣٣٥', '0593333333', '', '28000000000', '', '', '9722', 'gaza', 'الشجاعية', 'النزاز', '09665993333333', 'twitter', 'ali', 'تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة تجربة ', 'الرمال', '١٠ صباحا حتى ٤ مساءا', '٨ ساعات', 1, 1, 1, '2018-02-07 14:24:20', '2018-02-07 14:24:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mail_list`
--

CREATE TABLE `mail_list` (
  `id` int(11) NOT NULL,
  `email` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mail_list`
--

INSERT INTO `mail_list` (`id`, `email`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'mafnannah@gmail.com', NULL, '2018-02-02 17:47:17', '2018-02-02 17:55:08', '2018-02-02 17:55:08'),
(2, 'mohf_1992@hotmail.com', NULL, '2018-02-02 17:52:23', '2018-02-02 17:52:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_01_10_182029_entrust_setup_tables', 2);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `facility_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `text`, `image`, `status`, `facility_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'عنوان الخبر يوضع هنا ويكون بولد', '<p>\r\n\r\nتفاصيل الخبر \r\n\r\n\r\n\r\nتفاصيل الخبر \r\n\r\n\r\n\r\nتفاصيل الخبر \r\n\r\n\r\n\r\nتفاصيل الخبر \r\n\r\n\r\n\r\nتفاصيل الخبر \r\n\r\n\r\n\r\nتفاصيل الخبر \r\n\r\n\r\n\r\nتفاصيل الخبر \r\n\r\n\r\n\r\nتفاصيل الخبر \r\n\r\n\r\n\r\nتفاصيل الخبر \r\n\r\n\r\n\r\nتفاصيل الخبر \r\n\r\n\r\n\r\nتفاصيل الخبر \r\n\r\n</p>', '1517497068.png', 1, NULL, '2018-02-01 12:57:48', '2018-02-01 15:07:27', NULL),
(2, 'خبر ثاني يتم عرضه', '', '1517498678.png', 1, NULL, '2018-02-01 13:23:33', '2018-02-01 15:24:38', NULL),
(3, 'خبر ثاني يتم عرضه', '', '1517498647.png', 1, NULL, '2018-02-01 13:23:33', '2018-02-01 15:24:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `image`, `text`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'من نحن', '1517496077.png', '<p><b>\r\n\r\n<p></p></b>\r\n\r\n<p>ست شركة اعمار للتجارة و المقاولات عام 1984 م ، و بدأت تنفيذ خطط عملها بخطوات ثابته تتقدم و تتقدم بثبات حتى استطاعت المنافسة على المشاريع الكبيرة و ذلك بفضل الله ثم بفضل إدارتها الحكيمة و المتطورة و الجهاز الفني المتكامل و الكادر الوظيفي المؤهل و القدرة المالية الثابته.</p><p>و تحتل الشركة الآن و بفضل من الله مكانة مرموقة بين شركات المقاولات الوطنية و قد قامت بتنفيذ بعض المشاريع العامة في القطاع الحكومي و الخاص في مجال (المباني) و قد تم ذلك بكفائة عالية و إمكانيات قوية لتكون خطا بيانيا صاعدا من الثقة اللامحدودة بعد تحقيق هذه النجاحات الممتدة منذ تأسيس الشركة.</p>\r\n\r\n<b><p></p><u><p><br></p>\r\n\r\n</u></b></p>', 1, '2018-02-01 12:24:53', '2018-02-01 12:46:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `discount` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name`, `product`, `image`, `text`, `discount`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'شركة جوتن', 'دهانات', '1517607860.png', '<p>خصم على منتجات الدهانات بنسبة 15 %</p>', 15, 1, '2018-02-02 19:44:20', '2018-02-02 19:44:20', NULL),
(2, 'شركة المقاولات العربية', 'أبواب المنازل', '1517607926.png', '', 24, 1, '2018-02-02 19:45:26', '2018-02-02 19:45:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('mafnannah@gmail.com', '52fd47cbdeb6a10fa6e6615e21b3e52a3a9479dbada8cae1e03d9918723b71ce', '2018-01-10 17:32:07');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'permissions-update', 'تعديل صلاحية', '', '2018-01-14 11:14:14', '2018-01-14 15:43:35'),
(2, 'roles', 'المهام', '', '2018-01-14 11:57:14', '2018-01-14 15:44:22'),
(3, 'roles-data', 'بيانات المهام', '', '2018-01-14 11:57:14', '2018-01-14 15:46:51'),
(4, 'permissions', 'الصلاحيات', '', '2018-01-14 11:58:47', '2018-01-14 15:29:56'),
(5, 'permissions-data', 'بيانات الصلاحيات', '', '2018-01-14 11:58:47', '2018-01-14 16:08:33'),
(6, 'permissions-routes-update', 'تحديث الروابط', '', '2018-01-14 16:05:42', '2018-01-14 16:08:54'),
(7, 'permissions-show', 'عرض الصلاحية', '', '2018-01-14 16:05:42', '2018-01-14 16:08:44'),
(17, 'roles-add', NULL, NULL, '2018-01-15 17:31:03', '2018-01-15 17:31:03'),
(18, 'roles-check-name', NULL, NULL, '2018-01-15 17:31:03', '2018-01-15 17:31:03'),
(19, 'roles-delete', NULL, NULL, '2018-01-15 17:31:03', '2018-01-15 17:31:03'),
(20, 'roles-show', NULL, NULL, '2018-01-15 17:31:03', '2018-01-15 17:31:03'),
(21, 'roles-update', NULL, NULL, '2018-01-15 17:31:03', '2018-01-15 17:31:03'),
(22, 'administrators', NULL, NULL, '2018-01-15 17:32:12', '2018-01-15 17:32:12'),
(23, 'administrators-add', NULL, NULL, '2018-01-15 17:32:12', '2018-01-15 17:32:12'),
(24, 'administrators-change_status', NULL, NULL, '2018-01-15 17:32:12', '2018-01-15 17:32:12'),
(25, 'administrators-check-email', NULL, NULL, '2018-01-15 17:32:12', '2018-01-15 17:32:12'),
(26, 'administrators-check-name', NULL, NULL, '2018-01-15 17:32:12', '2018-01-15 17:32:12'),
(27, 'administrators-data', NULL, NULL, '2018-01-15 17:32:12', '2018-01-15 17:32:12'),
(28, 'administrators-delete', NULL, NULL, '2018-01-15 17:32:12', '2018-01-15 17:32:12'),
(29, 'administrators-show', NULL, NULL, '2018-01-15 17:32:12', '2018-01-15 17:32:12'),
(30, 'administrators-update', NULL, NULL, '2018-01-15 17:32:12', '2018-01-15 17:32:12'),
(31, 'contact', NULL, NULL, '2018-01-21 19:36:21', '2018-01-21 19:36:21'),
(32, 'contact-data', NULL, NULL, '2018-01-21 19:36:21', '2018-01-21 19:36:21'),
(33, 'contact-delete', NULL, NULL, '2018-01-21 19:36:21', '2018-01-21 19:36:21'),
(34, 'contact-show', NULL, NULL, '2018-01-21 19:36:21', '2018-01-21 19:36:21'),
(35, 'services', NULL, NULL, '2018-01-21 19:50:50', '2018-01-21 19:50:50'),
(36, 'services-add', NULL, NULL, '2018-01-21 19:50:50', '2018-01-21 19:50:50'),
(37, 'services-data', NULL, NULL, '2018-01-21 19:50:50', '2018-01-21 19:50:50'),
(38, 'services-delete', NULL, NULL, '2018-01-21 19:50:50', '2018-01-21 19:50:50'),
(39, 'services-show', NULL, NULL, '2018-01-21 19:50:50', '2018-01-21 19:50:50'),
(40, 'services-update', NULL, NULL, '2018-01-21 19:50:50', '2018-01-21 19:50:50'),
(41, 'banners', NULL, NULL, '2018-01-23 11:14:50', '2018-01-23 11:14:50'),
(42, 'banners-add', NULL, NULL, '2018-01-23 11:14:51', '2018-01-23 11:14:51'),
(43, 'banners-change_status', NULL, NULL, '2018-01-23 11:14:51', '2018-01-23 11:14:51'),
(44, 'banners-data', NULL, NULL, '2018-01-23 11:14:51', '2018-01-23 11:14:51'),
(45, 'banners-delete', NULL, NULL, '2018-01-23 11:14:52', '2018-01-23 11:14:52'),
(46, 'banners-show', NULL, NULL, '2018-01-23 11:14:52', '2018-01-23 11:14:52'),
(47, 'banners-update', NULL, NULL, '2018-01-23 11:14:53', '2018-01-23 11:14:53'),
(48, 'services-change_status', NULL, NULL, '2018-01-23 11:14:54', '2018-01-23 11:14:54'),
(49, 'services-check-name', NULL, NULL, '2018-01-23 11:14:54', '2018-01-23 11:14:54'),
(50, 'settings', NULL, NULL, '2018-01-23 12:19:33', '2018-01-23 12:19:33'),
(51, 'settings-save', NULL, NULL, '2018-01-23 12:19:34', '2018-01-23 12:19:34'),
(52, 'pages', NULL, NULL, '2018-02-01 12:00:18', '2018-02-01 12:00:18'),
(53, 'pages-add', NULL, NULL, '2018-02-01 12:00:19', '2018-02-01 12:00:19'),
(54, 'pages-change_status', NULL, NULL, '2018-02-01 12:00:20', '2018-02-01 12:00:20'),
(55, 'pages-data', NULL, NULL, '2018-02-01 12:00:20', '2018-02-01 12:00:20'),
(56, 'pages-delete', NULL, NULL, '2018-02-01 12:00:21', '2018-02-01 12:00:21'),
(57, 'pages-show', NULL, NULL, '2018-02-01 12:00:21', '2018-02-01 12:00:21'),
(58, 'pages-update', NULL, NULL, '2018-02-01 12:00:22', '2018-02-01 12:00:22'),
(59, 'news', NULL, NULL, '2018-02-01 12:56:37', '2018-02-01 12:56:37'),
(60, 'news-add', NULL, NULL, '2018-02-01 12:56:37', '2018-02-01 12:56:37'),
(61, 'news-change_status', NULL, NULL, '2018-02-01 12:56:37', '2018-02-01 12:56:37'),
(62, 'news-data', NULL, NULL, '2018-02-01 12:56:37', '2018-02-01 12:56:37'),
(63, 'news-delete', NULL, NULL, '2018-02-01 12:56:37', '2018-02-01 12:56:37'),
(64, 'news-show', NULL, NULL, '2018-02-01 12:56:38', '2018-02-01 12:56:38'),
(65, 'news-update', NULL, NULL, '2018-02-01 12:56:38', '2018-02-01 12:56:38');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 3),
(26, 3),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(33, 3),
(34, 3),
(35, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(40, 3),
(41, 3),
(42, 3),
(43, 3),
(44, 3),
(45, 3),
(46, 3),
(47, 3),
(48, 3),
(49, 3),
(50, 3),
(51, 3),
(52, 3),
(53, 3),
(54, 3),
(55, 3),
(56, 3),
(57, 3),
(58, 3),
(59, 3),
(60, 3),
(61, 3),
(62, 3),
(63, 3),
(64, 3),
(65, 3);

-- --------------------------------------------------------

--
-- Table structure for table `polls`
--

CREATE TABLE `polls` (
  `id` int(11) NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `polls`
--

INSERT INTO `polls` (`id`, `question`, `answer_1`, `answer_2`, `answer_3`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ما رأيك بالتطبيق الجديد لشركة إعمار ؟', 'مقبول', 'جيد', 'ممتاز', 1, '2018-02-02 20:34:17', '2018-02-02 18:34:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `polls_answers`
--

CREATE TABLE `polls_answers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `answer` int(11) NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `polls_answers`
--

INSERT INTO `polls_answers` (`id`, `poll_id`, `answer`, `ip`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, '2018-02-02 20:54:05', NULL, NULL),
(2, 1, 1, NULL, '2018-02-02 20:54:05', NULL, NULL),
(3, 1, 1, NULL, '2018-02-02 20:54:19', NULL, NULL),
(4, 1, 2, NULL, '2018-02-02 20:54:19', NULL, NULL),
(5, 1, 3, NULL, '2018-02-02 20:54:33', NULL, NULL),
(6, 1, 2, NULL, '2018-02-02 20:54:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'person', 'مستخدم فرد', 'مستخدم للموقع فقط', NULL, NULL, NULL),
(2, 'facility', 'منشأة', 'مستخدم في الموقع ', NULL, NULL, NULL),
(3, 'admin', 'المدير العام', 'ادارة لوحة التحكم', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `icon`, `image`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'استشارات هندسية وإشراف', NULL, '1516720226.png', 1, '2018-01-23 13:10:26', '2018-01-23 15:10:26', NULL),
(2, 'تصميم إلكتروني', NULL, '1516720538.png', 1, '2018-01-23 13:15:38', '2018-01-23 15:15:38', NULL),
(3, 'بناء', NULL, '1516721234.png', 1, '2018-01-23 15:26:33', '2018-01-23 15:27:14', NULL),
(4, 'سباكة', NULL, '1516720555.png', 1, '2018-01-23 13:15:55', '2018-01-23 15:15:55', NULL),
(5, 'كهرباء ومكيفات', NULL, '1516720585.png', 1, '2018-01-23 13:16:25', '2018-01-23 15:16:25', NULL),
(6, 'عوازل', NULL, '1516720599.png', 1, '2018-01-23 13:16:39', '2018-01-23 15:16:39', NULL),
(7, 'ألمنيوم وزجاج', NULL, '1516720624.png', 1, '2018-01-23 13:17:04', '2018-01-23 15:17:04', NULL),
(8, 'نجارة', NULL, '1516720650.png', 1, '2018-01-23 13:17:30', '2018-01-23 15:17:30', NULL),
(9, 'حدادة واستانلس ستيل', NULL, '1516720665.png', 1, '2018-01-23 13:17:45', '2018-01-23 15:37:04', NULL),
(10, 'تلييس', NULL, '1516720689.png', 1, '2018-01-23 13:18:09', '2018-01-23 15:18:09', NULL),
(11, 'بلاط وسراميك', NULL, '1516720707.png', 1, '2018-01-23 13:18:27', '2018-01-23 15:18:27', NULL),
(12, 'حجر ورخام وقرميد', NULL, '1516720729.png', 1, '2018-01-23 13:18:49', '2018-01-23 15:37:28', NULL),
(13, 'دهان', NULL, '1516720749.png', 1, '2018-01-23 13:19:09', '2018-01-23 15:19:09', NULL),
(14, 'ديكور', NULL, '1516720768.png', 1, '2018-01-23 13:19:28', '2018-01-23 15:19:28', NULL),
(15, 'شبكات ري', NULL, '1516720781.png', 1, '2018-01-23 13:19:41', '2018-01-23 15:19:41', NULL),
(16, 'مستلزمات حدائق', NULL, '1516720796.png', 1, '2018-01-23 13:19:56', '2018-01-23 15:19:56', NULL),
(17, 'مستلزمات مسابح', NULL, '1516720817.png', 1, '2018-01-23 13:20:17', '2018-01-23 15:20:17', NULL),
(18, 'خيام ومظلات', NULL, '1516720832.png', 1, '2018-01-23 13:20:32', '2018-01-23 15:37:58', NULL),
(19, 'وسائل سلامة', NULL, '1516720850.png', 1, '2018-01-23 13:20:50', '2018-01-23 15:20:50', NULL),
(20, 'مصاعد وسلالم', NULL, '1516720871.png', 1, '2018-01-23 13:21:11', '2018-01-23 15:21:11', NULL),
(21, 'كاميرات مراقبة وشبكات معلوماتية', NULL, '1516720888.png', 1, '2018-01-23 13:21:28', '2018-01-23 15:38:21', NULL),
(22, 'أبواب كهربائية', NULL, '1516720906.png', 1, '2018-01-23 13:21:46', '2018-01-23 15:21:46', NULL),
(23, 'تصاميم مكاتب', NULL, '1516720926.png', 1, '2018-01-23 13:22:06', '2018-01-23 15:22:06', NULL),
(24, 'حفريات وهدم ونظافة', NULL, '1516720965.png', 1, '2018-01-23 13:22:45', '2018-01-23 15:38:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `enabled` int(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `mobile`, `password`, `role_id`, `enabled`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mohammed Ali', 'mfnannah', 'mafnannah@gmail.com', NULL, '$2y$10$qMZtcytUIkmABvAXCMxuX.2Cphci2EIyrPpbeCPBC31.r0kpFsW9G', 3, 1, 'tBbVSTwUepvWuaXmZYXxWI827h3dW7SBJSIx1KEgs92jb779ZMnGmnWkngx0', '2018-01-10 16:45:23', '2018-02-07 20:12:47', NULL),
(4, 'محمد علي فنانه', NULL, 'mfnannah@gmail.com', '0597233620', '$2y$10$WoTknZcM3TTcLLmoJ3Lvge9TIZq3Hz0JUyD02h03U9FpAHkXAz9Su', 1, 1, 'VaxbQzFGztmtxUHDyu12tg9KVSReAX40eT1LeJHwrLLZUnK8NtOpamulUU0k', '2018-02-03 17:21:20', '2018-02-07 20:10:02', NULL),
(10, 'المنشأة الأولى التجريبية', NULL, 'test@test.test', '٠٥٩٧٢٣٣٦٢٠', '$2y$10$JE1B0QVQmEJ1Se0xGTht1OCBkgNoIkuC2WeKcwauzNyDHJUBmSVcu', 2, 1, 'fz5rOWyn3A0aAXKbkozSzVQD9AUhiy7W8mqQjVXBhpsvrByZRhKUhs0fJtc9', '2018-02-03 19:19:06', '2018-02-07 20:08:40', NULL),
(11, 'مدرسة الملك عبدالعزيز للصناعة', NULL, 'new@new.new', '0593333333', '$2y$10$3u.M0xuBvPDuEbFdNXLeue9ea0i9KB01igx5Ka8uH2POJVIdYQDbS', 2, 1, 'fsVZ31RVuuwr2yiFRDQptPvbGnQFphuJ7zee8sRxvSNqaXPVqr9i0kTuS9NC', '2018-02-07 14:24:20', '2018-02-07 17:25:08', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_polls_answers`
-- (See below for the actual view)
--
CREATE TABLE `v_polls_answers` (
`poll_id` int(11)
,`answer` int(11)
,`count` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `work_price`
--

CREATE TABLE `work_price` (
  `id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `work_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work_price`
--

INSERT INTO `work_price` (`id`, `contract_id`, `work_type`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'العمل ١', 11, '2018-02-07 21:10:26', '2018-02-07 21:10:26', NULL),
(2, 1, 'العمل ٢', 22, '2018-02-07 21:10:26', '2018-02-07 21:10:26', NULL);

-- --------------------------------------------------------

--
-- Structure for view `v_polls_answers`
--
DROP TABLE IF EXISTS `v_polls_answers`;

CREATE  VIEW `v_polls_answers`  AS  (select `polls_answers`.`poll_id` AS `poll_id`,`polls_answers`.`answer` AS `answer`,count(0) AS `count` from `polls_answers` where (`polls_answers`.`answer` <> 0) group by `polls_answers`.`answer`,`polls_answers`.`poll_id` order by `polls_answers`.`answer`,`polls_answers`.`poll_id`) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consultation`
--
ALTER TABLE `consultation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evaluation_work`
--
ALTER TABLE `evaluation_work`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_list`
--
ALTER TABLE `mail_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `polls_answers`
--
ALTER TABLE `polls_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `work_price`
--
ALTER TABLE `work_price`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `consultation`
--
ALTER TABLE `consultation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `evaluation_work`
--
ALTER TABLE `evaluation_work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mail_list`
--
ALTER TABLE `mail_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `polls`
--
ALTER TABLE `polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `polls_answers`
--
ALTER TABLE `polls_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `work_price`
--
ALTER TABLE `work_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
