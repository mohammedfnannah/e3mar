<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model{

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'applications';
    public $timestamps = true;
	protected $primaryKey = 'id';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];



	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];


    public function City(){
        return $this->belongsTo('App\Models\City', 'city_id');
    }

    public function Service(){
        return $this->belongsTo('App\Models\Services', 'service_id');
    }


    public function User(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function Facility(){
        return $this->belongsTo('App\Models\Facility', 'facility_id');
    }


}
