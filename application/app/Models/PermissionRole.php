<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model{

	// use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'permission_role';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['permission_id','role_id'];

}
?>