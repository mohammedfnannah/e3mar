<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BackendController extends Controller
{
    var $data = [];
    var $originalName = NULL;
    var $width = NULL;
    var $height = NULL;
    var $size = NULL;

    protected $uploadsPath;


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function uploadImage(Request $request,$image_name = 'image',$folder_name = 'images'){
        if($request->hasFile($image_name)){
            list($this->width, $this->height) = getimagesize(\Input::file($image_name));
            $this->originalName = \Input::file($image_name)->getClientOriginalName();
            $this->size = \Input::file($image_name)->getClientSize();
            $extension = \Input::file($image_name)->getClientOriginalExtension();
            $onlyName = explode('.'.$extension,$this->originalName);
            $fid = time();
            \Input::file($image_name)->move('uploads/'.$folder_name, $fid.'.'.$extension);
            $imgpath = $fid.'.'.$extension;

            return $imgpath;
        }else{
            return NULL;
        }
    }

}
