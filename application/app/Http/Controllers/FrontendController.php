<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    var $data;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function uploadImage(Request $request,$image_name = 'image',$folder_name = 'images'){
        if($request->hasFile($image_name)){
            list($this->width, $this->height) = getimagesize(\Input::file($image_name));
            $this->originalName = \Input::file($image_name)->getClientOriginalName();
            $this->size = \Input::file($image_name)->getClientSize();
            $extension = \Input::file($image_name)->getClientOriginalExtension();
            $onlyName = explode('.'.$extension,$this->originalName);
            $fid = time();
            \Input::file($image_name)->move('uploads/'.$folder_name, $fid.'.'.$extension);
            $imgpath = $fid.'.'.$extension;

            return $imgpath;
        }else{
            return NULL;
        }
    }
}
