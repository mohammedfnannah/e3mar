<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontendController;
use App\Models\Consultation;
use App\Models\Application;
use App\Models\Maillist;
use App\Models\News;
use App\Models\Services;
use App\Models\Banner;
use App\Models\Contactus;
use App\Models\Page;
use App\Models\Polls;
use App\Models\User;
use App\Models\City;
use App\Models\VPollsanswers;
use App\Models\Partners;
use App\Models\Facility;
use App\Models\Condition;
use App\Models\WorkPrice;
use App\Models\Contract;
use App\Models\EvaluationWork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends FrontendController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['services'] = Services::where('status',1)->orderBy('id','ASC')->get();
        $this->data['banners'] = Banner::where('status',1)->orderBy('id','DESC')->limit(3)->get();
        $this->data['news'] = News::where('status',1)->orderBy('id','DESC')->limit(3)->get();
        $this->data['polls'] = Polls::where('status',1)->orderBy('id','DESC')->first();
        $polls_answer = array();
        if(!empty($this->data['polls']) && intval($this->data['polls']->id) > 0){
            $this->data['polls_answer'] = VPollsanswers::where('poll_id',$this->data['polls']->id)->get();
            $count =0;
            foreach ($this->data['polls_answer'] as $row){
                $polls_answer[$row->answer] = $row->count;
                $count = $count + $row->count;
            }
            $polls_answer['count'] = $count;
        }else{
            $polls_answer[1] = 0;
            $polls_answer[2] = 0;
            $polls_answer[3] = 0;
            $polls_answer['count'] = 0;
        }
        $this->data['polls_answer'] = $polls_answer;
        return view('frontend/pages/home')
            ->with('data',$this->data);
    }

    public function contact()
    {
        return view('frontend/pages/contact');
    }
    public function save_contact(Request $request)
    {
        $messages = [
            'name.required'=> 'الاسم : هذا الحقل مطلوب',
            'name.min'=> 'الاسم : هذا الحقل على الاقل ٣ حروف',
        ];
        $this->validate($request, [
            'name'     => 'required|min:3',
        ],$messages);
        Contactus::create(\Input::all());
        return \Redirect::to('contact')->with(['success'=>'تمت العملية بنجاح']);
    }
    public function save_mail_list(Request $request)
    {
        $messages = [
            'email.required'=> 'الإيمل : هذا الحقل مطلوب',
            'email.email'=> 'الإيمل : يحب ان تكون صيغته صحيحة',
        ];
        $this->validate($request, [
            'email'     => 'required|email',
        ],$messages);
        Maillist::create(\Input::all());
        return \Redirect::back()->with(['success'=>'تمت العملية بنجاح','type'=>'mail_list']);
    }

    public function page_by_id($id)
    {
        $page = Page::where('id',$id)->where('status',1)->first();
        return view('frontend/pages/one_page')->with('page',$page);
    }
    public function news()
    {
        $this->data['news'] = News::where('status',1)->orderBy('id','DESC')->paginate(8);
        return view('frontend/pages/news')
            ->with('data',$this->data);
    }
    public function news_by_id($id)
    {
        $news = News::where('id','<>',$id)->where('status',1)->orderBy('id','DESC')->limit(2)->get();
        $one_news = News::where('id',$id)->where('status',1)->first();
        return view('frontend/pages/one_news')
            ->with('news',$news)
            ->with('one_news',$one_news);
    }
    public function services()
    {
        $services = Services::where('status',1)->orderBy('id','ASC')->paginate(24);
        return view('frontend/pages/services')
            ->with('services',$services);
    }
    public function service_by_id($id,$city_id = 0)
    {
        if(isset($city_id) && !empty($city_id) && intval($city_id) > 0){
            $facilities = Facility::where('service_id',$id)->where('status',1)->where('city_id',$city_id)->orderBy('id','ASC')->paginate(6);
        }else{
            $facilities = Facility::where('service_id',$id)->where('status',1)->orderBy('id','ASC')->paginate(6);
        }
        $service = Services::where('id',$id)->where('status',1)->first();
        $cites = City::where('status',1)->orderBy('id','ASC')->get();
        return view('frontend/pages/one_service')
            ->with('service',$service)
            ->with('cites',$cites)
            ->with('facilities',$facilities);
    }
    public function facility_by_id($id)
    {
        $facility = Facility::where('status',1)->where('id',$id)->with('City','Service')->first();
        $facilities = Facility::whereNotIn('id',[$facility->id])->where('service_id',$facility->service_id)->where('city_id',$facility->city_id)->where('status',1)->with('User','City','Service')->orderBy('id','ASC')->limit(3)->get();
        return view('frontend/pages/one_facility')
            ->with('facility',$facility)
            ->with('facilities',$facilities);
    }

    public function partners()
    {
        $partners = Partners::where('status',1)->orderBy('id','ASC')->paginate(8);
        return view('frontend/pages/partners')
            ->with('partners',$partners);
    }
    public function partner_by_id($id)
    {
        $partners = Partners::where('id','<>',$id)->where('status',1)->orderBy('id','DESC')->limit(2)->get();
        $partner = Partners::where('id',$id)->where('status',1)->first();
        return view('frontend/pages/one_partner')
            ->with('partners',$partners)
            ->with('partner',$partner);
    }


    public function consultation()
    {
        return view('frontend/pages/consultation');
    }
    public function save_consultation(Request $request)
    {
        $messages = [
            'question.required'=> 'السؤال : هذا الحقل مطلوب',
            'question.min'=> 'السؤال : هذا الحقل على الاقل ٣ حروف',
        ];
        $this->validate($request, [
            'question'     => 'required|min:3',
        ],$messages);
        Consultation::create(\Input::all());
        return \Redirect::to('consultation')->with(['success'=>'تمت العملية بنجاح']);
    }
    public function evaluation()
    {
        $this->data['cites'] = City::where('status',1)->orderBy('id','ASC')->get();
        $this->data['facilities'] = Facility::where('is_calender_work',1)->where('status',1)->orderBy('name','ASC')->get();
        return view('frontend/pages/evaluation')
            ->with('data',$this->data);
    }
    public function save_evaluation(Request $request)
    {
        $messages = [
            'city_id.required'=> 'المدينة : هذا الحقل مطلوب',
            'facility_id.required'=> 'المنشأة : هذا الحقل مطلوب',
            'text.required'=> 'النص : هذا الحقل مطلوب',
            'text.min'=> 'النص : هذا الحقل على الاقل ٣ حروف',
        ];
        $this->validate($request, [
            'text'     => 'required|min:3',
            'city_id'     => 'required',
            'facility_id'     => 'required',
        ],$messages);
        EvaluationWork::create(\Input::all());
        return \Redirect::to('evaluation')->with(['success'=>'تمت العملية بنجاح']);
    }


    /**
     *
     * @return mixed User Methods
     *
     */

    public function user_register()
    {
        if (\Auth::user()){
            return \Redirect::to('/');
        }
        return view('frontend/pages/user_register');
    }
    public function save_user_register(Request $request)
    {
        $messages = [
            'name.required'=> 'الاسم : هذا الحقل مطلوب',
            'name.min'=> 'الاسم : هذا الحقل على الاقل ٣ حروف',
            'email.required'=> 'الإيمل : هذا الحقل مطلوب',
            'email.email'=> 'الإيمل : يحب ان تكون صيغته صحيحة',
            'mobile.required'=> 'رقم الجوال : هذا الحقل مطلوب',
            'password.required'=> 'كلمة المرور : هذا الحقل مطلوب',
            'password.min'=> 'كلمة المرور : هذا الحقل على الاقل ٦ حروف',
        ];
        $this->validate($request, [
            'name'     => 'required|min:3',
            'email'     => 'required|email|unique:users',
            'mobile'     => 'required',
            'password'     => 'required|min:6',
        ],$messages);
        $data = \Input::all();
        $data['password'] = Hash::make($data['password']);
        $data['role_id'] = 1;
        $user = User::create($data);
        \Auth::loginUsingId($user->id);
        return \Redirect::to('user/profile')->with(['success'=>'تمت العملية بنجاح']);

    }
    public function user_login_view()
    {
        return view('frontend/pages/user_login');
    }
    public function user_login(Request $request)
    {
        $messages = [
            'email.required'=> 'الإيمل : هذا الحقل مطلوب',
            'email.email'=> 'الإيمل : يحب ان تكون صيغته صحيحة',
            'password.required'=> 'كلمة المرور : هذا الحقل مطلوب',
            'password.min'=> 'كلمة المرور : هذا الحقل على الاقل ٦ حروف',
        ];
        $this->validate($request, [
            'email'     => 'required|email',
            'password'     => 'required|min:6',
        ],$messages);

        $data = \Input::all();
        $email = $data['email'];

        $user = User::where('email',$email)->where('enabled',1)->whereIn('role_id',[1,2])->first();
        if($user && intval($user->id) > 0 && Hash::check($data['password'],$user->password)){
            \Auth::loginUsingId($user->id);
            if($user->role_id == 1){
                return \Redirect::to('user/profile');
            }elseif($user->role_id == 2){
                return \Redirect::to('company/profile');
            }
        }

        return \Redirect::to('user/login')->with(['error'=>'اسم المستخدم او كلمة المرور غير صحيحة , الرجاء التأكد من البيانات المدخلة']);

    }
    public function user_profile()
    {
        if (isset(\Auth::user()->id) && auth()->user()->role_id == 1){
            $this->data['user'] = User::where('id',\Auth::user()->id)->first();
            return view('frontend/pages/user_profile')
                ->with('data',$this->data);
        }
        return\Redirect::to('/');
    }
    public function edit_user_profile(Request $request)
    {
        $messages = [
            'name.required'=> 'الاسم : هذا الحقل مطلوب',
            'name.min'=> 'الاسم : هذا الحقل على الاقل ٣ حروف',
            'mobile.required'=> 'رقم الجوال : هذا الحقل مطلوب',
        ];
        $this->validate($request, [
            'name'     => 'required|min:3',
            'mobile'     => 'required',
        ],$messages);
        $data = \Input::all();
        unset($data['_token']);
        User::where('id',\Auth::user()->id)->update($data);
        return \Redirect::to('user/profile')->with(['success'=>'تمت العملية بنجاح']);
    }
    public function user_password()
    {
        if (isset(\Auth::user()->id) && auth()->user()->role_id == 1){
            $this->data['user'] = User::where('id',\Auth::user()->id)->first();
            return view('frontend/pages/user_password')
                ->with('data',$this->data);
        }
        return\Redirect::to('/');
    }
    public function save_user_password(Request $request)
    {
        $messages = [
            'old_password.required'=> 'كلمة المرور الحالية: هذا الحقل مطلوب',
            'old_password.min'=> 'كلمة المرور الحالية: هذا الحقل على الاقل ٦ حروف',
            'password.required'=> 'كلمة المرور الجديدة: هذا الحقل مطلوب',
            'password.min'=> 'كلمة المرور الجديدة: هذا الحقل على الاقل ٦ حروف',
        ];
        $this->validate($request, [
            'old_password'     => 'required|min:6',
            'password'     => 'required|min:6',
        ],$messages);

        $data = \Input::all();
        $user = User::where('id',\Auth::user()->id)->first();
        if($user && intval($user->id) > 0 && Hash::check($data['old_password'],$user->password)){
            $data['password'] = Hash::make($data['password']);
            unset($data['_token']);
            unset($data['old_password']);
            unset($data['confirm_password']);
            $user->update($data);
        }else{
            return \Redirect::to('user/password')->with(['error'=>'كلمة المرور الحالية غير صحيحة']);
        }
        return \Redirect::to('user/password')->with(['success'=>'تمت العملية بنجاح']);
    }
    public function user_applications()
    {
        $applications = Application::where('user_id',\Auth::user()->id)->orderBy('id','DESC')->get();
        return view('frontend/pages/user_applications')
            ->with('applications',$applications);
    }
    public function new_user_applications()
    {
        $this->data['services'] = Services::where('status',1)->orderBy('id','ASC')->get();
        $this->data['cites'] = City::where('status',1)->orderBy('id','ASC')->get();
        return view('frontend/pages/user_new_applications')
            ->with('data',$this->data);
    }
    public function save_user_applications(Request $request)
    {
        $messages = [
            'address.required'=> 'العنوان : هذا الحقل مطلوب',
            'address.min'=> 'العنوان : هذا الحقل على الاقل ٣ حروف',
            'text.required'=> 'التفاصيل : هذا الحقل مطلوب',
            'text.min'=> 'التفاصيل : هذا الحقل على الاقل ٣ حروف',
        ];
        $this->validate($request, [
            'address'     => 'required|min:3',
            'text'     => 'required|min:3',
        ],$messages);
        Application::create(\Input::all());
        return \Redirect::to('user/applications')->with(['success'=>'تمت العملية بنجاح']);
    }
    public function delete_user_application($id){
        $applications = Application::where('user_id',\Auth::user()->id)->where('id',$id)->first();
        if($applications){
            $applications->delete();
            return \Redirect::to('user/applications')->with(['success'=>'تمت العملية بنجاح']);
        }else{
            return \Redirect::back()->with(['error'=>'لا يمكن اتمام العملية']);
        }
    }



    /**
     *
     * @return mixed Company Methods
     *
     */

    public function company_register()
    {
        if (\Auth::user()){
            return \Redirect::to('/');
        }
        $this->data['services'] = Services::where('status',1)->orderBy('id','ASC')->get();
        $this->data['cites'] = City::where('status',1)->orderBy('id','ASC')->get();
        return view('frontend/pages/company_register')
            ->with('data',$this->data);
    }
    public function save_company_register(Request $request)
    {
        $messages = [
            'name.required'=> 'اسم المستخدم : هذا الحقل مطلوب',
            'name.min'=> 'اسم المستخدم : هذا الحقل على الاقل ٣ حروف',
            'email.required'=> 'الإيمل : هذا الحقل مطلوب',
            'email.email'=> 'الإيمل : يحب ان تكون صيغته صحيحة',
            'mobile.required'=> 'رقم الجوال : هذا الحقل مطلوب',
            'commercial_number.required'=> 'رقم السجل التجاري : هذا الحقل مطلوب',
            'password.required'=> 'كلمة المرور : هذا الحقل مطلوب',
            'password.min'=> 'كلمة المرور : هذا الحقل على الاقل ٦ حروف',
        ];
        $this->validate($request, [
            'name'     => 'required|min:3',
            'email'     => 'required|email|unique:users',
            'mobile'     => 'required',
            'commercial_number'     => 'required',
            'password'     => 'required|min:6',
        ],$messages);

        $all_data = \Input::all();

        $data['name'] = $all_data['name'];
        $data['email'] =$all_data['email'];
        $data['mobile'] =$all_data['mobile'];
        $data['password'] = Hash::make($all_data['password']);
        $data['role_id'] = 2;
        $user = User::create($data);

        unset($all_data['email']);
        unset($all_data['password']);
        unset($all_data['confirm_password']);
        $all_data['user_id'] = $user->id;

        $image_path = $this->uploadImage($request, 'image', 'facility');
        $all_data['image'] = $image_path;

        Facility::create($all_data);

        \Auth::loginUsingId($user->id);
        return \Redirect::to('company/profile')->with(['success'=>'تمت العملية بنجاح']);
    }
    public function company_profile()
    {
        if (isset(\Auth::user()->id) && auth()->user()->role_id == 2){
            $facility = Facility::where('status',1)->where('user_id',auth()->user()->id)->with('User','City','Service')->first();
            $applications = Application::where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',0)->count();
            return view('frontend/pages/company_profile')
                ->with('facility',$facility)
                ->with('new_applications',$applications);
        }
        return\Redirect::to('/');
    }
    public function edit_company_profile(Request $request)
    {
        $messages = [
            'name.required'=> 'الاسم : هذا الحقل مطلوب',
            'name.min'=> 'الاسم : هذا الحقل على الاقل ٣ حروف',
            'mobile.required'=> 'رقم الجوال : هذا الحقل مطلوب',
        ];
        $this->validate($request, [
            'name'     => 'required|min:3',
            'mobile'     => 'required',
        ],$messages);
        $data = \Input::all();
        unset($data['_token']);
        User::where('id',\Auth::user()->id)->update($data);
        return \Redirect::to('company/profile')->with(['success'=>'تمت العملية بنجاح']);
    }
    public function company_password()
    {
        if (isset(\Auth::user()->id) && auth()->user()->role_id == 2){
            $facility = Facility::where('status',1)->where('user_id',auth()->user()->id)->with('User','City','Service')->first();
            $this->data['user'] = User::where('id',\Auth::user()->id)->first();
            $applications = Application::where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',0)->count();
            return view('frontend/pages/company_password')
                ->with('facility',$facility)
                ->with('data',$this->data)
                ->with('new_applications',$applications);
        }
        return\Redirect::to('/');
    }
    public function save_company_password(Request $request)
    {
        $messages = [
            'old_password.required'=> 'كلمة المرور الحالية: هذا الحقل مطلوب',
            'old_password.min'=> 'كلمة المرور الحالية: هذا الحقل على الاقل ٦ حروف',
            'password.required'=> 'كلمة المرور الجديدة: هذا الحقل مطلوب',
            'password.min'=> 'كلمة المرور الجديدة: هذا الحقل على الاقل ٦ حروف',
        ];
        $this->validate($request, [
            'old_password'     => 'required|min:6',
            'password'     => 'required|min:6',
        ],$messages);

        $data = \Input::all();
        $user = User::where('id',\Auth::user()->id)->first();
        if($user && intval($user->id) > 0 && Hash::check($data['old_password'],$user->password)){
            $data['password'] = Hash::make($data['password']);
            unset($data['_token']);
            unset($data['old_password']);
            unset($data['confirm_password']);
            $user->update($data);
        }else{
            return \Redirect::to('company/password')->with(['error'=>'كلمة المرور الحالية غير صحيحة']);
        }
        return \Redirect::to('company/password')->with(['success'=>'تمت العملية بنجاح']);
    }
    public function company_applications()
    {
        if (isset(\Auth::user()->id) && auth()->user()->role_id == 2){
            $facility = Facility::where('status',1)->where('user_id',auth()->user()->id)->with('User','City','Service')->first();
            $applications = Application::where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',0)->get();
            $new_applications = Application::where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',0)->count();
            return view('frontend/pages/company_applications')
                ->with('title','طلبات جديدة')
                ->with('type','new')
                ->with('facility',$facility)
                ->with('new_applications',$new_applications)
                ->with('applications',$applications);
        }
        return\Redirect::to('/');
    }
    public function company_current_applications()
    {
        if (isset(\Auth::user()->id) && auth()->user()->role_id == 2){
            $facility = Facility::where('status',1)->where('user_id',auth()->user()->id)->with('User','City','Service')->first();
            $applications = Application::where('facility_id',$facility->id)->where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',1)->get();
            $new_applications = Application::where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',0)->count();
            return view('frontend/pages/company_applications')
                ->with('title','مشاريع وأعمال قيد التنفيذ')
                ->with('type','current')
                ->with('facility',$facility)
                ->with('new_applications',$new_applications)
                ->with('applications',$applications);
        }
        return\Redirect::to('/');
    }
    public function company_archive_applications()
    {
        if (isset(\Auth::user()->id) && auth()->user()->role_id == 2){
            $facility = Facility::where('status',1)->where('user_id',auth()->user()->id)->with('User','City','Service')->first();
            $applications = Application::where('facility_id',$facility->id)->where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',2)->get();
            $new_applications = Application::where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',0)->count();
            return view('frontend/pages/company_applications')
                ->with('title','أرشيف المشاريع والأعمال')
                ->with('type','archive')
                ->with('facility',$facility)
                ->with('new_applications',$new_applications)
                ->with('applications',$applications);
        }
        return\Redirect::to('/');
    }






    public function company_contracts()
    {
        if (isset(\Auth::user()->id) && auth()->user()->role_id == 2){
            $facility = Facility::where('status',1)->where('user_id',auth()->user()->id)->with('User','City','Service')->first();
            $applications = Application::where('facility_id',$facility->id)->where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',2)->get();
            $new_applications = Application::where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',0)->count();

            $contract = Contract::where('facility_id',\Auth::user()->id)->get();
            return view('frontend/pages/company_contracts')
                ->with('title','العقود المسجلة')
                ->with('type','archive')
                ->with('contract',$contract)
                ->with('facility',$facility)
                ->with('new_applications',$new_applications)
                ->with('applications',$applications);
        }
        return\Redirect::to('/');
    }



    public function new_company_contracts($application_id)
    {
        if (isset(\Auth::user()->id) && auth()->user()->role_id == 2){

            $facility = Facility::where('status',1)->where('user_id',auth()->user()->id)->with('User','City','Service')->first();
            $new_applications = Application::where('city_id',$facility->city_id)->where('service_id',$facility->service_id)->where('status',0)->count();

            $application = Application::where('id',$application_id)->with('User')->first();
            $this->data['services'] = Services::where('status',1)->orderBy('id','ASC')->get();
            $this->data['cites'] = City::where('status',1)->orderBy('id','ASC')->get();

            return view('frontend/pages/company_new_contract')
                ->with('title','تسجيل عقد')
                ->with('type','archive')
                ->with('data',$this->data)
                ->with('facility',$facility)
                ->with('new_applications',$new_applications)
                ->with('application',$application);
        }
        return\Redirect::to('/');
    }



    public function save_new_company_contracts()
    {
        if (isset(\Auth::user()->id) && auth()->user()->role_id == 2){


            $all_data = \Input::all();

            $condition = \Input::get('condition');
            $works = \Input::get('works');
            $price = \Input::get('price');

            unset($all_data['condition']);
            unset($all_data['works']);
            unset($all_data['price']);
            unset($all_data['_token']);

            $contract = Contract::create($all_data);

            if(count($condition) > 0){
                foreach ($condition as $row){
                    if (!empty($row)){
                        Condition::create([
                            'contract_id'=>$contract->id,
                            'text'=>$row,
                        ]);
                    }
                }
            }

            if(count($works) > 0){
                for ($i =0 ; $i < count($works) ; $i++){
                    if (!empty($works[$i])){
                        WorkPrice::create([
                            'contract_id'=>$contract->id,
                            'work_type'=>$works[$i],
                            'price'=>$price[$i],
                        ]);
                    }
                }
            }

            return\Redirect::to('company/contracts')->with('success','تم تسجيل العقد بنجاح');
        }
        return\Redirect::to('/');
    }



    /**
     * other
     */

    public function service_by_title()
    {
        return view('frontend/pages/one_service');
    }
    public function partner_by_title()
    {
        return view('frontend/pages/one_service');
    }
    public function page_by_title()
    {
        return view('frontend/pages/one_page');
    }
}
