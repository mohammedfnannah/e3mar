<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;

class EntrustController extends BackendController {

    public function __construct(){
        parent::__construct();
    }


    /**
     * @return $this
     * Role Sections
     */

    public function roles(){
        $this->data['url'] = 'admin/entrust';
        $this->data['name'] = 'إدارة المهام والصلاحيات';

        $permission = Permission::orderBy('name','ASC')->get();
        $this->data['permission'] = $permission;
        return view('backend/entrust/role',$this->data);
    }

    public function rolesdata(){
        $role = Role::select('*');
        return \Datatables::of($role)
            ->addColumn('edit_action', function ($role) {return '<a  id="'.$role->id.'" onclick="showModal('.$role->id.')" class="btn btn-info btn-social-icon"><i class="fa fa-edit"></i></a>';})
            ->addColumn('delete_action', function ($role) {
                return '<a onclick="deleteThis('.$role->id.')" class="btn btn-danger btn-social-icon"><i class="fa fa-trash-o"></i></a>';
            })
            ->make(true);

    }

    public function addrole(Request $request){
        $this->data = $request->all();
        $name = $this->data['name'];
        $display_name = $this->data['display_name'];
        $description = $this->data['description'];
        if(isset($this->data['permissions']) && !empty($this->data['permissions'])){
            $permissions = $this->data['permissions'];
        }else{
            $permissions = null;
        }
        $role = Role::create([
            'name'=>$name,
            'display_name'=>$display_name,
            'description'=>$description,
        ]);
        if (isset($permissions) && !empty($permissions) && count($permissions)>0){
            foreach ($permissions as $row){
                $role->attachPermission($row);
            }
        }
        if($role){
            $role_row = '';
            $role_row.= '<tr role="row">';
            $role_row.='<td class="text-center" id="'.$role->id.'">'.$role->name.'</td>';
            $role_row.='<td class="text-center">'.$role->display_name.'</td>';
            $role_row.='<td class="text-center">'.$role->description.'</td>';

            $role_row.='<td class="text-center"><a href="#" onclick="showModal('.$role->id.')" class="btn btn-info btn-social-icon"><i class="fa fa-edit"></i></a></td>';
            $role_row.='<td class="text-center"><a onclick="deleteThis('.$role->id.')" id="'.$role->id.'" class="btn btn-danger btn-social-icon"><i class="fa fa-trash-o"></i></a></td>';
            $role_row.='</tr>';

            return response()->json([
                'success'=>TRUE,
                'new_role'=>TRUE,
                'role_row' => $role_row
            ]);
        }
    }

    public function checkerolename(Request $request){
        $name = $request->input('name');
        $check_role = Role::where('name', $name)->whereNull('deleted_at')->first();
        return response()->json(array(
            'valid' => is_null($check_role),
        ));
    }

    public function showeditroleform(Request $request){
        if($request->has('id')){
            $role = Role::find($request->input('id'));
            $permission_role_array = [];
            $permission_role = PermissionRole::where('role_id',$request->input('id'))->get();
            if (!empty($permission_role) && count($permission_role) > 0){
                foreach ($permission_role as $row){
                    $permission_role_array[] = $row->permission_id;
                }
            }
            $permission = Permission::orderBy('name','ASC')->get();
            return response()->json([
                    'success' => TRUE,
                    'page' => view('backend/entrust/editrole')->with('role',$role)->with('role_permission',$permission_role_array)->with('permission',$permission)->render()
                ]
            );
        }
    }

    public function editrole(Request $request){
        $this->data = $request->all();
        $name = $this->data['name'];
        $display_name = $this->data['display_name'];
        $description = $this->data['description'];
        $permissions = $this->data['permissions'];

        $role = Role::where('id',$this->data['id'])->first();
        $role->update([
            'name'=>$name,
            'display_name'=>$display_name,
            'description'=>$description,
        ]);

        if (isset($permissions) && !empty($permissions) && count($permissions)>0){
            foreach ($permissions as $row){
                try{
                    $role->attachPermission($row);
                }catch (\Exception $e){

                }
            }
        }
        PermissionRole::where('role_id',$this->data['id'])->whereNotIn('permission_id',$permissions)->delete();

        if($role){
            $role_row = '';
            $role_row.= '<tr role="row">';
            $role_row.='<td class="text-center">'.$role->name.'</td>';
            $role_row.='<td class="text-center">'.$role->display_name.'</td>';
            $role_row.='<td class="text-center">'.$role->description.'</td>';

            $role_row.='<td class="text-center"><a href="#" onclick="showModal('.$role->id.')" id="'.$role->id.'" class="btn btn-info btn-social-icon"><i class="fa fa-edit"></i></a></td>';
            $role_row.='<td class="text-center"><a onclick="deleteThis('.$role->id.')"  class="btn btn-danger btn-social-icon"><i class="fa fa-trash-o"></i></a></td>';
//
            $role_row.='</tr>';

            return response()->json([
                'success'=>TRUE,
                'new_role'=>TRUE,
                'id'=>$this->data['id'],
                'role_row' => $role_row
            ]);
        }
    }

    public function destroyrole(Request $request){
        $this->data = $request->all();
        $id = $this->data['id'];
        $deletedRole = Role::destroy($id);
        if($deletedRole){
            return response()->json([
                'success'=>TRUE,
                'deleted_Role'=>TRUE,
                'role_id' => $id
            ]);
        }
    }


    /**
     * @return $this
     * Permission Sections
     */

    public function permissions(){
        $this->data['url'] = 'admin/entrust';
        $this->data['name'] = 'إدارة المهام والصلاحيات';
        return view('backend/entrust/permission',$this->data);
    }

    public function permissionsdata(){
        $permission = Permission::select('*');
        return \Datatables::of($permission)
            ->addColumn('edit_action', function ($permission) {return '<a  id="'.$permission->id.'"  onclick="showModal('.$permission->id.')" class="btn btn-info btn-social-icon"><i class="fa fa-edit"></i></a>';})
            ->make(true);
    }

    public function showeditpermissionform(Request $request){
        if($request->has('id')){
            $permission = Permission::find($request->input('id'));
            return response()->json([
                    'success' => TRUE,
                    'page' => view('backend/entrust/editPermission')->with('permission',$permission)->render()
                ]
            );
        }
    }

    public function editpermission(Request $request){
        $this->data = $request->all();
        $display_name = $this->data['display_name'];
        $description = $this->data['description'];

        $permission = Permission::find($this->data['id']);
        $permission->update([
            'display_name'=>$display_name,
            'description'=>$description,
        ]);

        if($permission){
            $permission_row = '';
            $permission_row.= '<tr role="row">';
            $permission_row.='<td class="text-center"  id="'.$permission->id.'" >'.$permission->name.'</td>';
            $permission_row.='<td class="text-center">'.$permission->display_name.'</td>';
            $permission_row.='<td class="text-center">'.$permission->description.'</td>';
            $permission_row.='<td class="text-center"><a href="#" onclick="showModal('.$permission->id.')" class="btn btn-info btn-social-icon"><i class="fa fa-edit"></i></a></td>';
            $permission_row.='</tr>';

            return response()->json([
                'success'=>TRUE,
                'new_permission'=>TRUE,
                'id'=>$this->data['id'],
                'permission_row' => $permission_row
            ]);
        }
    }
    
    public function update_permations(){
        $routeCollection = \Route::getRoutes();
        $routeArray = [];
        foreach ($routeCollection as $value) {
            if(substr($value->getPath(), 0, 5) === 'admin' && substr($value->getPath(), 0, 11) != 'admin/login' && substr($value->getPath(), 0, 12) != 'admin/logout' && substr($value->getPath(), 0, 14) != 'admin/register'  && substr($value->getPath(), 0, 14) != 'admin/password' && $value->getPath() != 'admin' && substr($value->getPath(), 0, 10) != 'admin/test'){
                $paths = explode('/',$value->getPath());
                $route = '';
                foreach ($paths as $row){
                    if ($row == 'admin'){
                        continue;
                    }
                    if (strpos($row, '{') === false){
                        $route.= $row.'-';
                    }
                }

                $routeArray[] = $route;
            }
        }
        $clearArray = [];
        foreach (array_filter(array_unique($routeArray)) as $row){
            $clearArray[] = substr($row, 0, -1);
        }
        sort($clearArray);
        $permissionArray = [];
        foreach ($clearArray as $row){
            $permission = Permission::firstOrNew(['name'=> $row]);
            $permission->name         = $row;
            $permission->save();

            $permissionArray[] = $permission->id;
        }
        $permissions = implode(',',$permissionArray);
        if (!empty($permissions) && count($permissions) > 0){
            \DB::statement('DELETE FROM `permissions` WHERE `id` NOT IN ('.$permissions.')');
        }
        echo '<pre>';
        print_r($clearArray);
        echo '</pre>';
        return \Redirect::back();
    }

}
?>