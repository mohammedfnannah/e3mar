<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\Contactus;

class ContactusController extends BackendController {

    protected $profilePicturesPath;
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $contactus = Contactus::orderBy('id','DESC')->get();
        $this->data['url'] = 'admin/contactus';
        $this->data['name'] = 'إدارة رسائل تواصل معنا';
        return view('backend/contactus/contactus')->with('contactus',$contactus)->with('data',$this->data);
    }

    public function data(){
        $contactus = Contactus::select('*');
        return \Datatables::of($contactus)
            ->addColumn('dt_created_date', function ($contact) {return date('Y-m-d',strtotime($contact->dt_created_date));})
            ->addColumn('edit_action', function ($contact) {return '<a onclick="showModal('.$contact->id.')" class="btn btn-info btn-social-icon"><i class="fa fa-envelope-o"></i></a>';})
            ->addColumn('delete_action', function ($contact) {return '<a onclick="deleteThis('.$contact->id.')" id="'.$contact->id.'" class="btn btn-danger btn-social-icon"><i class="fa fa-trash-o"></i></a>';})
            ->make(true);
    }

    public function show_edit_form(Request $request){
        if($request->has('id')){
            $contactus = Contactus::find($request->input('id'));
            return response()->json([
                    'success' => TRUE,
                    'page' => view('backend/contactus/edit')->with('contactus',$contactus)->render()
                ]
            );
        }
    }

    public function delete(Request $request){
        $data = $request->all();
        $id = $data['id'];
        $deletedRestaurant = Contactus::destroy($id);
        if($deletedRestaurant){
            return response()->json([
                'success'=>TRUE,
                'deleted_Restaurant'=>TRUE,
                'restaurant_id' => $id
            ]);
        }
    }



}
