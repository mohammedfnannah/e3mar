<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;

class SettingsController extends BackendController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(\Cache::get('settings.facebook'));
        $this->data['title'] = 'الاعدادات';
        return view('backend.settings.index')->with('data',$this->data);
    }

    public function update()
    {
        $inputs = \Input::except('image');
        foreach($inputs as $key=>$input)
        {
            \Cache::store('file')->forever('settings.'.$key,$input);
        }

        $advs = ['top_main_image','middle_main_image','bottom_main_image'];
        foreach($advs as $adv)
        {
            if(\Input::hasFile($adv))
            {
                $filename = \Input::file($adv)->getClientOriginalName();
                $extension = \Input::file($adv)->getClientOriginalExtension();
                $fid = time();
                try{
                    unlink('uploads/'.\Cache::get('settings.'.$adv));
                }catch (Exception $exp){}
                \Input::file($adv)->move('uploads/', $fid.'_'.$filename);
                \Cache::store('file')->forever('settings.'.$adv,$fid.'_'.$filename);
            }
        }

        \Session::flash('success','تمت العملية بنجاح');
        return \Redirect::back();
    }

}
