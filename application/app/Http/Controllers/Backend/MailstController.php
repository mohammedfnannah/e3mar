<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\Maillist;

class MailstController extends BackendController {

    protected $profilePicturesPath;
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $mai_list = Maillist::orderBy('id','DESC')->get();
        $this->data['url'] = 'admin/mail_list';
        $this->data['name'] = 'إدارة القائمة البريدية';
        return view('backend/mai_list/index')->with('mai_list',$mai_list)->with('data',$this->data);
    }

    public function data(){
        $mai_list = Maillist::select('*');
        return \Datatables::of($mai_list)
            ->addColumn('dt_created_date', function ($mail) {return date('Y-m-d',strtotime($mail->dt_created_date));})
            ->addColumn('edit_action', function ($mail) {return '<a onclick="showModal('.$mail->id.')" class="btn btn-info btn-social-icon"><i class="fa fa-envelope-o"></i></a>';})
            ->addColumn('delete_action', function ($mail) {return '<a onclick="deleteThis('.$mail->id.')" id="'.$mail->id.'" class="btn btn-danger btn-social-icon"><i class="fa fa-trash-o"></i></a>';})
            ->make(true);
    }

    public function delete(Request $request){
        $data = $request->all();
        $id = $data['id'];
        $deletedRestaurant = Maillist::destroy($id);
        if($deletedRestaurant){
            return response()->json([
                'success'=>TRUE,
                'deleted_Restaurant'=>TRUE,
                'restaurant_id' => $id
            ]);
        }
    }



}
