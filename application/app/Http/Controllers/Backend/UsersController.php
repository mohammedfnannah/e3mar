<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends BackendController {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $users = User::whereIn('fk_i_role_id',[2,3])->get();
        $this->data['url'] = 'admin/users';
        $this->data['name'] = 'إدارة المستخدمين';
        return view('admin/users/users')->with('restaurants_users',$users)->with('data',$this->data);
    }

    public function data(){
         $restaurants_users = User::whereIn('fk_i_role_id',[2,3])->select('*');
        return \Datatables::of($restaurants_users)
            ->addColumn('fk_i_role_id', function ($restaurants_user) {
                if($restaurants_user->fk_i_role_id == 2) return 'زبون';
                if($restaurants_user->fk_i_role_id == 3) return 'سائق';
            })
            ->addColumn('i_order_count', function ($restaurants_user) {
                if (intval($restaurants_user->i_order_count) > 0) {
                    if($restaurants_user->fk_i_role_id == 2) return '<a href="'.url('admin/orders/search?customers='.$restaurants_user->pk_i_id).'">'.$restaurants_user->i_order_count.'</a>';;
                    if($restaurants_user->fk_i_role_id == 3) return '<a href="'.url('admin/orders/search?drivers='.$restaurants_user->pk_i_id).'">'.$restaurants_user->i_order_count.'</a>';;
                    
                }else{
                    return $restaurants_user->i_order_count;
                }
                
            })
            ->addColumn('b_enabled', function ($restaurants_user) {
                $is_active = '';
                $is_active.='<div class="col-md-12">';
                $is_active.='<div class="md-checkbox">';
                if($restaurants_user->b_enabled == TRUE) {
                    $is_active .= '<input type="checkbox" value="' . $restaurants_user->pk_i_id . '" checked id="checkbox33_' . $restaurants_user->pk_i_id . '" class="md-check">';
                    $is_active .= '<label onclick="ch_st(' . $restaurants_user->pk_i_id . ')" id="label_status_' . $restaurants_user->pk_i_id . '" for="checkbox33_' . $restaurants_user->pk_i_id . '">';
                    $is_active .= '<span class="inc"></span>';
                    $is_active .= '<span class="check"></span>';
                    $is_active .= '<span class="box"></span>مفعل';
                    $is_active .= '</label>';
                }
                else {
                    $is_active .= '<input type="checkbox" value="' . $restaurants_user->pk_i_id . '" id="checkbox33_' . $restaurants_user->pk_i_id . '" class="md-check">';
                    $is_active .= '<label onclick="ch_st(' . $restaurants_user->pk_i_id . ')" id="label_status_' . $restaurants_user->pk_i_id . '" for="checkbox33_' . $restaurants_user->pk_i_id . '">';
                    $is_active .= '<span class="inc"></span>';
                    $is_active .= '<span class="check"></span>';
                    $is_active .= '<span class="box"></span>تفعيل؟';
                    $is_active .= '</label>';
                }
                $is_active .= '</div>';
                $is_active .= '</div>';
                return $is_active;
            })

            ///////////////////////////////////////////////////////////////////////
            
            ->addColumn('dt_verified_date', function ($restaurants_user) {            
            $is_verify = '';
            $is_verify.='<div class="col-md-12">';
            $is_verify.='<div class="md-checkbox">';
            if(!empty($restaurants_user->dt_verified_date)) {
                $is_verify .= '<input type="checkbox" value="' . $restaurants_user->pk_i_id . '" checked id="checkbox32_' . $restaurants_user->pk_i_id . '" class="md-check">';
                $is_verify .= '<label onclick="ch_st2(' . $restaurants_user->pk_i_id . ')" id="label_verify_' . $restaurants_user->pk_i_id . '" for="checkbox32_' . $restaurants_user->pk_i_id . '">';
                $is_verify .= '<span class="inc"></span>';
                $is_verify .= '<span class="check"></span>';
                $is_verify .= '<span class="box"></span>مفعل';
                $is_verify .= '</label>';
            }
            else {
                $is_verify .= '<input type="checkbox" value="' . $restaurants_user->pk_i_id . '" id="checkbox32_' . $restaurants_user->pk_i_id . '" class="md-check">';
                $is_verify .= '<label onclick="ch_st2(' . $restaurants_user->pk_i_id . ')" id="label_verify_' . $restaurants_user->pk_i_id . '" for="checkbox32_' . $restaurants_user->pk_i_id . '">';
                $is_verify .= '<span class="inc"></span>';
                $is_verify .= '<span class="check"></span>';
                $is_verify .= '<span class="box"></span>تفعيل؟';
                $is_verify .= '</label>';
            }
            $is_verify .= '</div>';
            $is_verify .= '</div>';

                return $is_verify;
            })
            /////////////////////////////////////////////////////////////////////////
            ->addColumn('dt_created_date', function ($restaurants_user) {return date('Y-m-d',strtotime($restaurants_user->dt_created_date));})
            ->addColumn('edit_action', function ($restaurants_user) {return '<a onclick="showModal('.$restaurants_user->pk_i_id.')" class="btn btn-xs blue"><i class="icon-pencil"></i></a>';})
            ->addColumn('delete_action', function ($restaurants_user) {return '<a onclick="deleteThis('.$restaurants_user->pk_i_id.')" id="'.$restaurants_user->pk_i_id.'" class="btn btn-xs red"><i class="icon-trash"></i></a>';})
            ->make(true);
    }

    public function add(Request $request){
        $this->data = $request->all();
        $s_name = $this->data['s_name'];
        $s_email = $this->data['s_email'];
        $s_mobile_number = $this->data['s_mobile_number'];
        $fk_i_role_id = $this->data['fk_i_role_id'];
        $password = $this->data['s_password'];
        $s_password = md5($password);
        $b_enabled = TRUE;
        $dt_verified_date = date('Y-m-d h:i:s',time());



        $restaurant = User::create([
            's_name'=>$s_name,
            's_email'=>$s_email,
            'fk_i_role_id'=>$fk_i_role_id,
            's_mobile_number'=>$s_mobile_number,
            's_password'=>$s_password,
            'b_enabled'=>$b_enabled,
            'dt_verified_date'=>$dt_verified_date,
        ]);


        if($restaurant){

            $restaurant_row = '';
            $restaurant_row.= '<tr role="row">';
            $restaurant_row.='<td class="text-center">'.$restaurant->pk_i_id.'</td>';
            $restaurant_row.='<td class="text-center">'.$restaurant->s_name.'</td>';
            $restaurant_row.='<td class="text-center">'.$restaurant->s_mobile_number.'</td>';            
            if($restaurant->fk_i_role_id == 2){
                $restaurant_row.='<td class="text-center">'.'زبون'.'</td>';    
            }elseif ($restaurant->fk_i_role_id == 3){
                $restaurant_row.='<td class="text-center">'.'سائق'.'</td>';            
            }
            $restaurant_row.='<td class="text-center">'.$restaurant->s_email.'</td>';

            $is_active = '';
            $is_active.='<div class="col-md-12">';
            $is_active.='<div class="md-checkbox">';
            if($restaurant->b_enabled == TRUE) {
                $is_active .= '<input type="checkbox" value="' . $restaurant->pk_i_id . '" checked id="checkbox33_' . $restaurant->pk_i_id . '" class="md-check">';
                $is_active .= '<label onclick="ch_st(' . $restaurant->pk_i_id . ')" id="label_status_' . $restaurant->pk_i_id . '" for="checkbox33_' . $restaurant->pk_i_id . '">';
                $is_active .= '<span class="inc"></span>';
                $is_active .= '<span class="check"></span>';
                $is_active .= '<span class="box"></span>مفعل';
                $is_active .= '</label>';
            }
            else {
                $is_active .= '<input type="checkbox" value="' . $restaurant->pk_i_id . '" id="checkbox33_' . $restaurant->pk_i_id . '" class="md-check">';
                $is_active .= '<label onclick="ch_st(' . $restaurant->pk_i_id . ')" id="label_status_' . $restaurant->pk_i_id . '" for="checkbox33_' . $restaurant->pk_i_id . '">';
                $is_active .= '<span class="inc"></span>';
                $is_active .= '<span class="check"></span>';
                $is_active .= '<span class="box"></span>تفعيل؟';
                $is_active .= '</label>';
            }
            $is_active .= '</div>';
            $is_active .= '</div>';

            $restaurant_row.='<td class="text-center">'.$is_active.'</td>';

            ///////////////////////////////////////////////////////////////////////
            $is_verify = '';
            $is_verify.='<div class="col-md-12">';
            $is_verify.='<div class="md-checkbox">';
            if(!empty($restaurant->dt_verified_date)) {
                $is_verify .= '<input type="checkbox" value="' . $restaurant->pk_i_id . '" checked id="checkbox32_' . $restaurant->pk_i_id . '" class="md-check">';
                $is_verify .= '<label onclick="ch_st2(' . $restaurant->pk_i_id . ')" id="label_verify_' . $restaurant->pk_i_id . '" for="checkbox32_' . $restaurant->pk_i_id . '">';
                $is_verify .= '<span class="inc"></span>';
                $is_verify .= '<span class="check"></span>';
                $is_verify .= '<span class="box"></span>مفعل';
                $is_verify .= '</label>';
            }
            else {
                $is_verify .= '<input type="checkbox" value="' . $restaurant->pk_i_id . '" id="checkbox32_' . $restaurant->pk_i_id . '" class="md-check">';
                $is_verify .= '<label onclick="ch_st2(' . $restaurant->pk_i_id . ')" id="label_verify_' . $restaurant->pk_i_id . '" for="checkbox32_' . $restaurant->pk_i_id . '">';
                $is_verify .= '<span class="inc"></span>';
                $is_verify .= '<span class="check"></span>';
                $is_verify .= '<span class="box"></span>تفعيل؟';
                $is_verify .= '</label>';
            }
            $is_verify .= '</div>';
            $is_verify .= '</div>';

            $restaurant_row.='<td class="text-center">'.$is_verify.'</td>';

            /////////////////////////////////////////////////////////////////////////

            $restaurant_row.='<td class="text-center">'.date('Y-m-d',strtotime($restaurant->dt_created_date)).'</td>';


            $restaurant_row.='<td class="text-center"><a href="#" onclick="showModal('.$restaurant->pk_i_id.')" class="btn btn-xs blue"><i class="icon-pencil"></i></a></td>';
            $restaurant_row.='<td class="text-center"><a onclick="deleteThis('.$restaurant->pk_i_id.')" id="'.$restaurant->pk_i_id.'" class="btn btn-xs red"><i class="icon-trash"></i></a></td>';
            $restaurant_row.='</tr>';

            return response()->json([
                'success'=>TRUE,
                'new_restaurant'=>TRUE,
                'restaurant_user' => $restaurant_row
            ]);
        }

    }

    public function update(Request $request){
        $this->data = $request->all();
        
        $s_name = $this->data['s_name'];
        $s_email = $this->data['s_email'];
        $fk_i_role_id = $this->data['fk_i_role_id'];
        $s_mobile_number = $this->data['s_mobile_number'];

        $restaurant = User::find($this->data['pk_i_id']);
        if($request->has('s_password')) {
            $password = $this->data['s_password'];
            $s_password = md5($password);
        }
        else
            $s_password = $restaurant->s_password;

        $update = [
            's_name'=>$s_name,
            's_mobile_number'=>$s_mobile_number,
            's_email'=>$s_email,
            's_password'=>$s_password,
            'fk_i_role_id'=>$fk_i_role_id,
        ];

        if($fk_i_role_id == 3 && $restaurant->fk_i_role_id == 2){
            $group = 4;
            $fk_i_user_id = intval($this->data['pk_i_id']);
            $s_message = '';
            $sql2 = "SELECT s_name FROM `t_constant_localization` WHERE `s_language_code` = (SELECT `s_client_language_code` FROM `t_user_device` WHERE dt_deleted_date IS NULL AND `fk_i_user_id`=".$fk_i_user_id." ORDER BY pk_i_id DESC LIMIT 1) AND fk_i_constant_id = 5012";
            $message = \DB::select($sql2);
            if (isset($message[0]) && !empty($message[0]) && isset($message[0]->s_name) && !empty($message[0]->s_name)) {
                $s_message = $message[0]->s_name;
                $this->httpGetRequest(config('app.api_url').'/notifications/sendnotification2/'.$s_message.'/'.$group.'/'.$fk_i_user_id,null);
            }
              
        }

        $restaurant->update($update);

        if($restaurant){
            $restaurant_row = '';
            $restaurant_row.= '<tr role="row">';
            $restaurant_row.='<td class="text-center">'.$restaurant->s_name.'</td>';
            $restaurant_row.='<td class="text-center">'.$restaurant->s_mobile_number.'</td>';
            if($restaurant->fk_i_role_id == 2){
                $restaurant_row.='<td class="text-center">'.'زبون'.'</td>';    
            }elseif ($restaurant->fk_i_role_id == 3){
                $restaurant_row.='<td class="text-center">'.'سائق'.'</td>';            
            }     
            $restaurant_row.='<td class="text-center">'.$restaurant->s_email.'</td>';

            $is_active = '';
            $is_active.='<div class="col-md-12">';
            $is_active.='<div class="md-checkbox">';
            if($restaurant->b_enabled == TRUE) {
                $is_active .= '<input type="checkbox" value="' . $restaurant->pk_i_id . '" checked id="checkbox33_' . $restaurant->pk_i_id . '" class="md-check">';
                $is_active .= '<label onclick="ch_st(' . $restaurant->pk_i_id . ')" id="label_status_' . $restaurant->pk_i_id . '" for="checkbox33_' . $restaurant->pk_i_id . '">';
                $is_active .= '<span class="inc"></span>';
                $is_active .= '<span class="check"></span>';
                $is_active .= '<span class="box"></span>مفعل';
                $is_active .= '</label>';
            }
            else {
                $is_active .= '<input type="checkbox" value="' . $restaurant->pk_i_id . '" id="checkbox33_' . $restaurant->pk_i_id . '" class="md-check">';
                $is_active .= '<label onclick="ch_st(' . $restaurant->pk_i_id . ')" id="label_status_' . $restaurant->pk_i_id . '" for="checkbox33_' . $restaurant->pk_i_id . '">';
                $is_active .= '<span class="inc"></span>';
                $is_active .= '<span class="check"></span>';
                $is_active .= '<span class="box"></span>تفعيل؟';
                $is_active .= '</label>';
            }
            $is_active .= '</div>';
            $is_active .= '</div>';

            $restaurant_row.='<td class="text-center">'.$is_active.'</td>';

            ///////////////////////////////////////////////////////////////////////
            $is_verify = '';
            $is_verify.='<div class="col-md-12">';
            $is_verify.='<div class="md-checkbox">';
            if(!empty($restaurant->dt_verified_date)) {
                $is_verify .= '<input type="checkbox" value="' . $restaurant->pk_i_id . '" checked id="checkbox32_' . $restaurant->pk_i_id . '" class="md-check">';
                $is_verify .= '<label onclick="ch_st2(' . $restaurant->pk_i_id . ')" id="label_verify_' . $restaurant->pk_i_id . '" for="checkbox32_' . $restaurant->pk_i_id . '">';
                $is_verify .= '<span class="inc"></span>';
                $is_verify .= '<span class="check"></span>';
                $is_verify .= '<span class="box"></span>مفعل';
                $is_verify .= '</label>';
            }
            else {
                $is_verify .= '<input type="checkbox" value="' . $restaurant->pk_i_id . '" id="checkbox32_' . $restaurant->pk_i_id . '" class="md-check">';
                $is_verify .= '<label onclick="ch_st2(' . $restaurant->pk_i_id . ')" id="label_verify_' . $restaurant->pk_i_id . '" for="checkbox32_' . $restaurant->pk_i_id . '">';
                $is_verify .= '<span class="inc"></span>';
                $is_verify .= '<span class="check"></span>';
                $is_verify .= '<span class="box"></span>تفعيل؟';
                $is_verify .= '</label>';
            }
            $is_verify .= '</div>';
            $is_verify .= '</div>';

            $restaurant_row.='<td class="text-center">'.$is_verify.'</td>';

            /////////////////////////////////////////////////////////////////////////

            $restaurant_row.='<td class="text-center">'.date('Y-m-d',strtotime($restaurant->dt_created_date)).'</td>';

            $restaurant_row.='<td class="text-center"><a href="#" onclick="showModal('.$restaurant->pk_i_id.')" class="btn btn-xs blue"><i class="icon-pencil"></i></a></td>';
            $restaurant_row.='<td class="text-center"><a onclick="deleteThis('.$restaurant->pk_i_id.')" id="'.$restaurant->pk_i_id.'" class="btn btn-xs red"><i class="icon-trash"></i></a></td>';
            $restaurant_row.='</tr>';

            return response()->json([
                'success'=>TRUE,
                'update_restaurant_user'=>TRUE,
                'restaurant_user_row' => $restaurant_row,
                'pk_i_id' => $restaurant->pk_i_id
            ]);
        }

    }

    public function show_edit_form(Request $request){
        if($request->has('pk_i_id')){
            $restaurant_user = User::find($request->input('pk_i_id'));
            return response()->json([
                    'success' => TRUE,
                    'page' => view('admin/users/edit')->with('restaurant_user',$restaurant_user)->render()
                ]
            );
        }
    }

    public function change_status(Request $request){
        $pk_i_id = $request->get('pk_i_id');
        $restaurant_user = User::find($pk_i_id);
        if($restaurant_user->b_enabled == 1){
            $restaurant_user->update(['b_enabled'=>0]);
            $b_enabled = 'تفعيل؟';
        }else{
            $restaurant_user->update(['b_enabled'=>1]);
            $b_enabled = 'مفعل';
        }
        return response()->json([
            'success'=>TRUE,
            'b_enabled' => $b_enabled
        ]);
    }    

    public function verify(Request $request){
        $pk_i_id = $request->get('pk_i_id');
        $restaurant_user = User::find($pk_i_id);
        if(!empty($restaurant_user->dt_verified_date)){
            $restaurant_user->update(['dt_verified_date'=>null]);
            $dt_verified_date = 'تفعيل؟';
        }else{
            $restaurant_user->update(['dt_verified_date'=>date('Y-m-d h:i:s',time())]);
            $dt_verified_date = 'مفعل';
        }
        return response()->json([
            'success'=>TRUE,
            'dt_verified_date' => $dt_verified_date
        ]);
    }

    public function delete(Request $request){
        $this->data = $request->all();
        $pk_i_id = $this->data['pk_i_id'];
        $deletedRestaurant = User::destroy($pk_i_id);
        if($deletedRestaurant){
            return response()->json([
                'success'=>TRUE,
                'deleted_Restaurant'=>TRUE,
                'restaurant_id' => $pk_i_id
            ]);
        }
    }

    public function random_string($type = 'alnum', $len = 8)
    {
        switch($type)
        {
            case 'basic'	: return mt_rand();
                break;
            case 'alnum'	:
            case 'numeric'	:
            case 'nozero'	:
            case 'alpha'	:

                switch ($type)
                {
                    case 'alpha'	:	$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        break;
                    case 'alnum'	:	$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        break;
                    case 'numeric'	:	$pool = '0123456789';
                        break;
                    case 'nozero'	:	$pool = '123456789';
                        break;
                }

                $str = '';
                for ($i=0; $i < $len; $i++)
                {
                    $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
                }
                return $str;
                break;
            case 'unique'	:
            case 'md5'		:

                return md5(uniqid(mt_rand()));
                break;
            case 'encrypt'	:
            case 'sha1'	:

                $CI =& get_instance();
                $CI->load->helper('security');

                return do_hash(uniqid(mt_rand(), TRUE), 'sha1');
                break;
        }
    }

    public function check_email(Request $request){
        $s_email = $request->input('s_email');
        $check_email = User::where('s_email', $s_email)->whereNull('dt_deleted_date')->first();
        return response()->json(array(
            'valid' => is_null($check_email),
        ));
    }

    public function check_name(Request $request){
        if (!empty($request->input('s_username'))) {
            $s_username = $request->input('s_username');
            $check_user = User::where('s_username', $s_username)->whereNull('dt_deleted_date')->first();
        }elseif(!empty($request->input('s_mobile_number'))){
            $s_mobile_number = $request->input('s_mobile_number');
            $check_user = User::where('s_mobile_number', $s_mobile_number)->whereNull('dt_deleted_date')->first();
        }
        
        
        return response()->json(array(
            'valid' => is_null($check_user),
        ));
    }

}
