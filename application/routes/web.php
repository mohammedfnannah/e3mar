<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logout', 'Auth\LoginController@logout');
Route::get('/', 'Frontend\HomeController@index');
Route::get('home', 'Frontend\HomeController@index');
Route::get('contact', 'Frontend\HomeController@contact');
Route::post('contact/save', 'Frontend\HomeController@save_contact');
Route::post('mail_list/save', 'Frontend\HomeController@save_mail_list');

Route::get('register/user', 'Frontend\HomeController@user_register');
Route::post('register/user/save', 'Frontend\HomeController@save_user_register');
Route::get('user/login', 'Frontend\HomeController@user_login_view');
Route::post('user/login', 'Frontend\HomeController@user_login');
Route::get('user/profile', 'Frontend\HomeController@user_profile');
Route::post('user/profile/edit', 'Frontend\HomeController@edit_user_profile');
Route::get('user/password', 'Frontend\HomeController@user_password');
Route::post('user/password/save', 'Frontend\HomeController@save_user_password');
Route::get('user/applications', 'Frontend\HomeController@user_applications');
Route::get('user/applications/new', 'Frontend\HomeController@new_user_applications');
Route::post('user/applications/save', 'Frontend\HomeController@save_user_applications');
Route::get('user/applications/delete/{id}', 'Frontend\HomeController@delete_user_application');

Route::get('register/company', 'Frontend\HomeController@company_register');
Route::post('register/company/save', 'Frontend\HomeController@save_company_register');
Route::get('company/profile', 'Frontend\HomeController@company_profile');
Route::post('company/profile/edit', 'Frontend\HomeController@edit_company_profile');
Route::get('company/password', 'Frontend\HomeController@company_password');
Route::post('company/password/save', 'Frontend\HomeController@save_company_password');
Route::get('company/applications', 'Frontend\HomeController@company_applications');
Route::get('company/current/applications', 'Frontend\HomeController@company_current_applications');
Route::get('company/applications/archive', 'Frontend\HomeController@company_archive_applications');
Route::get('company/contracts', 'Frontend\HomeController@company_contracts');
Route::get('company/contracts/new/{application_id}', 'Frontend\HomeController@new_company_contracts');
Route::post('company/contracts/new', 'Frontend\HomeController@save_new_company_contracts');

Route::get('page/{id}', 'Frontend\HomeController@page_by_id');

Route::get('news', 'Frontend\HomeController@news');
Route::get('news/{id}', 'Frontend\HomeController@news_by_id');

Route::get('services', 'Frontend\HomeController@services');
Route::get('services/{id}', 'Frontend\HomeController@service_by_id');
Route::get('services/{id}/{city_id}', 'Frontend\HomeController@service_by_id');
Route::get('facility/{id}', 'Frontend\HomeController@facility_by_id');

Route::get('partners', 'Frontend\HomeController@partners');
Route::get('partners/{id}', 'Frontend\HomeController@partner_by_id');

//Route::get('page/{title}', 'Frontend\HomeController@page_by_title');
//Route::get('service/{title}', 'Frontend\HomeController@service_by_title');
//Route::get('partners/{title}', 'Frontend\HomeController@partner_by_title');

Route::get('consultation', 'Frontend\HomeController@consultation');
Route::post('consultation/save', 'Frontend\HomeController@save_consultation');

Route::get('evaluation', 'Frontend\HomeController@evaluation');
Route::post('evaluation/save', 'Frontend\HomeController@save_evaluation');

Auth::routes();

Route::group(['middleware' => 'web','prefix' => config('app.prefix','admin')], function () {

    Route::get('/', 'Backend\HomeController@index');

    Route::get('roles', 'Backend\EntrustController@roles');
    Route::get('roles/data', 'Backend\EntrustController@rolesdata');
    Route::post('roles/add', 'Backend\EntrustController@addrole');
    Route::post('roles/show', 'Backend\EntrustController@showeditroleform');
    Route::post('roles/update', 'Backend\EntrustController@editrole');
    Route::post('roles/delete', 'Backend\EntrustController@destroyrole');
    Route::get('roles/check/name', 'Backend\EntrustController@checkerolename');

    Route::get('permissions', 'Backend\EntrustController@permissions');
    Route::get('permissions/data', 'Backend\EntrustController@permissionsdata');
    Route::post('permissions/show', 'Backend\EntrustController@showeditpermissionform');
    Route::post('permissions/update', 'Backend\EntrustController@editpermission');
    Route::get('permissions/routes/update', 'Backend\EntrustController@update_permations');

    Route::get('administrators', 'Backend\AdministratorsController@index');
    Route::get('administrators/data', 'Backend\AdministratorsController@data');
    Route::post('administrators/add', 'Backend\AdministratorsController@add');
    Route::post('administrators/show', 'Backend\AdministratorsController@show_edit_form');
    Route::post('administrators/update', 'Backend\AdministratorsController@update');
    Route::post('administrators/delete', 'Backend\AdministratorsController@delete');
    Route::post('administrators/check/name', 'Backend\AdministratorsController@check_name');
    Route::post('administrators/check/email', 'Backend\AdministratorsController@check_email');
    Route::post('administrators/change_status', 'Backend\AdministratorsController@change_status');

    Route::get('contact', 'Backend\ContactusController@index');
    Route::get('contact/data', 'Backend\ContactusController@data');
    Route::post('contact/show', 'Backend\ContactusController@show_edit_form');
    Route::post('contact/delete', 'Backend\ContactusController@delete');

    Route::get('mai_list', 'Backend\MailstController@index');
    Route::get('mai_list/data', 'Backend\MailstController@data');
    Route::post('mai_list/delete', 'Backend\MailstController@delete');

    Route::get('services', 'Backend\ServicesController@index');
    Route::get('services/data', 'Backend\ServicesController@data');
    Route::post('services/add', 'Backend\ServicesController@add');
    Route::post('services/show', 'Backend\ServicesController@show_edit_form');
    Route::post('services/update', 'Backend\ServicesController@update');
    Route::post('services/delete', 'Backend\ServicesController@delete');
    Route::post('services/change_status', 'Backend\ServicesController@change_status');
    Route::get('services/check/name', 'Backend\ServicesController@check_name');

    Route::get('banners', 'Backend\BannersController@index');
    Route::get('banners/data', 'Backend\BannersController@data');
    Route::post('banners/add', 'Backend\BannersController@add');
    Route::post('banners/show', 'Backend\BannersController@show_edit_form');
    Route::post('banners/update', 'Backend\BannersController@update');
    Route::post('banners/delete', 'Backend\BannersController@delete');
    Route::post('banners/change_status', 'Backend\BannersController@change_status');

    Route::get('pages', 'Backend\PagesController@index');
    Route::get('pages/data', 'Backend\PagesController@data');
    Route::post('pages/add', 'Backend\PagesController@add');
    Route::post('pages/show', 'Backend\PagesController@show_edit_form');
    Route::post('pages/update', 'Backend\PagesController@update');
    Route::post('pages/delete', 'Backend\PagesController@delete');
    Route::post('pages/change_status', 'Backend\PagesController@change_status');

    Route::get('partners', 'Backend\PartnersController@index');
    Route::get('partners/data', 'Backend\PartnersController@data');
    Route::post('partners/add', 'Backend\PartnersController@add');
    Route::post('partners/show', 'Backend\PartnersController@show_edit_form');
    Route::post('partners/update', 'Backend\PartnersController@update');
    Route::post('partners/delete', 'Backend\PartnersController@delete');
    Route::post('partners/change_status', 'Backend\PartnersController@change_status');

    Route::get('polls', 'Backend\PollsController@index');
    Route::get('polls/data', 'Backend\PollsController@data');
    Route::post('polls/add', 'Backend\PollsController@add');
    Route::post('polls/show', 'Backend\PollsController@show_edit_form');
    Route::post('polls/update', 'Backend\PollsController@update');
    Route::post('polls/delete', 'Backend\PollsController@delete');
    Route::post('polls/change_status', 'Backend\PollsController@change_status');

    Route::get('news', 'Backend\NewsController@index');
    Route::get('news/data', 'Backend\NewsController@data');
    Route::post('news/add', 'Backend\NewsController@add');
    Route::post('news/show', 'Backend\NewsController@show_edit_form');
    Route::post('news/update', 'Backend\NewsController@update');
    Route::post('news/delete', 'Backend\NewsController@delete');
    Route::post('news/change_status', 'Backend\NewsController@change_status');

    Route::get('settings/', 'Backend\SettingsController@index');
    Route::post('settings/save', 'Backend\SettingsController@update');

});
