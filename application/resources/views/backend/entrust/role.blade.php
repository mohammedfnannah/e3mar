@extends('backend.layouts.app')
@section('css')
@endsection
@section('js')
<script>
    $(document).ready(function() {
        $('body').on('change','#add-forrm .parent_ch',function(){
            var val_name = $(this).data('name');
            if(this.checked){
                $('#add-forrm .'+val_name).each(function(){
                    $('#add-forrm .'+val_name).parent().addClass('checked');
                    console.log($(this));
                });
            }else{
               $('#add-forrm .'+val_name).each(function(){
                    $('#add-forrm .'+val_name).parent().removeClass('checked');
                    console.log($(this));
                }); 
            }
            $('#add-forrm .'+val_name).prop('checked', this.checked);
            $('#add-forrm .'+val_name)[0].checked = this.checked;
        });
        $('body').on('change','#edit-forrm .parent_ch',function(){
            var val_name = $(this).data('name');
            if(this.checked){
                $('#edit-forrm .'+val_name).each(function(){
                    $('#edit-forrm .'+val_name).parent().addClass('checked');
                    console.log($(this));
                });
            }else{
               $('#edit-forrm .'+val_name).each(function(){
                    $('#edit-forrm .'+val_name).parent().removeClass('checked');
                    console.log($(this));
                }); 
            }
            $('#edit-forrm .'+val_name).prop('checked', this.checked);
            $('#edit-forrm .'+val_name)[0].checked = this.checked;
        });

        var TABLE = $('#roles_table');
        var Add_Form = $('#add_role_form');
        var Edit_Form = $('#edit_role_form');
        var Modal_add = $('#add_role');
        var Modal_edit = $('#edit_role');
        TABLE.DataTable({
            processing: true,
            serverSide: true,
            "aaSorting": [ [0,'ASC'] ],
            "language": {
                "sProcessing":   "جارٍ التحميل...",
                "sLengthMenu":   "أظهر _MENU_ مدخلات",
                "sZeroRecords":  "لم يعثر على أية سجلات",
                "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
                "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                "sInfoPostFix":  "",
                "sSearch":       "ابحث:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "الأول",
                    "sPrevious": "السابق",
                    "sNext":     "التالي",
                    "sLast":     "الأخير"
                }
            },
            ajax: '{!! url('admin/roles/data') !!}',
            columns: [
                {className: 'text-center', data: 'name', name: 'name', searchable: true,orderable: true},
                {className: 'text-center', data: 'display_name', name: 'display_name', searchable: true},
                {className: 'text-center', data: 'description', name: 'description', searchable: true,orderable: true},
                {className: 'text-center', data: 'edit_action',name: 'edit_action',orderable: false,searchable: false},
                {className: 'text-center', data: 'delete_action', name: 'delete_action', orderable: false, searchable: false}
            ]
        });

        $('.add_role').click(function(e){
            e.preventDefault();
            postData(Add_Form);
        });

        Modal_add.on('shown.bs.modal',function(e){
            Add_Form.bootstrapValidator({
                message: '',
                live: true,
                feedbackIcons: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                },
                fields: {
                    display_name: {
                        message: 'اسم المهمة ',
                        validators: {
                            notEmpty: {
                                message: 'هذا الحقل مطلوب'
                            },
                            stringLength: {
                                min: 2,
                                max: 50,
                                message: 'يجب ان يكون الاسم من حرفين و حتى خمسين حرف'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z_1-9\u0600-\u06FF\u0020]+$/,
                                message: 'اسم المهمة يحتوي فقط على ارقام وحروف'
                            }
                        }
                    },
                    name: {
                        message: 'المهمة - انجليزي',
                        validators: {
                            notEmpty: {
                                message: 'هذا الحقل مطلوب'
                            },
                            stringLength: {
                                min: 2,
                                max: 50,
                                message: 'يجب ان يكون اسم المهمة - انجليزي من حرفين و حتى خمسين حرف'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z_1-9\u0600-\u06FF\u0020]+$/,
                                message: 'المهمة - انجليزي يحتوي فقط على ارقام وحروف'
                            },
                            remote: {
                                url: '{!! url('admin/roles/check/name') !!}',
                                data: function(validator) {
                                    return {
                                        name: validator.getFieldElements('name').val(),

                                    };
                                },
                                message: 'المهمة - انجليزي موجود مسبقا'
                            }
                        }
                    },
                }
            });
            $('#add_role_form').bootstrapValidator('resetForm');
        });

    });

    function showModal(id){
        $("html, body").animate({
            scrollTop: 0
        }, 600);

        if(id == null){
            $('#add_role .modal-title').html('إضافة مهمة ');
            $('#add_role').modal('show', {backdrop: 'static'});
        }
        else{
            $.post('{{ url('admin/roles/show')}}', {'id':id,'_token':'{!! csrf_token() !!}'} , function(data) {
                if(data.success) {
                    $('#edit_role').html(data.page).modal('show');
                    $('#edit_role .modal-title').html('تعديل مهمة');
                    validateForm();
                }else{
                    $('#edit_role').html('').modal('hide');
                    showAlertMessage('alert-danger', 'خطأ !', 'خطأ في الصفحة');

                }
            }, 'json');
        }
    }

    function validateForm(){

        $('#edit_role_form').bootstrapValidator({
            message: '',
            live: true,
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            },
            fields: {
                display_name: {
                    message: 'اسم المهمة ',
                    validators: {
                        notEmpty: {
                            message: 'هذا الحقل مطلوب'
                        },
                        stringLength: {
                            min: 2,
                            max: 50,
                            message: 'يجب ان يكون الاسم من حرفين و حتى خمسين حرف'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z_1-9\u0600-\u06FF\u0020]+$/,
                            message: 'اسم المهمة يحتوي فقط على ارقام وحروف'
                        }
                    }
                },
                name: {
                    message: 'المهمة - انجليزي',
                    validators: {
                        notEmpty: {
                            message: 'هذا الحقل مطلوب'
                        },
                        stringLength: {
                            min: 2,
                            max: 50,
                            message: 'يجب ان يكون اسم المهمة - انجليزي من حرفين و حتى خمسين حرف'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z_1-9\u0600-\u06FF\u0020]+$/,
                            message: 'المهمة - انجليزي يحتوي فقط على ارقام وحروف'
                        },
                    }
                },
            }
        });

        $('#edit_role_form').bootstrapValidator('resetForm');



        $('body').on('click','.edit_role',function(e){
            e.preventDefault();
            postEditableData($('#edit_role_form'));
        });
    }

    function deleteThis(id){
        if(!confirm('هل انت متاكد؟')){
            return false;
        }else {
            $.ajax({
                url: '{{url('admin/roles/delete')}}',
                data: {id: id, _token: '{!! csrf_token() !!}'},
                type: "POST",
                success: function (data, textStatus, jqXHR) {
                    var id = data.role_id;
                    $('#'+id).closest('tr').remove();
                    showAlertMessage('alert-success', 'ادارة المهام / ', 'تم حذف المهام بنجاح');
                },
                error: function (data, textStatus, jqXHR) {
                    console.log(data);
                },
                statusCode: {
                    500: function (data) {
                        console.log(data);
                    }
                }
            });
        }
    }

    function postData(form){
        form.data('bootstrapValidator').validate();
        if(!form.data('bootstrapValidator').isValid()){
            return;
        }
        var data = form.serializeArray();
        $.ajax({
            url : '{{ url('admin/roles/add') }}',
            data : data,
            type: "POST",
            success:function(data) {
                if(data.success){
                    $('#add_role').modal('hide');
                    $('#roles_table > tbody > tr:first').before(data.role_row);
                    showAlertMessage('alert-success',' / ', 'تم اضافة المهام بنجاح');
                }else{
                    showAlertMessage('alert-danger','ادارة المهام / ', ' حدث خطا أثناء الاضافة ! حاول مجددا');
                }
            },
            error:function(data) {
                console.log(data);
            } ,
            statusCode: {
                500: function(data) {
                    console.log(data);
                }
            }
        });
    }

    function postEditableData(form){
        var data = form.serializeArray();
        $.ajax({
            url : '{{ url('admin/roles/update') }}',
            data : data,
            type: "POST",
            success:function(data) {
                if(data.success){
                    $('#edit_role').modal('hide');
                    $('#'+data.id).closest('tr').replaceWith(data.role_row);
                    showAlertMessage('alert-success','ادارة المهام / ', 'تم تعديل بيانات المهام بنجاح');
                }else{
                    showAlertMessage('alert-danger',' / ', ' حدث خطا أثناء التعديل ! حاول مجددا');
                }
            },
            error:function(data) {
                console.log(data);
            } ,
            statusCode: {
                500: function(data) {
                    console.log(data);
                }
            }
        });
    }

</script>
@endsection


@section('content')


    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                <span>المهام</span>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/'.config('app.prefix','admin')) }}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
                <li class="active">المهام</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="modal fadeIn" id="edit_role" data-width="760" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    </div>
                    <div class="modal fadeIn" id="add_role" data-width="800" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- BEGIN SAMPLE FORM PORTLET-->
                                            <div class="portlet light bordered">
                                                {!! FORM::open(['url'=>url('admin/entrust/addrole'),'class'=>'form-horizontal','role'=>'form','id'=>'add_role_form']) !!}
                                                <div class="portlet-body form">
                                                    <div class="col-md-12">
                                                        <div class="form-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-4 control-label" for="name">المهمة (انجليزي اجباري)</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control" name="name" id="name" placeholder="المهمة - انجليزي">
                                                                            <div class="form-control-focus">
                                                                            </div>
                                                                            <span class="help-block"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-4 control-label" for="display_name">اسم المهمة</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control" name="display_name" id="display_name" placeholder="اسم المهمة">
                                                                            <span class="help-block"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-4 control-label" for="description">الوصف</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control" name="description" id="description" placeholder="الوصف">
                                                                            <span class="help-block"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-12" id="add-forrm" >
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-2 control-label" for="description">الصلاحيات</label>
                                                                        <div class="col-md-10">
                                                                            {{--<pre>{{ print_r($permission->toArray()) }}</pre>--}}
                                                                            <ul style="list-style: none;-webkit-column-count: 2;-moz-column-count: 2;column-count: 2;">
                                                                                @if(isset($permission) && !empty($permission) && count($permission)>0 )
                                                                                    <?php $counter = 0; ?>
                                                                                    @foreach($permission as $row)
                                                                                        <?php $counter++; ?>
                                                                                        @if($counter == 1)
                                                                                            <li >
                                                                                                <input checked="checked" type="checkbox" id="{{ $row->id }}" name="permissions[]" <?php if(strpos($row->name, '-') === false){ $classname = str_replace('-',' ',$row->name); echo ' class="parent_ch" data-name="'.$classname.'" '; }elseif(isset($classname) && !empty($classname)){ echo ' class="'.$classname.'" '; } ?>   value="{{ $row->id }}"><label  <?php if(strpos($row->name, '-') === false){  echo ' style="font-weight: bold !important;
            font-size: 18px;color: #288f85;" ';  } ?> for="{{ $row->id }}">@if(!empty($row->display_name)){{ $row->display_name }} @else {{ str_replace('-',' ',$row->name)  }} @endif</label><br>
                                                                                            </li>
                                                                                        @else
                                                                                            <li >
                                                                                                <input checked="checked"  type="checkbox" id="{{ $row->id }}" name="permissions[]" <?php if(strpos($row->name, '-') === false){ $classname = str_replace('-',' ',$row->name); echo ' class="parent_ch" data-name="'.$classname.'" '; }elseif(isset($classname) && !empty($classname)){ echo ' class="'.$classname.'" '; } ?>   value="{{ $row->id }}"><label  <?php if(strpos($row->name, '-') === false){  echo ' style="font-weight: bold !important;
            font-size: 18px;color: #288f85;margin-top:30px;" ';  } ?> for="{{ $row->id }}">@if(!empty($row->display_name)){{ $row->display_name }} @else {{ str_replace('-',' ',$row->name)  }} @endif</label><br>
                                                                                            </li>
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                {!! FORM::close() !!}
                                            </div>
                                            <!-- END SAMPLE FORM PORTLET-->
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary id blue add_role">حفظ</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-default">اغلاق</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a  onclick="showModal()"  class="btn btn-info btn-flat">
                        <i class="fa fa-plus"></i> اضافة جديد
                    </a>
                </div>
                <div class="col-xs-12" style="margin-top: 15px;">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-striped table-bordered table-hover" id="roles_table">
                                <thead>
                                <tr>
                                    <th class="text-center">المهمة - انجليزي</th>
                                    <th class="text-center">اسم المهمة</th>
                                    <th class="text-center">الوصف</th>
                                    <th class="text-center">تعديل</th>
                                    <th class="text-center">حذف</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection