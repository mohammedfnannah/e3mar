@extends('backend.layouts.app')

@section('css')
@endsection

@section('js')
<script>
    $(document).ready(function() {
        var TABLE = $('#permissions_table');
        var Edit_Form = $('#edit_permission_form');
        var Modal_edit = $('#edit_permission');
        TABLE.DataTable({
            processing: true,
            serverSide: true,
            "aaSorting": [ [0,'ASC'] ],
            "language": {
                "sProcessing":   "جارٍ التحميل...",
                "sLengthMenu":   "أظهر _MENU_ مدخلات",
                "sZeroRecords":  "لم يعثر على أية سجلات",
                "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
                "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                "sInfoPostFix":  "",
                "sSearch":       "ابحث:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "الأول",
                    "sPrevious": "السابق",
                    "sNext":     "التالي",
                    "sLast":     "الأخير"
                }
            },
            ajax: '{!! url('admin/permissions/data') !!}',
            columns: [
                {className: 'text-center', data: 'name', name: 'name', searchable: true,orderable: true},
                {className: 'text-center', data: 'display_name', name: 'display_name', searchable: true},
                {className: 'text-center', data: 'description', name: 'description', searchable: true,orderable: true},
                {className: 'text-center', data: 'edit_action',name: 'edit_action',orderable: false,searchable: false},
            ]
        });

        $('body').on('click','.edit_permission',function(e){
            e.preventDefault();
            postEditableData($('#edit_permission_form'));
        });

    });
    function showModal(id){
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        $.post('{{ url('admin/permissions/show')}}', {'id':id,'_token':'{!! csrf_token() !!}'} , function(data) {
            if(data.success) {
                $('#edit_permission').html(data.page).modal('show');
                $('#edit_permission .modal-title').html('تعديل بيانات الصلاحية ');
            }else{
                showAlertMessage('alert-danger', 'خطأ !', 'خطأ في الصفحة');

            }
        }, 'json');
    }

    function postEditableData(form){
        var data = form.serializeArray();
        $.ajax({
            url : '{{ url('admin/permissions/update') }}',
            data : data,
            type: "POST",
            success:function(data) {
                if(data.success){
                    $('#edit_permission').modal('hide');
                    $('#'+data.id).closest('tr').replaceWith(data.permission_row);
                    showAlertMessage('alert-success','ادارة الصلاحيات / ', 'تم تعديل بيانات الصلاحيات بنجاح');
                }else{
                    showAlertMessage('alert-danger',' / ', ' حدث خطا أثناء التعديل ! حاول مجددا');
                }
            },
            error:function(data) {
                console.log(data);
            } ,
            statusCode: {
                500: function(data) {
                    console.log(data);
                }
            }
        });
    }

</script>
@endsection


@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                <span>الصلاحيات</span>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/'.config('app.prefix','admin')) }}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
                <li class="active">الصلاحيات</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <div class="modal fade fadeIn" id="edit_permission" data-width="760" tabindex="-1" permission="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>

                    <a href="{{ url(config('app.prefix','admin').'/permissions/routes/update') }}" class="btn btn-info btn-flat">
                        <i class="fa fa-refresh"></i> تحديث الصلاحيات
                    </a>
                </div>
                <div class="col-xs-12" style="margin-top: 15px;">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-striped table-bordered table-hover" id="permissions_table">
                                <thead>
                                <tr>
                                    <th class="text-center">الصلاحية - انجليزي</th>
                                    <th class="text-center">اسم الصلاحية</th>
                                    <th class="text-center">الوصف</th>
                                    <th class="text-center">تعديل</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    
@endsection