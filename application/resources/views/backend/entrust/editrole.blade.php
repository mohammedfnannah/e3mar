<div class="modal-dialog">
    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                        {!! FORM::open(['url'=>url('admin/entrust/editrole'),'class'=>'form-horizontal','role'=>'form','id'=>'edit_role_form']) !!}
                        <div class="portlet-body form">
                            <div class="col-md-12">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-4 control-label" for="name">المهمة (انجليزي اجباري)</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="name" value="{{ $role->name }}"  id="name" placeholder="المهمة - انجليزي">
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-4 control-label" for="display_name">اسم المهمة</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="display_name" value="{{ $role->display_name }}"  id="display_name" placeholder="اسم المهمة">
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-4 control-label" for="description">الوصف</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="description" value="{{ $role->description }}"  id="description" placeholder="الوصف">
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12" id="edit-forrm" >
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-2 control-label" for="description">الصلاحيات</label>
                                                <div class="col-md-10">
                                                    {{--<pre>{{ print_r($permission->toArray()) }}</pre>--}}
                                                    <ul style="list-style: none;-webkit-column-count: 2;-moz-column-count: 2;column-count: 2;">
                                                        @if(isset($permission) && !empty($permission) && count($permission)>0 )
                                                            <?php $counter = 0; ?>
                                                            @foreach($permission as $row)
                                                                <?php $counter++; ?>
                                                                @if($counter == 1)
                                                                    <li >
                                                                        <input <?php if(in_array($row->id,$role_permission)){ echo ' checked '; } ?>  type="checkbox" id="{{ $row->id }}" name="permissions[]" <?php if(strpos($row->name, '-') === false){ $classname=str_replace('-',' ',$row->name); echo ' class="parent_ch" data-name="'.$classname.'" '; }else{ echo ' class="'.$classname.'" '; } ?>  value="{{ $row->id }}"><label  <?php if(strpos($row->name, '-') === false){  echo ' style="font-weight: bold !important;
            font-size: 18px;color: #288f85;" ';  } ?> for="{{ $row->id }}">@if(!empty($row->display_name)){{ $row->display_name }} @else {{ str_replace('-',' ',$row->name)  }} @endif</label><br>
                                                                    </li>
                                                                @else
                                                                    <li >
                                                                        <input <?php if(in_array($row->id,$role_permission)){ echo ' checked '; } ?> type="checkbox" id="{{ $row->id }}" name="permissions[]" <?php if(strpos($row->name, '-') === false){ $classname=str_replace('-',' ',$row->name); echo ' class="parent_ch" data-name="'.$classname.'" '; }else{ echo ' class="'.$classname.'" '; } ?>   value="{{ $row->id }}"><label  <?php if(strpos($row->name, '-') === false){  echo ' style="font-weight: bold !important;
            font-size: 18px;color: #288f85;margin-top:30px;" ';  } ?> for="{{ $row->id }}">@if(!empty($row->display_name)){{ $row->display_name }} @else {{ str_replace('-',' ',$row->name)  }} @endif</label><br>
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <input type="hidden" name="id" value="{{ $role->id }}">
                        </div>
                        {!! FORM::close() !!}
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary blue edit_role" id="edit_role_submit">حفظ</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">اغلاق</button>
        </div>

    </div>
</div>