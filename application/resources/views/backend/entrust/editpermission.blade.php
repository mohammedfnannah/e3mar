<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            {!! FORM::open(['url'=>url('admin/entrust/editpermission'),'class'=>'form-horizontal','permission'=>'form','id'=>'edit_permission_form']) !!}
                            <div class="portlet-body form">
                                <div class="col-md-12">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-md-line-input has-success">
                                                    <label class="col-md-4 control-label" for="name">الصلاحية (انجليزي اجباري)</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" disabled name="name" value="{{ $permission->name }}"  id="name" placeholder="الصلاحية - انجليزي">
                                                        <div class="form-control-focus">
                                                        </div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group form-md-line-input has-success">
                                                    <label class="col-md-4 control-label" for="display_name">اسم الصلاحية</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="display_name" value="{{ $permission->display_name }}"  id="display_name" placeholder="اسم الصلاحية">
                                                        <div class="form-control-focus">
                                                        </div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group form-md-line-input has-success">
                                                    <label class="col-md-4 control-label" for="description">الوصف</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="description" value="{{ $permission->description }}"  id="description" placeholder="الوصف">
                                                        <div class="form-control-focus">
                                                        </div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <input type="hidden" name="id" value="{{ $permission->id }}">
                            </div>
                            {!! FORM::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary blue edit_permission">حفظ</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">اغلاق</button>
        </div>
    </div>
</div>



