@extends('backend.layouts.app')
@section('css')
@endsection

@section('js')
<script>
    $(document).ready(function() {
        var TABLE = $('#object_table');
        TABLE.DataTable({
            processing: true,
            serverSide: true,
            "aaSorting": [ [0,'desc'] ],
            "language": {
                "sProcessing":   "جارٍ التحميل...",
                "sLengthMenu":   "أظهر _MENU_ مدخلات",
                "sZeroRecords":  "لم يعثر على أية سجلات",
                "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
                "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                "sInfoPostFix":  "",
                "sSearch":       "ابحث:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "الأول",
                    "sPrevious": "السابق",
                    "sNext":     "التالي",
                    "sLast":     "الأخير"
                }
            },
            ajax: '{!! url(config('app.prefix','admin').'/mai_list/data') !!}',
            columns: [
                {className: 'text-center', data: 'id', name: 'id', searchable: true,orderable: true},
                {className: 'text-center', data: 'email', name: 'email', searchable: true,orderable: true},
                {className: 'text-center', data: 'created_at', name: 'created_at', searchable: false,orderable: true},
                {className: 'text-center', data: 'delete_action', name: 'delete_action', orderable: false, searchable: false}
            ]
        });

    });
    
    function deleteThis(id){
        if(!confirm('هل انت متاكد؟')){
            return false;
        }else {
            $.ajax({
                url: '{{url(config('app.prefix','admin').'/mai_list/delete')}}',
                data: {id: id, _token: '{!! csrf_token() !!}'},
                type: "POST",
                success: function (data, textStatus, jqXHR) {
                    var id = data.restaurant_id;
//                        console.log('#cards_table > tbody > tr[id='+id+']');
                    $('#'+id).closest('tr').remove();
//                    $('#restaurants_table > tbody > tr[id='+id+']').remove();
                    showAlertMessage('alert-success', 'تمت عملية الحذف بنجاح');
//                    setTimeout(function(){location.reload()},1000);
                },
                error: function (data, textStatus, jqXHR) {
                    console.log(data);
                },
                statusCode: {
                    500: function (data) {
                        console.log(data);
                    }
                }
            });
        }
    }

</script>
@endsection


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                <span>القائمة البريدية</span>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/'.config('app.prefix','admin')) }}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
                <li class="active">القائمة البريدية</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12" style="margin-top: 15px;">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-striped table-bordered table-hover" id="object_table">
                                <thead>
                                <tr>
                                    <th width="5%" class="text-center">#</th>
                                    <th width="40%" class="text-center">البريد الإلكتروني</th>
                                    <th width="10%" class="text-center">التاريخ</th>
                                    <th width="5%" class="text-center">حذف</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>




@endsection