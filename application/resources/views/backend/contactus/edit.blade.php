<div class="modal-dialog">
    <div class="modal-content" style="width: 800px;">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <div class="col-md-12">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-5 control-label" for="s_mobile_number">اسم المرسل</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" disabled value="{{ $contactus->name }}" >
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-5 control-label" for="s_password">رقم الجوال</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" name="s_title" disabled value="{{ $contactus->mobile }}" >
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-5 control-label" for="s_password">البريد الالكتروني</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" name="s_title" disabled value="{{ $contactus->email }}" >
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-5 control-label" for="dt_created_date">التاريخ والوقت</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" disabled value="{{ $contactus->created_at }}"  >
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-2 control-label" for="subject">العنوان</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" disabled value="{{ $contactus->subject }}"  >
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-2 control-label" for="fk_i_role_id">الرسالة</label>
                                                <div class="col-md-10">
                                                    <textarea  class="form-control"  name="s_description" disabled>{{ $contactus->message }}</textarea>
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">اغلاق</button>
        </div>


    </div>
</div>