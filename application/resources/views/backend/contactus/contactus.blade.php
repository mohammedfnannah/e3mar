@extends('backend.layouts.app')
@section('css')
@endsection

@section('js')
<script>
    $(document).ready(function() {
        var TABLE = $('#contact_us_table');
        var Add_Form = $('#add_contact_us_form');
        var Edit_Form = $('#edit_contact_us_form');
        var Modal_add = $('#add_contact_us');
        var Modal_edit = $('#edit_contact_us');
        TABLE.DataTable({
            processing: true,
            serverSide: true,
            "aaSorting": [ [0,'desc'] ],
            "language": {
                "sProcessing":   "جارٍ التحميل...",
                "sLengthMenu":   "أظهر _MENU_ مدخلات",
                "sZeroRecords":  "لم يعثر على أية سجلات",
                "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
                "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                "sInfoPostFix":  "",
                "sSearch":       "ابحث:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "الأول",
                    "sPrevious": "السابق",
                    "sNext":     "التالي",
                    "sLast":     "الأخير"
                }
            },
            ajax: '{!! url('admin/contact/data') !!}',
            columns: [
                {className: 'text-center', data: 'id', name: 'id', searchable: true,orderable: true},
                {className: 'text-center', data: 'name', name: 'name', searchable: true,orderable: true},
                {className: 'text-center', data: 'email', name: 'email', searchable: true,orderable: true},
                {className: 'text-center', data: 'mobile', name: 'mobile', searchable: true,orderable: true},
                {className: 'text-center', data: 'subject', name: 'subject', searchable: true,orderable: true},
                {className: 'text-center', data: 'created_at', name: 'created_at', searchable: false,orderable: true},
                {className: 'text-center', data: 'edit_action',name: 'edit_action',orderable: false,searchable: false},
                {className: 'text-center', data: 'delete_action', name: 'delete_action', orderable: false, searchable: false}
            ]
        });

    });

    function showModal(id){
        $("html, body").animate({
            scrollTop: 0
        }, 600);

        if(id == null){
            $('#add_contact_us .modal-title').html('اضافة مدير لوحة ');
            $('#add_contact_us').modal('show', {backdrop: 'static'});
        }
        else{
            $.post('{{ url('admin/contact/show')}}', {'id':id,'_token':'{!! csrf_token() !!}'} , function(data) {
                if(data.success) {
                    $('#edit_contact_us').html(data.page).modal('show');
                    $('#edit_contact_us .modal-title').html('عرض بيانات رسالة تواصل معنا ');
                    validateForm();
                }else{
                    showAlertMessage('alert-danger', 'خطأ !', 'خطأ في الصفحة');

                }
            }, 'json').error(function(error){
                showAlertMessage('alert-danger', 'Fatal error !', 'An unknown error occured !');
            });
        }
    }
    
    function deleteThis(id){
        if(!confirm('هل انت متاكد؟')){
            return false;
        }else {
            $.ajax({
                url: '{{url('admin/contact/delete')}}',
                data: {id: id, _token: '{!! csrf_token() !!}'},
                type: "POST",
                success: function (data, textStatus, jqXHR) {
                    var id = data.restaurant_id;
//                        console.log('#cards_table > tbody > tr[id='+id+']');
                    $('#'+id).closest('tr').remove();
//                    $('#restaurants_table > tbody > tr[id='+id+']').remove();
                    showAlertMessage('alert-success', 'ادارة رسالة تواصل معناين / ', 'تم حذف رسالة تواصل معنا بنجاح');
//                    setTimeout(function(){location.reload()},1000);
                },
                error: function (data, textStatus, jqXHR) {
                    console.log(data);
                },
                statusCode: {
                    500: function (data) {
                        console.log(data);
                    }
                }
            });
        }
    }

</script>
@endsection


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                <span>رسائل تواصل معنا</span>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/'.config('app.prefix','admin')) }}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
                <li class="active">رسائل تواصل معنا</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <div class="modal fadeIn" id="edit_contact_us" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>

                </div>
                <div class="col-xs-12" style="margin-top: 15px;">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-striped table-bordered table-hover" id="contact_us_table">
                                <thead>
                                <tr>
                                    <th width="5%" class="text-center">#</th>
                                    <th width="15%" class="text-center">اسم المرسل</th>
                                    <th width="15%" class="text-center">البريد الإلكتروني</th>
                                    <th width="15%" class="text-center">رقم الجوال</th>
                                    <th width="30%" class="text-center">عنوان الرسالة</th>
                                    <th width="10%" class="text-center">التاريخ</th>
                                    <th width="5%" class="text-center">عرض</th>
                                    <th width="5%" class="text-center">حذف</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>




@endsection