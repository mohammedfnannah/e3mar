@extends('admin.layout.layout')
@section('css')
    {!! HTML::style('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap-rtl.css') !!}
    {!! HTML::style('assets/global/plugins/select2/select2.css') !!}
    {!! HTML::style('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') !!}
    {!! HTML::style('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') !!}
    {!! HTML::style('assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css') !!}
    {!! HTML::style('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') !!}
@endsection

@section('js')
    {!! HTML::script('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! HTML::script('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}
    {!! HTML::script('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}

    {!! HTML::script('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') !!}
    {!! HTML::script('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') !!}

    {!! HTML::script('assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js') !!}
    {!! HTML::script('assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js') !!}
    {!! HTML::script('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
    {!! HTML::script('assets/global/plugins/bootstrap-validator/bootstrapValidator.min.js') !!}
    {!! HTML::script('assets/public/js/jquery.validate.js') !!}

    <!--[if (gte IE 8)&(lt IE 10)]>
{!! HTML::script('assets/global/plugins/jquery-file-upload/jquery.iframe-transport.js') !!}
<![endif]-->
<script>
    $(document).ready(function() {
        var TABLE = $('#restaurants_user_table');
        var Add_Form = $('#add_restaurant_user_form');
        var Edit_Form = $('#edit_restaurant_user_form');
        var Modal_add = $('#add_restaurant_user');
        var Modal_edit = $('#edit_restaurant_user');
        TABLE.DataTable({
            processing: true,
            serverSide: true,
            order: [ 0, "desc" ],
            ajax: '{!! url('admin/users/data') !!}',
            columns: [
                {className: 'text-center', data: 'pk_i_id', name: 'pk_i_id', searchable: true},
                {className: 'text-center', data: 's_name', name: 's_name', searchable: true},
                {className: 'text-center', data: 's_mobile_number', name: 's_mobile_number', searchable: true},
                {className: 'text-center', data: 'fk_i_role_id', name: 'fk_i_role_id', searchable: false},
                {className: 'text-center', data: 'i_order_count', name: 'i_order_count', searchable: false},
                {className: 'text-center', data: 's_email', name: 's_email', searchable: true},
                {className: 'text-center', data: 'b_enabled', name: 'b_enabled', searchable: false},
                {className: 'text-center', data: 'dt_verified_date', name: 'dt_verified_date', searchable: false},
                {className: 'text-center', data: 'dt_created_date', name: 'dt_created_date', searchable: false},
                {className: 'text-center', data: 'edit_action',name: 'edit_action',orderable: false,searchable: false},
                {className: 'text-center', data: 'delete_action', name: 'delete_action', orderable: false, searchable: false}
            ]
        });

        $('.add_restaurant_user').click(function(e){
            e.preventDefault();
            postData(Add_Form);
        });

        Modal_add.on('shown.bs.modal',function(e){
            Add_Form.bootstrapValidator({
                message: '',
                live: true,
                feedbackIcons: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                },
                fields: {
                    s_name: {
                        message: 'اسم المستخدم ',
                        validators: {
                            notEmpty: {
                                message: 'هذا الحقل مطلوب'
                            },
                            stringLength: {
                                min: 2,
                                max: 50,
                                message: 'يجب ان يكون الاسم من حرفين و حتى خمسين حرف'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z_1-9\u0600-\u06FF\u0020]+$/,
                                message: 'اسم المستخدم يحتوي فقط على ارقام وحروف'
                            }
                        }
                    },
                    s_password: {
                        message: 'كلمة المرور',
                        validators: {
                            notEmpty: {
                                message: 'هذا الحقل مطلوب'
                            },
                            stringLength: {
                                min: 6,
                                max: 50,
                                message: 'يجب ان تكون كلمة المررور اكثر من 6 حروف'
                            }
                        }
                    },
                    s_mobile_number: {
                        message: 'رقم الجوال',
                        validators: {
                            notEmpty: {
                                message: 'هذا الحقل مطلوب'
                            },
                            stringLength: {
                                min: 9,
                                max: 13,
                                message: 'يجب ان يكون الرقم من 9 رقم و حتى 13 رقم'
                            },
                            remote: {
                                url: '{!! url('admin/users/checkeuser') !!}',
                                data: function(validator) {
                                    return {
                                        s_mobile_number: validator.getFieldElements('s_mobile_number').val(),

                                    };
                                },
                                message: 'رقم الجوال موجود مسبقا'
                            }
                        }
                    },
                    s_email: {
                        validators: {
                            notEmpty: {
                                message: 'هذا الحقل مطلوب'
                            },
                            regexp: {
                                regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                                message: 'صيغة البريد الإلكتروني غير صحيحة'
                            },
                            remote: {
                                url: '{!! url('admin/users/checkemail') !!}',
                                data: function(validator) {
                                    return {
                                        s_email: validator.getFieldElements('primary_email').val(),

                                    };
                                },
                                message: 'البريد الإلكتروني موجود مسبقا'
                            }
                        }
                    },
                }
            });
            $('#add_restaurant_user_form').bootstrapValidator('resetForm');
        });

    });

    function showModal(id){
        $("html, body").animate({
            scrollTop: 0
        }, 600);

        if(id == null){
            $('#add_restaurant_user .modal-title').html('اضافة مدير لوحة ');
            $('#add_restaurant_user').modal('show', {backdrop: 'static'});
        }
        else{
            $.post('{{ url('admin/users/showeditform')}}', {'pk_i_id':id,'_token':'{!! csrf_token() !!}'} , function(data) {
                if(data.success) {
                    $('#edit_restaurant_user').html(data.page).modal('show');
                    $('#edit_restaurant_user .modal-title').html('تعديل بيانات المستخدم ');
                    validateForm();
                }else{
                    showAlertMessage('alert-danger', 'خطأ !', 'خطأ في الصفحة');

                }
            }, 'json').error(function(error){
                showAlertMessage('alert-danger', 'Fatal error !', 'An unknown error occured !');
            });
        }
    }

    function validateForm(){

        $('#edit_restaurant_user_form').bootstrapValidator({
            message: '',
            live: true,
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            },
            fields: {
                s_name: {
                    message: 'اسم المستخدم',
                    validators: {
                        notEmpty: {
                            message: 'هذا الحقل مطلوب'
                        },
                        stringLength: {
                            min: 2,
                            max: 50,
                            message: 'يجب ان يكون الاسم من حرفين و حتى خمسين حرف'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z_1-9\u0600-\u06FF\u0020]+$/,
                            message: 'اسم المستخدم يحتوي فقط على ارقام وحروف'
                        }
                    }
                },
                s_mobile_number: {
                    message: 'رقم الجوال',
                    validators: {
                        notEmpty: {
                            message: 'هذا الحقل مطلوب'
                        },
                        stringLength: {
                            min: 9,
                            max: 13,
                            message: 'يجب ان يكون الرقم من 9 رقم و حتى 13 رقم'
                        }
                    }
                },
                s_email: {
                    message: 'البريد الإلكتروني',
                    validators: {
                        email: {
                            message: 'هذا الحقل مطلوب'
                        },
                    }
                },
            }
        });

        $('#edit_restaurant_user_form').bootstrapValidator('resetForm');



        $('.edit_restaurant_user').click(function(e){
            e.preventDefault();
            postEditableData($('#edit_restaurant_user_form'));
        });
    }

    function ch_st(id){
        $.ajax({
            url : '{{url('admin/users/status')}}',
            data : {pk_i_id:id,_token : '{!! csrf_token() !!}' },
            type: "POST",
            success:function(data, textStatus, jqXHR) {
                var b_enabled = data.b_enabled;
                var str = '<span class="inc"></span><span class="check"></span><span class="box"></span>'+b_enabled;
                setTimeout(function(){$('#label_status_'+id).html(str)},1000);

            },
            error:function(data, textStatus, jqXHR) {
                console.log(data);
            } ,
            statusCode: {
                500: function(data) {
                    console.log(data);
                }
            }
        });
    }

    function ch_st2(id){
        $.ajax({
            url : '{{url('admin/users/verify')}}',
            data : {pk_i_id:id,_token : '{!! csrf_token() !!}' },
            type: "POST",
            success:function(data, textStatus, jqXHR) {
                var dt_verified_date = data.dt_verified_date;
                var str = '<span class="inc"></span><span class="check"></span><span class="box"></span>'+dt_verified_date;
                setTimeout(function(){$('#label_verify_'+id).html(str)},1000);

            },
            error:function(data, textStatus, jqXHR) {
                console.log(data);
            } ,
            statusCode: {
                500: function(data) {
                    console.log(data);
                }
            }
        });
    }

    function deleteThis(pk_i_id){
        if(!confirm('هل انت متاكد؟')){
            return false;
        }else {
            $.ajax({
                url: '{{url('admin/users/destroy')}}',
                data: {pk_i_id: pk_i_id, _token: '{!! csrf_token() !!}'},
                type: "POST",
                success: function (data, textStatus, jqXHR) {
                    var id = data.restaurant_id;
//                        console.log('#cards_table > tbody > tr[id='+id+']');
                    $('#'+pk_i_id).closest('tr').remove();
//                    $('#restaurants_table > tbody > tr[id='+id+']').remove();
                    showAlertMessage('alert-success', 'ادارة المستخدمين / ', 'تم حذف المستخدم بنجاح');
//                    setTimeout(function(){location.reload()},1000);
                },
                error: function (data, textStatus, jqXHR) {
                    console.log(data);
                },
                statusCode: {
                    500: function (data) {
                        console.log(data);
                    }
                }
            });
        }
    }

    function postData(form){
        form.data('bootstrapValidator').validate();
        if(!form.data('bootstrapValidator').isValid()){
            return;
        }
        var data = form.serializeArray();
        $.ajax({
            url : '{{ url('admin/users/add') }}',
            data : data,
            type: "POST",
            success:function(data) {
                if(data.success){
                    $('#add_restaurant_user').modal('hide');
                    $('#restaurants_user_table > tbody > tr:first').before(data.restaurant_user);
                    showAlertMessage('alert-success','مستخدمظو المطعم / ', 'تم اضافة المستخدم بنجاح');
                }else{
                    showAlertMessage('alert-danger','ادارة المستخدمين / ', ' حدث خطا أثناء الاضافة ! حاول مجددا');
                }
            },
            error:function(data) {
                console.log(data);
            } ,
            statusCode: {
                500: function(data) {
                    console.log(data);
                }
            }
        });
    }

    function postEditableData(form){
        var data = form.serializeArray();
        $.ajax({
            url : '{{ url('admin/users/edit') }}',
            data : data,
            type: "POST",
            success:function(data) {
                if(data.success){
                    $('#edit_restaurant_user').modal('hide');
                    $('#'+data.pk_i_id).closest('tr').replaceWith(data.restaurant_user_row);
                    showAlertMessage('alert-success','ادارة المستخدمين / ', 'تم تعديل بيانات المستخدم بنجاح');
                }else{
                    showAlertMessage('alert-danger','موظظفو المطعم / ', ' حدث خطا أثناء الاضافة ! حاول مجددا');
                }
            },
            error:function(data) {
                console.log(data);
            } ,
            statusCode: {
                500: function(data) {
                    console.log(data);
                }
            }
        });
    }

</script>
@endsection


@section('content')
    @include('admin.layout.breadcrumb',$data)
    <div class="modal fadeIn" id="edit_restaurant_user" data-width="760" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    </div>
    <div class="modal fadeIn" id="add_restaurant_user" data-width="800" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">اضافة / تعديل مستخدم</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-green-haze">
                                <i class="icon-settings font-green-haze"></i>
                                <span class="caption-subject bold uppercase"></span>بيانات المستخدم:</div>
                        </div>
                        {!! FORM::open(['url'=>url('admin/users/add'),'class'=>'form-horizontal','role'=>'form','id'=>'add_restaurant_user_form']) !!}
                        <div class="portlet-body form">
                            <div class="col-md-12">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-4 control-label" for="s_mobile_number">رقم الجوال</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="s_mobile_number" id="s_mobile_number" placeholder="رقم الجوال">
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-4 control-label" for="s_password">كلمة المرور</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="s_password" id="s_password" placeholder="كلمة المرور">
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-4 control-label" for="s_name">اسم المستخدم</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="s_name" id="s_name" placeholder="اسم المستخدم">
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-4 control-label" for="s_email">البريد الإلكتروني</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="s_email" id="s_email" placeholder="البريد الإلكتروني">
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            <div class="form-group form-md-line-input has-success">
                                                <label class="col-md-4 control-label" for="fk_i_role_id">النوع</label>
                                                <div class="col-md-8">
                                                    <select name="fk_i_role_id" id="fk_i_role_id" class="form-control">
                                                        <option value="2">زبون</option>
                                                        <option value="3">سائق</option>
                                                    </select>
                                                    <div class="form-control-focus">
                                                    </div>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {!! FORM::close() !!}
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn blue add_restaurant_user">حفظ التغييرات</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">اغلاق</button>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue-madison">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>ادارة مدراء لوحة التحكم
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <button onclick="showModal()" id="sample_editable_1_new" class="btn green">
                                        <i class="fa fa-plus"></i> اضافة جديد
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="restaurants_user_table">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">اسم المستخدم</th>
                            <th class="text-center">رقم الجوال</th>
                            <th class="text-center">النوع</th>
                            <th class="text-center">عدد الطلبات</th>
                            <th class="text-center">البريد الإلكتروني</th>
                            <th class="text-center">الحالة</th>
                            <th class="text-center">تفعيل التسجيل</th>
                            <th class="text-center">انشئ بتاريخ</th>
                            <th class="text-center">تعديل</th>
                            <th class="text-center">حذف</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection