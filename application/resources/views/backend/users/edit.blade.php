<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">اضافة / تعديل مستخدم</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject bold uppercase"></span>بيانات المستخدم:</div>
                </div>
                {!! FORM::open(['url'=>url('admin/users/edit'),'class'=>'form-horizontal','role'=>'form','id'=>'edit_restaurant_user_form']) !!}
                <div class="portlet-body form">
                    <div class="col-md-12">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input has-success">
                                        <label class="col-md-4 control-label" for="s_mobile_number">رقم الجوال</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{ $restaurant_user->s_mobile_number }}" name="s_mobile_number" id="s_mobile_number" placeholder="رقم الجوال">
                                            <div class="form-control-focus">
                                            </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input has-success">
                                        <label class="col-md-4 control-label" for="s_password">كلمة المرور</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="s_password" id="s_password" placeholder="كلمة المرور">
                                            <div class="form-control-focus">
                                            </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input has-success">
                                        <label class="col-md-4 control-label" for="s_name">اسم المستخدم</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{ $restaurant_user->s_name }}" name="s_name" id="s_name" placeholder="اسم المستخدم">
                                            <div class="form-control-focus">
                                            </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input has-success">
                                        <label class="col-md-4 control-label" for="s_email">البريد الإلكتروني</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{ $restaurant_user->s_email }}" name="s_email" id="s_email" placeholder="البريد الإلكتروني">
                                            <div class="form-control-focus">
                                            </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input has-success">
                                        <label class="col-md-4 control-label" for="fk_i_role_id">النوع</label>
                                        <div class="col-md-8">
                                            <select name="fk_i_role_id" id="fk_i_role_id" class="form-control">
                                                <option @if($restaurant_user->fk_i_role_id == 2) {{ 'selected' }} @endif value="2">زبون</option>
                                                <option @if($restaurant_user->fk_i_role_id == 3) {{ 'selected' }} @endif value="3">سائق</option>
                                            </select>
                                            <div class="form-control-focus">
                                            </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <input type="hidden" name="pk_i_id" value="{{ $restaurant_user->pk_i_id }}">
                {!! FORM::close() !!}
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn blue edit_restaurant_user">حفظ التغييرات</button>
    <button type="button" data-dismiss="modal" class="btn btn-default">اغلاق</button>
</div>