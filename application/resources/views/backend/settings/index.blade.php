@extends('backend.layouts.app')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                <span>{{ $data['title'] }}</span>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/'.config('app.prefix','admin')) }}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
                <li class="active">{{ $data['title'] }}</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div style="/*padding: 0 15px;*/">
                        <?php if (\Session::has('success')): ?>
                        <div class="alert alert-success">
                            <strong>نجاح !</strong>
                            {{\Session::get('success')}}
                        </div>
                        <?php endif ?>
                        <?php if (count($errors)): ?>
                        <div class="alert alert-danger">
                            <strong>خطأ !</strong>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li> {{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="col-xs-12" style="margin-top: 15px;">
                    <div class="panel panel-default">
                        <div class="panel-body form_view">

                            {{FORM::open([
                                        'files'=>'true',
                                        'class'=>"formular form-horizontal ls_form",
                                        'role'=>"form",
                                        'method'=>'post',
                                        'url'=>config('app.prefix','admin').'/settings/save'])}}

                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">الفيس بوك</label>
                                    <div class="col-md-9">
                                        <input type="text" name="facebook" value="{{ Cache::store('file')->get('settings.facebook') }}" class="form-control" placeholder="Facebook">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">تويتر</label>
                                    <div class="col-md-9">
                                        <input type="text" name="twitter" value="{{Cache::store('file')->get('settings.twitter')}}" class="form-control" placeholder="Twitter">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">سكايب</label>
                                    <div class="col-md-9">
                                        <input type="text" name="skype" value="{{Cache::store('file')->get('settings.skype')}}" class="form-control" placeholder="Skype">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">يوتيوب</label>
                                    <div class="col-md-9">
                                        <input type="text" name="youtube" value="{{Cache::store('file')->get('settings.youtube')}}" class="form-control" placeholder="Youtube">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">انستجرام</label>
                                    <div class="col-md-9">
                                        <input type="text" name="instagram" value="{{Cache::store('file')->get('settings.instagram')}}" class="form-control" placeholder="Instagram">
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">مميزات إعمار</label>
                                    <div class="col-md-9">
                                        <textarea type="text" name="e3mar_features" class="form-control" placeholder="مميزات إعمار">{{Cache::store('file')->get('settings.e3mar_features')}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">لماذا اعمار</label>
                                    <div class="col-md-9">
                                        <textarea type="text" name="why_e3mar" class="form-control" placeholder="لماذا اعمار">{{Cache::store('file')->get('settings.why_e3mar')}}</textarea>
                                    </div>
                                </div>


                                <hr>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">العنوان</label>
                                    <div class="col-md-9">
                                        <input type="text" name="address" value="{{Cache::store('file')->get('settings.address')}}" class="form-control" placeholder="Address">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">رقم الهاتف</label>
                                    <div class="col-md-9">
                                        <input type="text" name="phone" value="{{Cache::store('file')->get('settings.phone')}}" class="form-control" placeholder="phone">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">الواتس اب</label>
                                    <div class="col-md-9">
                                        <input type="text" name="whatsapp" value="{{Cache::store('file')->get('settings.whatsapp')}}" class="form-control" placeholder="Whatsapp">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">البريد الإلكتروني</label>
                                    <div class="col-md-9">
                                        <input type="text" name="email" value="{{Cache::store('file')->get('settings.email')}}" class="form-control" placeholder="E-mail">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">الرقم الضريبي</label>
                                    <div class="col-md-9">
                                        <input type="text" name="tax_number" value="{{Cache::store('file')->get('settings.tax_number')}}" class="form-control" placeholder="Tax Number">
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">رابط متجر قوقل</label>
                                    <div class="col-md-9">
                                        <input type="text" name="google_play" value="{{Cache::store('file')->get('settings.google_play')}}" class="form-control" placeholder="Google Play">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">رابط متجر ابل</label>
                                    <div class="col-md-9">
                                        <input type="text" name="app_store" value="{{Cache::store('file')->get('settings.app_store')}}" class="form-control" placeholder="Apple store">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-3 control-label">رابط موثوق</label>
                                    <div class="col-md-9">
                                        <input type="text" name="mothoq" value="{{Cache::store('file')->get('settings.mothoq')}}" class="form-control" placeholder="Mothoq">
                                    </div>
                                </div>


                            </div>

                            <div class="form-actions">

                                <div class="row">

                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn btn-primary">تعديل</button>
                                    </div>

                                </div>

                            </div>

                            {{FORM::close()}}

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection