<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            {{--<div class="pull-left image">--}}
            <div class="text-center image">
                {{--<img src="{{ asset('assets/backend/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">--}}
                <img src="{{ asset('assets/frontend/images/logo-b.png') }}" class="" alt="إعمار">
            </div>
            {{--<div class="pull-left info">--}}
                {{--<p>Alexander Pierce</p>--}}
                {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
            {{--</div>--}}
        </div>
        <!-- search form -->
        {{--<form action="#" method="get" class="sidebar-form">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
                {{--<span class="input-group-btn">--}}
                {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
                {{--</button>--}}
              {{--</span>--}}
            {{--</div>--}}
        {{--</form>--}}
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">القائمة الرئيسية</li>
            <li  @if(Request::segment(2) == '') class="active" @endif >
                <a href="{{ url('/'.config('app.prefix','admin')) }}">
                    <i class="fa fa-dashboard"></i> <span>لوحة التحكم</span>
                </a>
            </li>
            @if(\Auth::user()->can('services'))
                <li @if(Request::segment(2) == 'services') class="active" @endif >
                    <a href="{{ url(config('app.prefix','admin').'/services') }}">
                        <i class="fa fa-th"></i> <span>المهن</span>
                    </a>
                </li>
            @endif
            @if(\Auth::user()->can('pages'))
                <li @if(Request::segment(2) == 'pages') class="active" @endif >
                    <a href="{{ url(config('app.prefix','admin').'/pages') }}">
                        <i class="fa fa-edit"></i> <span>الصفحات الثابتة</span>
                    </a>
                </li>
            @endif
            <li>
                <a href="pages/mailbox/mailbox.html">
                    <i class="fa fa-bank"></i> <span>المنشأت</span>
                </a>
            </li>
            @if(\Auth::user()->can('news'))
                <li @if(Request::segment(2) == 'news') class="active" @endif >
                    <a href="{{ url(config('app.prefix','admin').'/news') }}">
                        <i class="fa fa-rss"></i> <span>الأخبار</span>
                    </a>
                </li>
            @endif

            @if(\Auth::user()->can('banners'))
                <li @if(Request::segment(2) == 'banners') class="active" @endif >
                    <a href="{{ url(config('app.prefix','admin').'/banners') }}">
                        <i class="fa fa-laptop"></i> <span>البنرات</span>
                    </a>
                </li>
            @endif
            @if(\Auth::user()->can('contact'))
            <li @if(Request::segment(2) == 'contact') class="active" @endif >
                <a href="{{ url(config('app.prefix','admin').'/contact') }}">
                    <i class="fa fa-envelope"></i> <span>رسائل تواصل معنا</span>
                </a>
            </li>
            @endif
            <li>
                <a href="pages/mailbox/mailbox.html">
                    <i class="fa fa-pie-chart"></i> <span>التصويتات</span>
                </a>
            </li>

            <li>
                <a href="pages/widgets.html">
                    <i class="fa fa-user"></i> <span>شركاؤنا</span>
                </a>
            </li>

            <li class="treeview @if(in_array(Request::segment(2),['roles','permissions','administrators','users'])) menu-open active  @endif "  >
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>إدارة المستخدمين</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(Request::segment(2) == 'roles') class="active" @endif ><a href="{{ url(config('app.prefix','admin').'/roles') }}"><i class="fa fa-circle-o"></i>المهام</a></li>
                    <li @if(Request::segment(2) == 'permissions') class="active" @endif ><a href="{{ url(config('app.prefix','admin').'/permissions') }}"><i class="fa fa-circle-o"></i> الصلاحيات</a></li>
                    <li @if(Request::segment(2) == 'administrators') class="active" @endif ><a href="{{ url(config('app.prefix','admin').'/administrators') }}"><i class="fa fa-circle-o"></i>مدراء لوحة التحكم</a></li>
                    <li @if(Request::segment(2) == 'users') class="active" @endif ><a href="{{ url(config('app.prefix','admin').'/users') }}"><i class="fa fa-circle-o"></i>المستخدمين</a></li>
                </ul>
            </li>
            @if(\Auth::user()->can('settings'))
                <li @if(Request::segment(2) == 'settings') class="active" @endif >
                    <a href="{{ url(config('app.prefix','admin').'/settings') }}">
                        <i class="fa fa-cogs"></i> <span>الإعدادت</span>
                    </a>
                </li>
            @endif

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>