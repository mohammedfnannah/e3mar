<div class="inner-header-2-div">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('assets/frontend/images/company_icon.png') }}" class="user-image pull-left" alt="">
                </a>
                <div class="user-info pull-left">
                    <p>{{ auth()->user()->name }}</p>
                    <p>{{ auth()->user()->email }}</p>
                    <a href="{{ url('company/profile') }}"><i class="fa fa-edit"></i> تعديل بياناتي </a>
                    <a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> تسجيل الخروج </a>
                </div>
            </div>
        </div>
    </div>
</div>