<a href="{{ url('company/profile') }}" @if(Request::segment(1) == 'company' && Request::segment(2) == 'profile') class="active" @endif>
    <div class="side-bar-button">
        <i class="pull-left fa fa-user"></i>
        <p class="pull-left">بياناتي</p>
    </div>
</a>

<a href="{{ url('company/current/applications') }}" @if(Request::segment(1) == 'company' && Request::segment(2) == 'current' && Request::segment(3) == 'applications') class="active" @endif>
    <div class="side-bar-button">
        <i class="pull-left fa fa-cog"></i>
        <p class="pull-left">مشاريع وأعمال قيد التنفيذ</p>
    </div>
</a>

<a href="{{ url('company/applications/archive') }}" @if(Request::segment(1) == 'company' && Request::segment(2) == 'applications' && Request::segment(3) == 'archive') class="active" @endif>
    <div class="side-bar-button">
        <i class="pull-left fa fa-briefcase"></i>
        <p class="pull-left">أرشيف المشاريع والأعمال</p>
    </div>
</a>
<a href="{{ url('company/applications') }}" @if(Request::segment(1) == 'company' && Request::segment(2) == 'applications' && Request::segment(3) == '') class="active" @endif>
    <div class="side-bar-button">
        <i class="pull-left fa fa-bell"></i>
        <p class="pull-left">طلبات جديدة</p>
        <i class="pull-left bell">{{ $new_applications }}</i>
    </div>
</a>

<a href="{{ url('company/contracts') }}" @if(Request::segment(1) == 'company' && Request::segment(2) == 'contracts') class="active" @endif>
    <div class="side-bar-button">
        <i class="pull-left fa fa-file"></i>
        <p class="pull-left">العقود المسجلة</p>
    </div>
</a>

<div class="facility-information">
    <img src="{{ asset('assets/frontend/images/company_icon.png') }}" width="100px" alt="">
    <div class="div-title text-left">
        <span>{{ auth()->user()->name }}</span>
    </div>
    <div class="facility-details-ul">
        <ul>
            <li><i class="fa fa-map-marker"></i><span>{{ $facility->city->name }} - {{ $facility->district }} - {{ $facility->street }}</span></li>
            <li><i class="fa fa-whatsapp"></i><span>{{ $facility->whatsapp }}</span></li>
            <li><i class="fa fa-mobile"></i><span>{{ auth()->user()->mobile }}</span></li>
            <li><i class="fa fa-phone"></i><span>{{ $facility->tel }}</span></li>
            <li><i class="fa fa-envelope-o"></i><span>{{ auth()->user()->email }}</span></li>
        </ul>
    </div>
</div>

<a href="{{ url('company/password') }}" @if(Request::segment(1) == 'company' && Request::segment(2) == 'password') class="active" @endif>
    <div class="side-bar-button">
        <i class="pull-left fa fa-lock"></i>
        <p class="pull-left">تغير كلمة المرور</p>
    </div>
</a>
<a href="{{ url('logout') }}">
    <div class="side-bar-button">
        <i class="pull-left fa fa-sign-out"></i>
        <p class="pull-left">تسجيل الخروج</p>
    </div>
</a>