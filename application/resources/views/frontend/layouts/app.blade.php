<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>إعمار</title>
    <link href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/bootstrap-arabic.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/frontend/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/css/lightbox.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="{{ asset('assets/frontend/js/html5shiv.js') }}"></script>
    <script src="{{ asset('assets/frontend/js/respond.min.js') }}"></script>
    <![endif]-->

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('assets/frontend/images/logo.ico') }}">

    <link href="{{ asset('assets/frontend/css/style_ar.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/frontend/owl_carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/owl_carousel/assets/owl.theme.default.min.css') }}">


    <link href="{{ asset('assets/frontend/css/responsive.css') }}" rel="stylesheet">

    @yield('css')
</head><!--/head-->

<body>

@include('frontend.layouts.header')

<div id="body">
    @yield('content')
</div>

@include('frontend.layouts.footer')


<script type="text/javascript" src="{{ asset('assets/frontend/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/frontend/js/jquery.inview.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/frontend/js/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/frontend/js/mousescroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/frontend/js/smoothscroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/frontend/js/jquery.countTo.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/frontend/js/lightbox.min.js') }}"></script>

<script src="{{ asset('assets/frontend/owl_carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/parsley.js') }}"></script>
<script src="{{ asset('assets/frontend/js/parsley-error-top.js') }}"></script>

<script>
    $(document).ready(function(){

        @if (\Session::has('success'))
            $('html, body').scrollTop( $(document).height() );
        @endif

        $(".owl-carousel").owlCarousel({
            loop:true,
            rtl:true,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            items:1,
            dots:true
        });
    });

    $(window).load(function(){

        $('.poll-vote-list li .percentage').each(function() {
            var value = $(this).text();
            $(this).closest('li').find('.nul').removeClass('inner-bar null').addClass('voted');
            $(this).closest('li').find('.inner-bar').animate({width: value}, 1000);
            $(this).closest('li').find('.voted').delay('1000').animate({width: value}, 1000);
            $('.percentage').delay('2000').fadeIn('fast');
        });

        $('body').on('click', '.results-trigger', function() {
            $(this).hide();
            $('.poll-vote-list li .percentage').each(function() {
                var value = $(this).text();
                $(this).closest('li').find('.nul').removeClass('inner-bar null').addClass('voted');
                $(this).closest('li').find('.inner-bar').animate({width: value}, 1000);
                $(this).closest('li').find('.voted').delay('1000').animate({width: value}, 1000);
                $('.percentage').delay('2000').fadeIn('fast');
            });
            return false;
        });

    });
</script>
@yield('js')

@include('frontend.layouts.script')

</body>
</html>