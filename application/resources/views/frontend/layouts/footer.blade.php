<footer id="footer">
    <div class="top-footer @if(Request::segment(1) != '') inner-footer @endif">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="footer-logo">
                        <img src="{{ asset('assets/frontend/images/footer_logo.png') }}" alt="">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="footer-links">
                        <ul>
                            <li><a href="{{ url('/') }}">الرئيسية</a></li>
                            <li><a href="{{ url('page/1') }}">من نحن</a></li>
                            <li><a href="{{ url('services') }}">المهن</a></li>
                            <li><a href="{{ url('partners') }}">أصدقاؤنا</a></li>
                            <li><a href="{{ url('consultation') }}">مستشارك</a></li>
                            <li><a href="{{ url('evaluation') }}">تقويم عمل</a></li>
                            <li><a href="{{ url('contact') }}">اتصل بنا</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="follow-us">
                        <div class="title">
                            <span>تابعونا على</span>
                        </div>
                        <ul class="follow-us-icons">
                            @if(!empty(Cache::store('file')->get('settings.youtube')))
                            <li><a href="{{Cache::store('file')->get('settings.youtube')}}"><i class="fa fa-youtube"></i></a></li>
                            @endif
                            @if(!empty(Cache::store('file')->get('settings.instagram')))
                            <li><a href="{{Cache::store('file')->get('settings.instagram')}}"><i class="fa fa-instagram"></i></a></li>
                            @endif
                            @if(!empty(Cache::store('file')->get('settings.skype')))
                            <li><a href="{{Cache::store('file')->get('settings.skype')}}"><i class="fa fa-skype"></i></a></li>
                            @endif
                            @if(!empty(Cache::store('file')->get('settings.twitter')))
                            <li><a href="{{Cache::store('file')->get('settings.twitter')}}"><i class="fa fa-twitter"></i></a></li>
                            @endif
                            @if(!empty(Cache::store('file')->get('settings.facebook')))
                            <li><a href="{{Cache::store('file')->get('settings.facebook')}}"><i class="fa fa-facebook"></i></a></li>
                            @endif

                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div class="mail-list">

                        @if (\Session::has('success') && \Session::has('type') && \Session::get('type') == 'mail_list')
                            <p class="alert-success text-success"  style="padding: 10px;">
                                {{\Session::get('success')}}
                            </p>
                        @endif
                        @if (isset($errors) && count($errors))
                            @foreach($errors->all() as $error)
                                <p class="text-danger alert-danger" style="padding: 10px;">{{$error}} </p>
                            @endforeach
                        @endif

                        <div class="title">
                            <span>اشترك بالقائمة البريدية</span>
                        </div>
                        <div class="mail-list-div">
                            {!! FORM::open(array(
                                'url' => 'mail_list/save',
                                'class'=>'form-horizontal',
                                'data-validate'=>'top_parsley',
                                'method'=>'post',
                            )) !!}
                                <input type="text" name="email" placeholder="ادخل البريد الإلكتروني"  data-trigger="change" data-type="email" data-required="true" data-error-message=" هذا الحقل مطلوب ويجب ان تكون صيغة ايمل صحيحة" >
                                <button type="submit"><i class="fa fa-send"></i></button>
                            {!! FORM::close() !!}
                        </div>
                        <div class="clearfix"></div>
                        <div>
                            <ul class="parsley-list-error"></ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="footer-ma3rof text-right">
                        <a href="{{Cache::store('file')->get('settings.mothoq')}}">
                            <img src="{{ asset('assets/frontend/images/ma3rof.png') }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <span class="footer-copywrite">© جميع الحقوق محفوظة لمؤسسة موقع إعمار للتسويق الالكتروني {{ date('Y',time()) }} </span>
                </div>
            </div>
        </div>
    </div>
</footer>