<div class="inner-header-1-div">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('assets/frontend/images/logo.png') }}" class="logo" width="165px" alt="">
                </a>
            </div>
        </div>
    </div>
</div>