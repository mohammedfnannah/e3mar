<a href="{{ url('user/profile') }}" @if(Request::segment(1) == 'user' && Request::segment(2) == 'profile') class="active" @endif>
    <div class="side-bar-button">
        <i class="pull-left fa fa-cog"></i>
        <p class="pull-left">بياناتي</p>
    </div>
</a>

<a href="{{ url('user/applications') }}" @if(Request::segment(1) == 'user' && Request::segment(2) == 'applications' && Request::segment(3) == '') class="active" @endif>
    <div class="side-bar-button">
        <i class="pull-left fa fa-list"></i>
        <p class="pull-left">طلباتي</p>
    </div>
</a>

<a href="{{ url('user/applications/new') }}" @if(Request::segment(1) == 'user' && Request::segment(2) == 'applications' && Request::segment(3) == 'new') class="active" @endif>
    <div class="side-bar-button">
        <i class="pull-left fa fa-plus"></i>
        <p class="pull-left">طلب خدمة</p>
    </div>
</a>
<a href="{{ url('user/password') }}" @if(Request::segment(1) == 'user' && Request::segment(2) == 'password') class="active" @endif>
    <div class="side-bar-button">
        <i class="pull-left fa fa-lock"></i>
        <p class="pull-left">تغير كلمة المرور</p>
    </div>
</a>
<a href="{{ url('logout') }}">
    <div class="side-bar-button">
        <i class="pull-left fa fa-sign-out"></i>
        <p class="pull-left">تسجيل الخروج</p>
    </div>
</a>