<div id="header">
    @if(!empty(Cache::store('file')->get('settings.whatsapp')) || !empty(Cache::store('file')->get('settings.email')) || !empty(Cache::store('file')->get('settings.tax_number')))
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-right">
                    <ul class="info-list">
                        @if(!empty(Cache::store('file')->get('settings.whatsapp')))
                        <li>
                            <i class="fa fa-whatsapp"></i>
                            <span>{{Cache::store('file')->get('settings.whatsapp')}}</span>
                        </li>
                        @endif
                        @if(!empty(Cache::store('file')->get('settings.email')))
                        <li>
                            <i class="fa fa-envelope"></i>
                            <span>{{Cache::store('file')->get('settings.email')}}</span>
                        </li>
                        @endif
                        @if(!empty(Cache::store('file')->get('settings.tax_number')))
                        <li>
                            <span>رقم السجل التجاري :</span>
                            <span>{{Cache::store('file')->get('settings.tax_number')}}</span>
                        </li>
                        @endif
                        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
                            @if(auth()->user()->role_id == 1)
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="{{ url('user/profile') }}"><span>حسابي</span></a>
                            </li>
                            @else
                                <li>
                                    <i class="fa fa-user"></i>
                                    <a href="{{ url('company/profile') }}"><span>حسابي</span></a>
                                </li>
                            @endif
                            <li>
                                <i class="fa fa-sign-out"></i>
                                <a href="{{ url('logout') }}"><span>تسجيل الخروج</span></a>
                            </li>
                        @else
                                <li>
                                    <i class="fa fa-sign-in"></i>
                                    <a href="{{ url('user/login') }}"><span>تسجيل الدخول</span></a>
                                </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="bottom-header">
        <div class="main-nav">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left">
                                <li @if(Request::segment(1) == '') class="active" @endif ><a href="{{ url('/') }}">الرئيسية</a></li>
                                <li @if(Request::segment(1) == 'page' && Request::segment(2) == '1') class="active" @endif ><a href="{{ url('page/1') }}">من نحن</a></li>
                                <li @if(Request::segment(1) == 'services') class="active" @endif ><a href="{{ url('services') }}">المهن</a></li>
                                <li @if(Request::segment(1) == 'partners') class="active" @endif ><a href="{{ url('partners') }}">أصدقاؤنا</a></li>
                                <li @if(Request::segment(1) == 'consultation') class="active" @endif ><a href="{{ url('consultation') }}">مستشارك</a></li>
                                <li @if(Request::segment(1) == 'evaluation') class="active" @endif ><a href="{{ url('evaluation') }}">تقويم عمل</a></li>
                                <li @if(Request::segment(1) == 'contact') class="active" @endif ><a href="{{ url('contact') }}">اتصل بنا</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="search-div">
                            <form action="" method="post">
                                <button type="submit"><i class="fa fa-search"></i></button>
                                <input type="text" placeholder="بحث ...">
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div><!--/#main-nav-->
    </div>
</div>