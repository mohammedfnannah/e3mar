@extends('frontend.layouts.app')

@section('content')

    <div id="body">

        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif


        <div class="inner-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                    </div>

                    <div class="clearfix"></div>

                    <div class="inner-content">
                        <div class="col-sm-4 side-bar-buttons">

                            @include('frontend.layouts.user_sidebar')

                        </div>
                        <div class="col-sm-8">

                            <div class="bookmark-container">
                                <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                                <ul class="bookmark-link">
                                    <li><a><span>تغير كلمة المرور</span></a></li>
                                </ul>

                            </div>

                            @if (\Session::has('success'))
                                <p class="alert-success text-success"  style="padding: 10px;">
                                    {{\Session::get('success')}}
                                </p>
                            @endif
                            @if (\Session::has('error'))
                                <p class="text-danger alert-danger"  style="padding: 10px;">
                                    {{\Session::get('error')}}
                                </p>
                            @endif
                            @if (isset($errors) && count($errors))
                                @foreach($errors->all() as $error)
                                    <p class="text-danger alert-danger" style="padding: 10px;">{{$error}} </p>
                                @endforeach
                            @endif

                            <div class="register-form">

                                {!! FORM::open(array(
                                'url' => 'user/password/save',
                                'class'=>'form-horizontal',
                                'data-validate'=>'parsley',
                                'id'=>'formID',
                                'method'=>'post',
                            )) !!}

                                <div class="input-group">
                                    <label class="pull-left"><span>كلمة المرور الحالية</span><span class="required">*</span></label>
                                    <input type="password" class="pull-left" name="old_password" id="old_password" placeholder="كلمة المرور الحالية" data-trigger="change" data-minlength="6" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>كلمة المرور الجديدة</span><span class="required">*</span></label>
                                    <input type="password" class="pull-left" name="password" id="password" placeholder="كلمة المرور الجديدة" data-trigger="change" data-minlength="6" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>تأكيد كلمة المرور</span><span class="required">*</span></label>
                                    <input type="password" class="pull-left" name="confirm_password" data-equalto="#password" placeholder="تأكيد كلمة المرور" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب ويجب ان يكون نفس كلمة المرور">
                                </div>

                                <div class="input-group">
                                    <label class="pull-left"></label>
                                    <button type="submit" class="main_button pull-left" >تسجيل</button>
                                </div>
                                {!! FORM::close() !!}

                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

@endsection
