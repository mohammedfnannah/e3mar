@extends('frontend.layouts.app')

@section('content')

    <div id="body">


        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif


        <div class="inner-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                    </div>

                    <div class="clearfix"></div>

                    <div class="inner-content">
                        <div class="col-sm-4 side-bar-buttons">

                            @include('frontend.layouts.company_sidebar')

                        </div>
                        <div class="col-sm-8">

                            <div class="bookmark-container">
                                <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                                <ul class="bookmark-link">
                                    <li><a><span>{{ $title }}</span></a></li>
                                </ul>

                            </div>

                            @if (\Session::has('success'))
                                <p class="alert-success text-success"  style="padding: 10px;">
                                    {{\Session::get('success')}}
                                </p>
                            @endif
                            @if (\Session::has('error'))
                                <p class="text-danger alert-danger"  style="padding: 10px;">
                                    {{\Session::get('error')}}
                                </p>
                            @endif
                            @if (isset($errors) && count($errors))
                                @foreach($errors->all() as $error)
                                    <p class="text-danger alert-danger" style="padding: 10px;">{{$error}} </p>
                                @endforeach
                            @endif

                            <div class="register-form">

                                <table class="table table-striped">

                                    <thead>
                                        <tr>
                                            <th width="10%">رقم العقد</th>
                                            <th width="30%">اسم صاحب العقد</th>
                                            <th width="30%">تاريخ الإضافة</th>
                                            <th width="15%">طباعه</th>
                                            <th width="15%">الغاء العقد</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(isset($contract) && !empty($contract) && count($contract->toArray()) > 0)
                                            @foreach($contract as $row)
                                                <tr>
                                                    <td>{{ $row->id }}</td>
                                                    <td>{{ $row->name }}</td>
                                                    <td>{{ $row->created_at }}</td>
                                                    <td><a href="{{ url('company/contracts/print/'.$row->id) }}" class="btn btn-info"><i class="fa fa-print"></i></a></td>
                                                    <td><a href="{{ url('company/contracts/cancel/'.$row->id) }}" class="btn btn-info"><i class="fa fa-remove"></i></a></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>

                                </table>

                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

@endsection
