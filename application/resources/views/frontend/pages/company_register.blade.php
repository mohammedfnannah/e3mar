@extends('frontend.layouts.app')

@section('css')
    <style>
        .parsley-error-list li {
            margin-right: 58% !important;
            margin-top: 10px !important;
        }
    </style>
@endsection
@section('content')

    @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
        @if(auth()->user()->role_id == 1)
            @include('frontend.layouts.user_header')
        @else
            @include('frontend.layouts.company_header')
        @endif
    @else
        @include('frontend.layouts.inner_header')
    @endif


    <div class="inner-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                </div>

                <div class="clearfix"></div>

                <div class="inner-content">
                    <div class="col-sm-12">

                        <div class="bookmark-container">
                            <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                            <ul class="bookmark-link">
                                <li><a href=""><span>تسجيل  منشأة</span></a></li>
                            </ul>

                        </div>

                        @if (\Session::has('success'))
                            <p class="alert-success text-success"  style="padding: 10px;">
                                {{\Session::get('success')}}
                            </p>
                        @endif
                        @if (isset($errors) && count($errors))
                            @foreach($errors->all() as $error)
                                <p class="text-danger alert-danger" style="padding: 10px;">{{$error}} </p>
                            @endforeach
                        @endif

                        {!! FORM::open(array(
                                'url' => 'register/company/save',
                                'class'=>'form-horizontal',
                                'data-validate'=>'parsley',
                                'id'=>'formID',
                                'files'=> true,
                                'method'=>'post',
                            )) !!}

                            <div class="register-form  facility-register-form">
                                <i class="fa fa-home heading-icon"></i>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>مكان المنشأة</span></label>
                                    <div class="select-continer pull-left">
                                        <select name="city_id" >
                                            @if(isset($data['cites']) && !empty($data['cites']) && count($data['cites']->toArray()) > 0)
                                                @foreach($data['cites'] as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <img src="{{ asset('assets/frontend/images/select.png') }}" alt="">
                                    </div>
                                </div>
                                {{--<div class="input-group">--}}
                                    {{--<p class="pull-left"></p>--}}
                                    {{--<label class="pull-left"><span>نوع المنشأة</span></label>--}}
                                    {{--<div class="select-continer pull-left">--}}
                                        {{--<select name="service_id" >--}}
                                            {{--@if(isset($data['services']) && !empty($data['services']) && count($data['services']->toArray()) > 0)--}}
                                                {{--@foreach($data['services'] as $row)--}}
                                                    {{--<option value="{{ $row->id }}">{{ $row->name }}</option>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</select>--}}
                                        {{--<img src="{{ asset('assets/frontend/images/select.png') }}" alt="">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>نوع المنشأة</span><span class="required">*</span></label>
                                    <input type="text" class="pull-left" name="sub_title" placeholder="نوع المنشأة" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>اسم المنشأة</span><span class="required">*</span></label>
                                    <input type="text" class="pull-left" name="name" placeholder="اسم المنشأة" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>رقم السجل التجاري</span><span class="required">*</span></label>
                                    <input type="text" class="pull-left" name="commercial_number" placeholder="رقم السجل التجاري" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>صورة المنشأة</span></label>
                                    <input type="file" class="pull-left" name="image" >
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span></span></label>
                                    <span class="pull-left note">الابعاد المفضلة للصورة 800px عرض * 500px طول</span>
                                </div>
                            </div>

                            <div class="register-form  facility-register-form">
                                <i class="fa fa-mobile heading-icon"></i>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>الهاتف المتنقل</span><span class="required">*</span></label>
                                    <input type="text" class="pull-left" name="mobile" placeholder="الهاتف المتنقل" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب">
                                    <label class="pull-left"><span>رقم اخر</span></label>
                                    <input type="text" class="pull-left" name="other_mobile" placeholder="">
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>الهاتف الثابت</span></label>
                                    <input type="text" class="pull-left" name="tel" placeholder="الهاتف الثابت">
                                    <label class="pull-left"><span>رقم اخر</span></label>
                                    <input type="text" class="pull-left" name="other_tel" placeholder="">
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>الفاكس</span></label>
                                    <input type="text" class="pull-left" name="fax" placeholder="الفاكس">
                                </div>
                            </div>

                            <div class="register-form  facility-register-form">
                                <i class="fa fa-envelope heading-icon"></i>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>الرمز البريدي</span></label>
                                    <input type="text" class="pull-left" name="postal_code" placeholder="الرمز البريدي">
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>صندوق البريد</span></label>
                                    <input type="text" class="pull-left" name="mail_box" placeholder="الرمز البريدي">
                                </div>
                            </div>

                            <div class="register-form  facility-register-form">
                                <i class="fa fa-map-marker heading-icon"></i>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>اسم الحي</span></label>
                                    <input type="text" class="pull-left" name="district" placeholder="اسم الحي">
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>اسم الشارع</span></label>
                                    <input type="text" class="pull-left" name="street" placeholder="اسم الشارع">
                                </div>
                            </div>

                            <div class="register-form  facility-register-form">
                                <i class="fa fa-share-alt heading-icon"></i>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>واتس اب</span></label>
                                    <div class="input-with-icon pull-left">
                                        <input type="text" class="pull-left" name="whatsapp" placeholder="واتس اب">
                                        <i class="fa fa-whatsapp"></i>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>تويتر</span></label>
                                    <div class="input-with-icon pull-left">
                                        <input type="text" class="pull-left" name="twitter" placeholder="تويتر">
                                        <i class="fa fa-twitter"></i>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>انستجرام</span></label>
                                    <div class="input-with-icon pull-left">
                                        <input type="text" class="pull-left" name="instagram" placeholder="انستجرام">
                                        <i class="fa fa-instagram"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="register-form  facility-register-form">
                                <i class="fa fa-institution heading-icon"></i>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>مشروعات سابقة</span></label>
                                    <textarea name="projects"  cols="30" rows="10" class="pull-left"></textarea>
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span></span></label>
                                    <span class="pull-left note">وصف المشروع  باقصاه 500 كلمة</span>
                                </div>
                            </div>




                            <div class="register-form  facility-register-form">
                                <i class="fa fa-briefcase heading-icon"></i>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>المهنة</span></label>
                                    <div class="select-continer pull-left">
                                        <select name="service_id" >
                                            @if(isset($data['services']) && !empty($data['services']) && count($data['services']->toArray()) > 0)
                                                @foreach($data['services'] as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <img src="{{ asset('assets/frontend/images/select.png') }}" alt="">
                                    </div>
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>الموقع</span></label>
                                    <div class="input-with-icon pull-left">
                                        <input type="text" class="pull-left" name="location" placeholder="الموقع">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>أوقات العمل</span></label>
                                    <div class="input-with-icon pull-left">
                                        <input type="text" class="pull-left" name="work_time" placeholder="أوقات العمل">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>ساعات العمل</span></label>
                                    <div class="input-with-icon pull-left">
                                        <input type="text" class="pull-left" name="work_day" placeholder="ساعات العمل">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="register-form  facility-register-form">
                                <i class="fa fa-legal heading-icon"></i>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>هل ترغب في تقويم عمل ؟</span></label>
                                    <input type="radio" name="is_calender_work" checked value="1">  نعم
                                    <input type="radio" name="is_calender_work" value="0">  لا
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>هل ترغب بتقديم استشارات خاصة ؟</span></label>
                                    <input type="radio" name="is_consulting" checked value="1">   نعم
                                    <input type="radio" name="is_consulting" value="0">  لا
                                </div>
                            </div>

                            <div class="register-form  facility-register-form">
                                <i class="fa fa-user heading-icon"></i>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>البريد الإلكتروني</span><span class="required">*</span></label>
                                    <input type="email" class="pull-left" name="email" placeholder="البريد الإلكتروني" data-trigger="change" data-required="true"  data-type="email" data-error-message=" هذا الحقل مطلوب ويجب ان تكون صيغة ايمل صحيحة">
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span class="main-color-color2">كلمة المرور</span><span class="required">*</span></label>
                                    <input type="password" class="pull-left" name="password" id="password" placeholder="كلمة المرور" data-trigger="change" data-minlength="6" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"><span>اعادة كلمة المرور</span><span class="required">*</span></label>
                                    <input type="password" class="pull-left" name="confirm_password" data-equalto="#password" placeholder="اعادة كلمة المرور" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب ويجب ان يكون نفس كلمة المرور">
                                </div>
                            </div>


                            <div class="register-form  facility-register-form">
                                <div class="input-group">
                                    <p class="pull-left"></p>
                                    <label class="pull-left"></label>
                                    <button type="submit" class="main_button pull-left" >تسجيل</button>
                                </div>
                            </div>

                        {!! FORM::close() !!}

                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
