@extends('frontend.layouts.app')

@section('content')
    <div id="body">

        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif




        <div class="services">
            <div class="container">
                <div class="row">

                    <div class="col-sm-12">
                        <div class="bookmark-container">
                            <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                            <ul class="bookmark-link">
                                <li><a href="{{ url('/') }}"><span>الرئيسية</span><span> / </span></a></li>
                                <li><span>المهن</span></li>
                            </ul>

                        </div>
                    </div>

                    @if(isset($services) && !empty($services) && count($services->toArray()) > 0)
                    <div class="services-container">
                        @foreach($services as $row)
                            <div class="col-sm-2">
                                <a href="{{ url('services/'.$row->id) }}">
                                    <div class="one-service">
                                        <img src="{{ asset('uploads/services/'.$row->image) }}" alt="">
                                        <p>{{ $row->name }}</p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>

            <div class="clearfix"></div>
            <div  class="text-center">
                {{ $services->links() }}
            </div>

        </div>


</div>




@endsection
