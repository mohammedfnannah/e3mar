@extends('frontend.layouts.app')

@section('content')

    <div id="body">

        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif


            <div class="inner-container">
                <div class="container">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="bookmark-container">
                                <img src="images/arrowleft-b.png" class="arrow-left" alt="">

                                <ul class="bookmark-link pull-left">
                                    <li><a href="{{ url('/') }}"><span>الرئيسية</span><span> / </span></a></li>
                                    <li><a href="{{ url('services/'.$facility->service->id) }}"><span>{{ $facility->service->name }}</span><span> / </span></a></li>
                                    <li><span>{{ $facility->name }}</span></li>
                                </ul>

                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="inner-content">

                            <div class="col-sm-4">
                                <div class="div-title">
                                    <span>منشآت ذات صلة</span>
                                </div>

                                <div class="one-facility-container">

                                    @if(isset($facilities) && !empty($facilities) && count($facilities->toArray()) > 0)
                                        @foreach($facilities as $row)
                                            <div class="one-facility">
                                                <div class="facility-img">
                                                    <a href="{{ url('facility/'.$row->id) }}">
                                                        <img src="{{ asset('uploads/facility/'.$row->image) }}" alt="{{ $row->name }}">
                                                    </a>
                                                </div>
                                                <div class="facility-title">
                                                    <a href="{{ url('facility/'.$row->id) }}">
                                                        <span>{{ $row->name }}</span>
                                                    </a>
                                                </div>
                                                <div class="facility-details">
                                                    <div class="pull-right">
                                                        <i class="fa fa-map-marker"></i>
                                                        <span>{{ $row->city->name }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <p class="text-danger alert-danger"  style="padding: 10px;">عذرا لا يوجد منشآت ذات صلة</p>
                                    @endif


                                </div>

                            </div>

                            <div class="col-sm-8">
                                <div class="div-title">
                                    <span>{{ $facility->name }}</span>
                                </div>
                                <div class="news-date">
                                    <span>{{ $facility->sub_title }}</span>
                                </div>
                                <div class="news-date">
                                    <i class="fa fa-map-marker"></i>
                                    <span>{{ $facility->city->name }}</span>
                                </div>
                                <div class="news-dis">
                                    <p>{{ $facility->projects }}</p>
                                </div>

                                <div class="div-title">
                                    <span>معلومات عن المنشأة</span>
                                </div>
                                <div class="facility-details-ul">
                                    <ul>
                                        <li><i class="fa fa-map-marker"></i><span>{{ $facility->city->name }} - {{ $facility->district }} - {{ $facility->street }}</span></li>
                                        <li><i class="fa fa-whatsapp"></i><span>{{ $facility->whatsapp }}</span></li>
                                        <li><i class="fa fa-mobile"></i><span>{{ $facility->user->mobile }}</span></li>
                                        <li><i class="fa fa-envelope-o"></i><span>{{ $facility->user->email }}</span></li>
                                    </ul>
                                </div>

                                {{--<div class="div-title">--}}
                                    {{--<span>أعمال المنشأة</span>--}}
                                {{--</div>--}}

                                {{--<div class="facility-works">--}}
                                    {{--<div class="owl-carousel owl-works owl-theme">--}}
                                        {{--<div class="item">--}}
                                            {{--<a class="example-image-link" data-lightbox="example-1" href="images/news1.png">--}}
                                                {{--<img src="images/news1.png" alt="">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="item">--}}
                                            {{--<a class="example-image-link" data-lightbox="example-1" href="images/news2.png">--}}
                                                {{--<img src="images/news2.png" alt="">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="item">--}}
                                            {{--<a class="example-image-link" data-lightbox="example-1" href="images/news3.png">--}}
                                                {{--<img src="images/news3.png" alt="">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="item">--}}
                                            {{--<a class="example-image-link" data-lightbox="example-1" href="images/news2.png">--}}
                                                {{--<img src="images/news2.png" alt="">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="item">--}}
                                            {{--<a class="example-image-link" data-lightbox="example-1" href="images/news1.png">--}}
                                                {{--<img src="images/news1.png" alt="">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="item">--}}
                                            {{--<a class="example-image-link" data-lightbox="example-1" href="images/news3.png">--}}
                                                {{--<img src="images/news3.png" alt="">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}


                            </div>

                            {{--<div class="col-sm-3">--}}
                                {{--<div class="div-title">--}}
                                    {{--<span>أخبار المنشأة</span>--}}
                                {{--</div>--}}

                                {{--<div class="one-facility-container one-facility-news-container">--}}
                                    {{--<div class="one-facility">--}}
                                        {{--<div class="facility-img">--}}
                                            {{--<a href="">--}}
                                                {{--<img src="images/news1.png" alt="">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="facility-title">--}}
                                            {{--<a href="">--}}
                                                {{--<span>ابراج الملك فهد الحديثة</span>--}}
                                            {{--</a>--}}
                                            {{--<div class="clearfix"></div>--}}
                                            {{--<div class="pull-left">--}}
                                                {{--<i class="fa fa-calendar"></i>--}}
                                                {{--<span>15 ابريل 2017</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="one-facility">--}}
                                        {{--<div class="facility-img">--}}
                                            {{--<a href="">--}}
                                                {{--<img src="images/news1.png" alt="">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="facility-title">--}}
                                            {{--<a href="">--}}
                                                {{--<span>ابراج الملك فهد الحديثة</span>--}}
                                            {{--</a>--}}
                                            {{--<div class="clearfix"></div>--}}
                                            {{--<div class="pull-left">--}}
                                                {{--<i class="fa fa-calendar"></i>--}}
                                                {{--<span>15 ابريل 2017</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="one-facility">--}}
                                        {{--<div class="facility-img">--}}
                                            {{--<a href="">--}}
                                                {{--<img src="images/news1.png" alt="">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="facility-title">--}}
                                            {{--<a href="">--}}
                                                {{--<span>ابراج الملك فهد الحديثة</span>--}}
                                            {{--</a>--}}
                                            {{--<div class="clearfix"></div>--}}
                                            {{--<div class="pull-left">--}}
                                                {{--<i class="fa fa-calendar"></i>--}}
                                                {{--<span>15 ابريل 2017</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            {{--</div>--}}

                        </div>


                    </div>
                </div>
            </div>

        </div>
@endsection
