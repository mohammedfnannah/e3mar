@extends('frontend.layouts.app')

@section('content')

    <div id="body">


        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif


        <div class="inner-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                    </div>

                    <div class="clearfix"></div>

                    <div class="inner-content">
                        <div class="col-sm-4 side-bar-buttons">

                            @include('frontend.layouts.user_sidebar')

                        </div>
                        <div class="col-sm-8">

                            <div class="bookmark-container">
                                <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                                <ul class="bookmark-link">
                                    <li><a><span>طلباتي</span></a></li>
                                </ul>

                            </div>

                            @if (\Session::has('success'))
                                <p class="alert-success text-success"  style="padding: 10px;">
                                    {{\Session::get('success')}}
                                </p>
                            @endif
                            @if (\Session::has('error'))
                                <p class="text-danger alert-danger"  style="padding: 10px;">
                                    {{\Session::get('error')}}
                                </p>
                            @endif
                            @if (isset($errors) && count($errors))
                                @foreach($errors->all() as $error)
                                    <p class="text-danger alert-danger" style="padding: 10px;">{{$error}} </p>
                                @endforeach
                            @endif

                            <div class="register-form">

                                <table class="table table-striped">

                                    <thead>
                                        <tr>
                                            <th width="10%">رقم الطلب</th>
                                            <th width="30%">العنوان</th>
                                            <th width="40%">التفاصيل</th>
                                            <th width="10%">الحالة</th>
                                            <th width="10%">حذف</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(isset($applications) && !empty($applications) && count($applications->toArray()) > 0)
                                            @foreach($applications as $row)
                                                <tr>
                                                    <td>{{ $row->id }}</td>
                                                    <td>{{ $row->address }}</td>
                                                    <td>{{ substr($row->text,0,30) }}</td>
                                                    <td>
                                                    @if($row->status == 0)
                                                        <p class="btn btn-warning">قيد الإنتظار</p>
                                                    @elseif($row->status == 1)
                                                        <a class="btn btn-info">جاري التنفيذ</a>
                                                    @elseif($row->status == 2)
                                                        <a class="btn btn-success">منتهي</a>
                                                    @elseif($row->status == 3)
                                                        <a class="btn btn-danger">مرفوض</a>
                                                    @endif
                                                    </td>
                                                    <td><a href="{{ url('user/applications/delete/'.$row->id) }}" class="btn btn-danger"><i class="fa fa-remove"></i></a></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>

                                </table>

                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

@endsection
