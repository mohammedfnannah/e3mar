@extends('frontend.layouts.app')

@section('content')

    @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
        @if(auth()->user()->role_id == 1)
            @include('frontend.layouts.user_header')
        @else
            @include('frontend.layouts.company_header')
        @endif
    @else
        @include('frontend.layouts.inner_header')
    @endif

    <div class="inner-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="bookmark-container">
                        <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                        <ul class="bookmark-link">
                            <li><a href=""><span>الرئيسية</span><span> / </span></a></li>
                            <li><span>اتصل بنا</span></li>
                        </ul>

                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="inner-content">

                    <div class="col-sm-7">

                        @if (\Session::has('success'))
                        <p class="alert-success text-success"  style="padding: 10px;">
                            {{\Session::get('success')}}
                        </p>
                        @endif
                        @if (isset($errors) && count($errors))
                            @foreach($errors->all() as $error)
                                <p class="text-danger alert-danger" style="padding: 10px;">{{$error}} </p>
                            @endforeach
                        @endif

                        <div class="register-form register-user-form">
                            {!! FORM::open(array(
                                'url' => 'contact/save',
                                'class'=>'form-horizontal',
                                'data-validate'=>'parsley',
                                'id'=>'formID',
                                'method'=>'post',
                            )) !!}

                                <div class="input-group">
                                    <label class="pull-left"><span>الاسم</span><span class="required">*</span></label>
                                    <input type="text" class="pull-left" name="name" placeholder="الاسم" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>رقم الجوال</span></label>
                                    <input type="text" class="pull-left" name="mobile" placeholder="رقم الجوال">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>البريد الإلكتروني</span><span class="required">*</span></label>
                                    <input type="email" class="pull-left" name="email" placeholder="البريد الإلكتروني" data-trigger="change" data-required="true" data-type="email" data-error-message=" هذا الحقل مطلوب ويجب ان تكون صيغة ايمل صحيحة">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>الموضوع</span><span class="required">*</span></label>
                                    <input type="text" class="pull-left" name="subject" placeholder="الموضوع" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>الرسالة</span><span class="required">*</span></label>
                                    <textarea name="message" style="height: 90px;" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب"></textarea>
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"></label>
                                    <button type="submit" class="main_button pull-left" >ارسال</button>
                                </div>
                            {!! FORM::close() !!}
                        </div>

                    </div>
                    <div class="col-sm-5">
                        <div class="contact-form-info">
                            <h3 class="title">بيانات التواصل</h3>
                            <ul>
                                @if(!empty(Cache::store('file')->get('settings.address')))
                                    <li>
                                        <i class="fa fa-map-marker"></i>
                                        <span>{{Cache::store('file')->get('settings.address')}}</span>
                                    </li>
                                @endif
                                @if(!empty(Cache::store('file')->get('settings.email')))
                                    <li>
                                        <i class="fa fa-envelope"></i>
                                        <span>{{Cache::store('file')->get('settings.email')}}</span>
                                    </li>
                                @endif
                                @if(!empty(Cache::store('file')->get('settings.phone')))
                                    <li>
                                        <i class="fa fa-phone"></i>
                                        <span>{{Cache::store('file')->get('settings.phone')}}</span>
                                    </li>
                                @endif
                                @if(!empty(Cache::store('file')->get('settings.whatsapp')))
                                    <li>
                                        <i class="fa fa-whatsapp"></i>
                                        <span>{{Cache::store('file')->get('settings.whatsapp')}}</span>
                                    </li>
                                @endif
                            </ul>

                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
@endsection
