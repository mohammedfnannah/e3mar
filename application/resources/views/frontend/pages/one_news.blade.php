@extends('frontend.layouts.app')

@section('content')

    <div id="body">

        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif

        <div class="inner-container">
            <div class="container">
                <div class="row">

                    <div class="col-sm-12">
                        <div class="bookmark-container">
                            <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                            <ul class="bookmark-link">
                                <li><a href="{{ url('/') }}"><span>الرئيسية</span><span> / </span></a></li>
                                <li><a href="{{ url('news') }}"><span>الأخبار</span><span> / </span></a></li>
                                <li><span>{{ $one_news->name }}</span></li>
                            </ul>

                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="inner-content">
                        <div class="col-sm-4">
                            <div class="div-title">
                                <span>أخبار ذات صلة</span>
                            </div>

                            <div class="one-news-container">

                                @if(isset($news) && !empty($news) && count($news->toArray()) > 0)
                                    @foreach($news as $row)

                                        <div class="one-news">
                                            <div class="news-img">
                                                <a href="{{ url('news/'.$row->id) }}">
                                                    <img src="{{ asset('uploads/news/'.$row->image) }}" alt="">
                                                </a>
                                            </div>
                                            <div class="news-title">
                                                <a href="{{ url('news/'.$row->id) }}">
                                                    <span>{{ $row->name }}</span>
                                                </a>
                                            </div>
                                            <div class="news-details">
                                                <div class="pull-left">
                                                    <i class="fa fa-calendar"></i>
                                                    <span>{{ date('Y M d',strtotime($row->created_at))}}</span>
                                                </div>
                                                <a href="{{ url('news/'.$row->id) }}">
                                                    <div class="pull-right">
                                                        <span>اقرأ المزيد</span>
                                                        <i class="fa fa-search"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                            {{--<div class="div-title" style="margin-top: 30px">--}}
                                {{--<span>منشآت ذات صلة</span>--}}
                            {{--</div>--}}

                            {{--<div class="facilities">--}}

                                {{--<div class="one-facility">--}}
                                    {{--<a href="">--}}
                                        {{--<div class="facility-img">--}}
                                            {{--<img src="images/instuation1.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="facility-title">اسم المنشأة</div>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                                {{--<div class="one-facility">--}}
                                    {{--<a href="">--}}
                                        {{--<div class="facility-img">--}}
                                            {{--<img src="images/instuation2.png" alt="">--}}
                                        {{--</div>--}}
                                        {{--<div class="facility-title">اسم المنشأة</div>--}}
                                    {{--</a>--}}
                                {{--</div>--}}

                            {{--</div>--}}
                        </div>

                        <div class="col-sm-5">
                            <div class="div-title">
                                <span>{{ $one_news->name }}</span>
                            </div>
                            <div class="news-date">
                                <i class="fa fa-calendar"></i>
                                <span>{{ date('Y M d',strtotime($one_news->created_at))}}</span>
                            </div>
                            <div class="news-dis">
                                <p>{!! $one_news->text !!}</p>
                            </div>


                        </div>

                        <div class="col-sm-3" style="margin-bottom: 60px;">
                            <div class="one-news-images">
                                <div class="one-news-main-image">
                                    <a class="example-image-link" data-lightbox="example-1" href="{{ asset('uploads/news/'.$one_news->image) }}">
                                        <img src="{{ asset('uploads/news/'.$one_news->image) }}" alt="">
                                    </a>
                                </div>
                                {{--<div class="clearfix"></div>--}}
                                {{--<div class="one-news-image pull-left">--}}
                                    {{--<a class="example-image-link" data-lightbox="example-1" href="images/news2.png">--}}
                                        {{--<img src="images/news2.png" alt="">--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                                {{--<div class="one-news-image pull-right">--}}
                                    {{--<a class="example-image-link" data-lightbox="example-1" href="images/news3.png">--}}
                                        {{--<img src="images/news3.png" alt="">--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        </div>
    </div>


@endsection
