@extends('frontend.layouts.app')

@section('content')
    <div id="body">

        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif


        <div class="inner-container">
            <div class="container">
                <div class="row">

                    <div class="col-sm-12">
                        <div class="bookmark-container">
                            <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                            <ul class="bookmark-link">
                                <li><a href="{{ url('/') }}"><span>الرئيسية</span><span> / </span></a></li>
                                <li><span>الأخبار</span></li>
                            </ul>

                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="inner-content">

                        <div class="one-facility-container one-facility-news-container">

                            @if(isset($data['news']) && !empty($data['news']) && count($data['news']->toArray()) > 0)
                                @foreach($data['news'] as $row)
                                    <div class="col-sm-3">
                                        <div class="one-facility">
                                            <div class="facility-img">
                                                <a href="{{ url('news/'.$row->id) }}">
                                                    <img src="{{ asset('uploads/news/'.$row->image) }}" alt="">
                                                </a>
                                            </div>
                                            <div class="facility-title">
                                                <a href="{{ url('news/'.$row->id) }}">
                                                    <span>{{ $row->name }}</span>
                                                </a>
                                                <div class="clearfix"></div>
                                                <div class="pull-left">
                                                    <i class="fa fa-calendar"></i>
                                                    <span>{{ date('Y M d',strtotime($row->created_at))}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <p class="text-danger alert-danger"  style="padding: 10px;">عذرا لا يوجد أخبار</p>
                            @endif
                        </div>

                        <div class="clearfix"></div>
                        <div  class="text-center">
                            {{ $data['news']->links() }}
                        </div>

                    </div>


                </div>
            </div>
        </div>

    </div>

@endsection
