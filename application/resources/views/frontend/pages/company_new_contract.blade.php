@extends('frontend.layouts.app')

@section('css')
    <style>
        .tips ,.tips *{
            font-size: 14px !important;
        }
        .tips ol:nth-child(3),.tips ol:nth-child(5){
            margin-right: 50px;
        }
        .register-form-info{
            border-right: 0 !important;
        }
        .input-group a.btn{
            margin: 5px 15px;
        }
        .work_price .input-group a.btn{
            margin: 5px 0px;
        }
        .work_price .input-group input{
            width: 31.5% !important;
            margin-left: 15px;
        }
    </style>
@endsection
@section('js')
    <script>
        $( document ).ready(function() {
            $('body').on('click','#add_condition',function(){
                $('.condition_container').append($('.condition_template').html());
            });
            $('body').on('click','.remove_condition',function(){
                $(this).parent().remove();
            });

            $('body').on('click','#add_work_price',function(){
                $('.work_price_container').append($('.work_price_template').html());
            });
            $('body').on('click','.remove_work_price',function(){
                $(this).parent().remove();
            });
        });
    </script>
@endsection
@section('content')

    <div id="body">


        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif


        <div class="inner-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                    </div>

                    <div class="clearfix"></div>

                    <div class="inner-content">
                        <div class="col-sm-4 side-bar-buttons">

                            @include('frontend.layouts.company_sidebar')

                        </div>
                        <div class="col-sm-8">

                            <div class="bookmark-container">
                                <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                                <ul class="bookmark-link">
                                    <li><a><span>{{ $title }}</span></a></li>
                                </ul>

                            </div>

                            @if (\Session::has('success'))
                                <p class="alert-success text-success"  style="padding: 10px;">
                                    {{\Session::get('success')}}
                                </p>
                            @endif
                            @if (isset($errors) && count($errors))
                                @foreach($errors->all() as $error)
                                    <p class="text-danger alert-danger" style="padding: 10px;">{{$error}} </p>
                                @endforeach
                            @endif

                            <div class="register-form-info">

                                <h3 class="title">إرشادات</h3>
                                <p><h4 class="logo-footer"></h4>
                                <div class="tips">
                                    <div><span id="docs-internal-guid-550e9296-7252-4581-42bc-e016c1c1e57a"><br><ol style="margin-top:0pt;margin-bottom:0pt;"><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">لا تقم بترقيم الشروط والأحكام أو بيانات العمل لأن ذلك يظهر تلقائيا أثناء طباعة العقد.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">البيانات التي تكتبها في هذا النموذج ستظهر مع كل عقد لاختصار الوقت عليك مع إمكانية تعديلك لبيانات النموذج لاحقا أو تعديل البيانات مع كل عقد جديد.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height:1.3800000000000001;margin-top:0pt;margin-bottom:0pt;text-align: justify;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">تذكر بأن عملاءك سيقومون بتقييمك وسيظهر هذا التقييم للمشاهدين وسيتمكن عميلك من تقييمك إذا أنهيت العقد وكان سبب إنهائه هو انتهاء العمل.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">أنت الذي تملك صلاحية إنهاء العقد بالدخول على العقد والضغط على أيقونة (إنهاء العقد) واختيار سبب إنهاء العقد:</span></p></li></ol><ol style="margin-top:0pt;margin-bottom:0pt;"><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"> انتهاء العمل.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">خلاف مالي.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">خلاف في مدة العمل.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">خلاف في إتقان العمل.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">خلاف في شروط العقد.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">خلاف في بيانات العقد.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">إلغاء هذا العقد وتدوين عقد جديد.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">غير ذلك.</span></p></li></ol><ol style="margin-top:0pt;margin-bottom:0pt;" start="5"><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">وسيكون التقييم على خمس نجوم للنقاط التالية:</span></p></li></ol><ol style="margin-top:0pt;margin-bottom:0pt;"><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">جودة العمل.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">حسن التعامل.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">مناسبة السعر.</span></p></li><li dir="rtl" style="list-style-type: decimal; font-size: 15pt; font-family: ae_AlMohanad; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre;"><p dir="rtl" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 10pt;"><span style="font-size: 15pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">أداء العمل في الوقت المحدد.</span></p></li></ol></span></div></p>

                                    <hr>
                                </div>


                            </div>

                            <div class="register-form">

                                {!! FORM::open(array(
                                'url' => 'company/contracts/new',
                                'class'=>'form-horizontal',
                                'data-validate'=>'parsley',
                                'id'=>'formID',
                                'method'=>'post',
                            )) !!}

                                <input type="hidden" name="facility_id" value="{{ auth()->user()->id }}">

                                <div class="input-group">
                                    <label class="pull-left"><span>المدينة</span><span class="required">*</span></label>
                                    <div class="select-continer pull-left">
                                        <select name="city_id" class="pull-left" id="city_id" data-trigger="change" data-notnull="true" data-required="true" data-error-message="هذا الحقل مطلوب">
                                            <option value="">-- الرجاء اختيار المدينة --</option>
                                            @if(isset($data['cites']) && !empty($data['cites']) && count($data['cites']->toArray()) > 0)
                                                @foreach($data['cites'] as $row)
                                                    <option @if($application->city_id == $row->id) selected @endif value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <img src="{{ asset('assets/frontend/images/select.png') }}" alt="">
                                    </div>
                                </div>


                                <input type="hidden" name="user_id" value="{{ $application->user_id }}">

                                <div class="input-group">
                                    <label class="pull-left"><span>الاسم</span><span class="required">*</span></label>
                                    <input type="text" class="pull-left" value="{{ $application->User->name }}" name="name" placeholder="الاسم" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>رقم الجوال</span><span class="required">*</span></label>
                                    <input type="text" class="pull-left" value="{{ $application->User->mobile }}"  name="mobile" placeholder="رقم الجوال" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب" >
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>رقم الهوية</span></label>
                                    <input type="text" class="pull-left" value=""  name="identification" placeholder="رقم الهوية" >
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>البريد الإلكتروني</span></label>
                                    <input type="email" class="pull-left" value="{{ $application->User->email }}"  name="email" placeholder="البريد الإلكتروني" data-trigger="change" data-required="true"  data-type="email" data-error-message=" هذا الحقل مطلوب ويجب ان تكون صيغة ايمل صحيحة">
                                </div>

                                <div class="input-group">
                                    <label class="pull-left">المطلوب من الطرف الاول </label>
                                    <textarea name="work" id="text" cols="30" rows="10" class="pull-left" placeholder="المطلوب من الطرف الاول">{{ $application->text }}</textarea>
                                </div>

                                <div class="input-group">
                                    <label class="pull-left"><span>الحي</span></label>
                                    <input type="text" class="pull-left" value=""  name="district" placeholder="الحي" >
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>الموقع</span></label>
                                    <input type="text" class="pull-left" value=""  name="location" placeholder="الموقع" >
                                </div>


                                <div class="input-group">
                                    <label class="pull-left"><span>الشروط والأحكام</span></label>
                                    <input type="text" class="pull-left" value=""  name="condition[]" placeholder="شرط" >
                                    <a class="btn btn-info" id="add_condition"><i class="fa fa-plus"></i></a>
                                </div>

                                <div class="condition_template" style="display: none;">
                                    <div class="input-group">
                                        <label class="pull-left"><span></span></label>
                                        <input type="text" class="pull-left" value=""  name="condition[]" placeholder="شرط" >
                                        <a class="btn btn-danger remove_condition"><i class="fa fa-remove"></i></a>
                                    </div>
                                </div>
                                <div class="condition_container"></div>

                                <div class="work_price">
                                    <div class="input-group">
                                        <label class="pull-left"><span>بيانات العمل والأسعار</span></label>
                                        <input type="text" class="pull-left" value=""  name="works[]" placeholder="العمل" >
                                        <input type="text" class="pull-left" value=""  name="price[]" placeholder="السعر" >
                                        <a class="btn btn-info pull-left" id="add_work_price"><i class="fa fa-plus"></i></a>
                                    </div>

                                    <div class="work_price_template" style="display: none;">
                                        <div class="input-group">
                                            <label class="pull-left"><span></span></label>
                                            <input type="text" class="pull-left" value=""  name="works[]" placeholder="العمل" >
                                            <input type="text" class="pull-left" value=""  name="price[]" placeholder="السعر" >
                                            <a class="btn btn-danger remove_condition"><i class="fa fa-remove"></i></a>
                                        </div>
                                    </div>
                                    <div class="work_price_container"></div>
                                </div>


                                <div class="input-group">
                                    <label class="pull-left"><span>تاريخ البداية</span></label>
                                    <input type="text" class="pull-left" value=""  name="start_date" placeholder="تاريخ البداية" >
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>تاريخ النهاية</span></label>
                                    <input type="text" class="pull-left" value=""  name="end_date" placeholder="تاريخ النهاية" >
                                </div>

                                <div class="input-group">
                                    <label class="pull-left"><span>اسم الموظف المختص</span></label>
                                    <input type="text" class="pull-left" value=""  name="employee_name" placeholder="اسم الموظف المختص" >
                                </div>

                                <div class="input-group">
                                    <label class="pull-left"><span>جوال الموظف المختص</span></label>
                                    <input type="text" class="pull-left" value=""  name="employee_mobile" placeholder="جوال الموظف المختص" >
                                </div>

                                <div class="input-group">
                                    <label class="pull-left"></label>
                                    <button type="submit" class="main_button pull-left" >تسجيل</button>
                                </div>
                                {!! FORM::close() !!}

                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

@endsection
