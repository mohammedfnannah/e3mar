@extends('frontend.layouts.app')

@section('js')
    <script>
        $( document ).ready(function() {
            $('body').on('change','#city_id',function(){
                if(parseInt($(this).val()) > 0 ){
                    var city_id = parseInt($(this).val());
                    var service_id = parseInt($('.service_id').val());
                    window.location.href = '{{ url('/') }}/services/'+service_id+'/'+city_id;
                }
            });
        });
    </script>
@endsection
@section('content')
    <div id="body">

        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif

        <div class="inner-container">
            <div class="container">
                <div class="row">

                    <div class="col-sm-12">
                        <div class="bookmark-container">
                            <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                            <ul class="bookmark-link pull-left">
                                <li><a href="{{ url('/') }}"><span>الرئيسية</span><span> / </span></a></li>
                                <li><span>{{ $service->name }}</span></li>
                            </ul>

                            <div class="select-continer pull-right">
                                <select name="" id="city_id">
                                    <option value="">اختر المدينة ...</option>
                                    @if(isset($cites) && !empty($cites) && count($cites->toArray()))
                                        @foreach($cites as $row)
                                            <option @if(Request::segment(3) == $row->id) selected @endif() value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <input type="hidden" class="service_id" value="{{ Request::segment(2) }}">
                                <img src="{{ asset('assets/frontend/images/select.png') }}" alt="">
                            </div>

                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="inner-content">

                        <div class="one-partners-container">
                            @if(isset($facilities) && !empty($facilities) && $facilities->toArray()['total'] > 0 )
                                @foreach($facilities as $row)
                                    <div class="col-sm-4">
                                        <div class="one-facility">
                                            <div class="facility-img">
                                                <img src="{{ asset('uploads/facility/'.$row->image) }}" alt="{{ $row->name }}">
                                                {{--<a class="example-image-link" data-lightbox="example-1" href="{{ asset('uploads/facility/'.$row->image) }}">--}}
                                                <a href="{{ url('facility/'.$row->id) }}">
                                                    <div class="facility-img-hover">
                                                        <i class="fa fa-search-plus"></i>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="facility-title">
                                                <a href="{{ url('facility/'.$row->id) }}">
                                                    <span>{{ $row->name }}</span>
                                                </a>
                                                <div class="clearfix"></div>
                                                <div class="pull-left">
                                                    <span>{{ $row->sub_title }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <p class="text-danger alert-danger"  style="padding: 10px;">عذرا لا يوجد منشآت</p>
                            @endif


                                <div class="clearfix"></div>
                                <div  class="text-center">
                                    {{ $facilities->links() }}
                                </div>

                        </div>

                    </div>


                </div>
            </div>
        </div>

    </div>
@endsection
