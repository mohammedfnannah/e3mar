@extends('frontend.layouts.app')

@section('content')

    @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
        @if(auth()->user()->role_id == 1)
            @include('frontend.layouts.user_header')
        @else
            @include('frontend.layouts.company_header')
        @endif
    @else
        @include('frontend.layouts.inner_header')
    @endif

    <div class="inner-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="bookmark-container">
                        <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                        <ul class="bookmark-link">
                            <li><a href="{{ url('/') }}"><span>الرئيسية</span><span> / </span></a></li>
                            <li><span>تسجيل أفراد</span></li>
                        </ul>

                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="inner-content">

                    <div class="col-sm-6">

                            @if (\Session::has('success'))
                                <p class="alert-success text-success"  style="padding: 10px;">
                                    {{\Session::get('success')}}
                                </p>
                            @endif
                            @if (isset($errors) && count($errors))
                                @foreach($errors->all() as $error)
                                    <p class="text-danger alert-danger" style="padding: 10px;">{{$error}} </p>
                                @endforeach
                            @endif

                        <div class="register-form register-user-form">
                            {!! FORM::open(array(
                                'url' => 'register/user/save',
                                'class'=>'form-horizontal',
                                'data-validate'=>'parsley',
                                'id'=>'formID',
                                'method'=>'post',
                            )) !!}

                                <div class="input-group">
                                    <label class="pull-left"><span>الاسم</span><span class="required">*</span></label>
                                    <input type="text" class="pull-left" name="name" placeholder="الاسم" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>رقم الجوال</span><span class="required">*</span></label>
                                    <input type="text" class="pull-left" name="mobile" placeholder="رقم الجوال" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب" >
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>البريد الإلكتروني</span><span class="required">*</span></label>
                                    <input type="email" class="pull-left" name="email" placeholder="البريد الإلكتروني" data-trigger="change" data-required="true"  data-type="email" data-error-message=" هذا الحقل مطلوب ويجب ان تكون صيغة ايمل صحيحة">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>كلمة المرور</span><span class="required">*</span></label>
                                    <input type="password" class="pull-left" name="password" id="password" placeholder="كلمة المرور" data-trigger="change" data-minlength="6" data-required="true" data-error-message="هذا الحقل مطلوب">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"><span>اعادة كلمة المرور</span><span class="required">*</span></label>
                                    <input type="password" class="pull-left" name="confirm_password" data-equalto="#password" placeholder="اعادة كلمة المرور" data-trigger="change" data-required="true" data-error-message="هذا الحقل مطلوب ويجب ان يكون نفس كلمة المرور">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left"></label>
                                    <button type="submit" class="main_button pull-left" >تسجيل</button>
                                </div>
                            {!! FORM::close() !!}
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="register-form-info">

                            <h3 class="title">مميزات إعمار</h3>
                            <p>{{Cache::store('file')->get('settings.e3mar_features')}}</p>

                            <h3 class="title">لماذا إعمار</h3>
                            <p>{{Cache::store('file')->get('settings.why_e3mar')}}</p>

                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
@endsection
