@extends('frontend.layouts.app')

@section('content')
    <div id="body">

        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif

        <div class="inner-container">
            <div class="container">
                <div class="row">

                    <div class="col-sm-12">
                        <div class="bookmark-container">
                            <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                            <ul class="bookmark-link">
                                <li><a href="{{ url('/') }}"><span>الرئيسية</span><span> / </span></a></li>
                                <li><span>أصدقاؤنا</span></li>
                            </ul>

                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="inner-content">


                        <div class="one-partners-container">

                            @if(isset($partners) && !empty($partners) && count($partners->toArray()) > 0)
                                @foreach($partners as $row)
                                    <div class="col-sm-4">
                                        <div class="one-facility">
                                            <div class="facility-img">
                                                <a href="{{ url('partners/'.$row->id) }}">
                                                    <img src="{{ asset('uploads/partners/'.$row->image) }}" alt="">
                                                </a>

                                                <div class="facility-price">
                                                    <div class="price-name pull-right">
                                                        <span>نسبة</span>
                                                        <div class="clearfix"></div>
                                                        <span>خصم</span>
                                                    </div>
                                                    <span class="new-price pull-right">%{{ $row->discount }}</span>
                                                    {{--<span class="old-price pull-right">300</span>--}}
                                                </div>

                                            </div>

                                            <div class="facility-title">
                                                <a href="{{ url('partners/'.$row->id) }}">
                                                    <span>{{ $row->name }}</span>
                                                </a>
                                                <div class="clearfix"></div>
                                                <div class="pull-left">
                                                    <span>{{ $row->product }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            @endif

                        </div>


                        <div class="clearfix"></div>
                        <div  class="text-center">
                            {{ $partners->links() }}
                        </div>

                    </div>


                </div>
            </div>
        </div>

    </div>

@endsection
