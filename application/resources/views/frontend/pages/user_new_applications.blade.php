@extends('frontend.layouts.app')

@section('content')

    <div id="body">


        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif


        <div class="inner-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                    </div>

                    <div class="clearfix"></div>

                    <div class="inner-content">
                        <div class="col-sm-4 side-bar-buttons">

                            @include('frontend.layouts.user_sidebar')

                        </div>
                        <div class="col-sm-8">

                            <div class="bookmark-container">
                                <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                                <ul class="bookmark-link">
                                    <li><a><span>طلب خدمة</span></a></li>
                                </ul>

                            </div>

                            @if (\Session::has('success'))
                                <p class="alert-success text-success"  style="padding: 10px;">
                                    {{\Session::get('success')}}
                                </p>
                            @endif
                            @if (isset($errors) && count($errors))
                                @foreach($errors->all() as $error)
                                    <p class="text-danger alert-danger" style="padding: 10px;">{{$error}} </p>
                                @endforeach
                            @endif

                            <div class="register-form">

                                {!! FORM::open(array(
                                'url' => 'user/applications/save',
                                'class'=>'form-horizontal',
                                'data-validate'=>'parsley',
                                'id'=>'formID',
                                'method'=>'post',
                            )) !!}

                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                                <div class="input-group">
                                    <label class="pull-left"><span>المدينة</span><span class="required">*</span></label>
                                    <div class="select-continer pull-left">
                                        <select name="city_id"  class="pull-left" id="city_id" data-trigger="change" data-notnull="true" data-required="true" data-error-message="هذا الحقل مطلوب">
                                            <option value="">-- الرجاء اختيار المدينة --</option>
                                            @if(isset($data['cites']) && !empty($data['cites']) && count($data['cites']->toArray()) > 0)
                                                @foreach($data['cites'] as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <img src="{{ asset('assets/frontend/images/select.png') }}" alt="">
                                    </div>
                                </div>


                                <div class="input-group">
                                    <label class="pull-left"><span>المهنة</span><span class="required">*</span></label>
                                    <div class="select-continer pull-left">
                                        <select name="service_id"  class="pull-left" id="city_id" data-trigger="change" data-notnull="true" data-required="true" data-error-message="هذا الحقل مطلوب">
                                            <option value="">-- الرجاء اختيار المهنة --</option>
                                            @if(isset($data['services']) && !empty($data['services']) && count($data['services1']->toArray()) > 0)
                                                @foreach($data['services'] as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <img src="{{ asset('assets/frontend/images/select.png') }}" alt="">
                                    </div>
                                </div>



                                <div class="input-group">
                                    <label class="pull-left">العنوان </label>
                                    <input type="text" class="pull-left" name="address" placeholder="عنوان المستخدم هنا">
                                </div>
                                <div class="input-group">
                                    <label class="pull-left">تفاصيل الطلب </label>
                                    <textarea name="text" id="text" cols="30" rows="10" class="pull-left" placeholder="التفاصيل"></textarea>
                                </div>

                                <div class="input-group">
                                    <label class="pull-left"></label>
                                    <button type="submit" class="main_button pull-left" >تسجيل</button>
                                </div>
                                {!! FORM::close() !!}

                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>

@endsection
