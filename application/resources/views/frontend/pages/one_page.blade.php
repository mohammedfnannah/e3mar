@extends('frontend.layouts.app')

@section('content')
    <div id="body">

        @if(isset(auth()->user()->id) && !empty(auth()->user()->id) && intval(auth()->user()->id) > 0)
            @if(auth()->user()->role_id == 1)
                @include('frontend.layouts.user_header')
            @else
                @include('frontend.layouts.company_header')
            @endif
        @else
            @include('frontend.layouts.inner_header')
        @endif

        <div class="inner-container">
            <div class="container">
                <div class="row">

                    <div class="col-sm-12">
                        <div class="bookmark-container">
                            <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">

                            <ul class="bookmark-link">
                                <li><a href="{{ url('/') }}"><span>الرئيسية</span><span> / </span></a></li>
                                <li><span>{{ $page->name }}</span></li>
                            </ul>

                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="inner-content">
                        <div class="col-sm-8">
                            <div class="div-title">
                                <span>{{ $page->name }}</span>
                            </div>
                            <div class="news-dis">
                                <p>{!! $page->text  !!}</p>
                            </div>


                        </div>

                        <div class="col-sm-4" style="margin-bottom: 60px;">
                            <div class="one-news-images">
                                <div class="one-news-main-image">
                                    <a class="example-image-link" data-lightbox="example-1" href="{{ asset('uploads/pages/'.$page->image) }}">
                                        <img src="{{ asset('uploads/pages/'.$page->image) }}" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        </div>
    </div>

@endsection
