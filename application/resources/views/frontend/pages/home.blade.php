@extends('frontend.layouts.app')

@section('content')
    <div class="logo-register-div">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <a href="">
                        <img src="{{ asset('assets/frontend/images/logo.png') }}" class="logo" alt="">
                    </a>
                    <div class="clearfix"></div>
                    <h2 class="logo-title">إعمار يختصر لك المشوار</h2>
                    <div class="clearfix"></div>
                    <div class="buttons">
                        <a href="{{ url('register/company') }}" class="btn btn-default" >تسجيل منشأة</a>
                        <a href="{{ url('register/user') }}" class="btn btn-default" >تسجيل أفراد</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(isset($data['services']) && !empty($data['services']) && count($data['services']->toArray()) > 0)
    <div class="services">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 block-title">
                    <div class="title-icon pull-left">
                        <i class="fa fa-th"></i>
                    </div>
                    <div class="title-text pull-left">
                        <img src="{{ asset('assets/frontend/images/arrowleft.png') }}" class="arrow-left" alt="">
                        <span>المهن</span>
                    </div>
                </div>

                <div class="services-container">
                    @foreach($data['services'] as $row)
                        <div class="col-sm-2">
                            <a href="{{ url('services/'.$row->id) }}">
                                <div class="one-service">
                                    <img src="{{ asset('uploads/services/'.$row->image) }}" alt="">
                                    <p>{{ $row->name }}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(isset($data['news']) && !empty($data['news']) && count($data['news']->toArray()) > 0)
    <div class="news">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 block-title">
                    <div class="title-icon pull-left">
                        <i class="fa fa-rss"></i>
                    </div>
                    <div class="title-text pull-left">
                        <img src="{{ asset('assets/frontend/images/arrowleft.png') }}" class="arrow-left" alt="">
                        <span>أخبار إعمار</span>
                    </div>
                </div>
                <div class="news-container">
                    @foreach($data['news'] as $row)
                        <div class="col-sm-3">
                            <div class="one-news">
                                <div class="news-img">
                                    <a href="{{ url('news/'.$row->id) }}">
                                        <img src="{{ asset('uploads/news/'.$row->image) }}" alt="">
                                    </a>
                                </div>
                                <div class="news-title">
                                    <a href="{{ url('news/'.$row->id) }}">
                                        <span>{{ $row->name }}</span>
                                    </a>
                                </div>
                                <div class="news-details">
                                    <div class="pull-left">
                                        <i class="fa fa-calendar"></i>
                                        <span>{{ date('Y M d',strtotime($row->created_at))}}</span>
                                    </div>
                                    <a href="{{ url('news/'.$row->id) }}">
                                        <div class="pull-right">
                                            <span>اقرأ المزيد</span>
                                            <i class="fa fa-search"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-sm-12 text-center">
                    <div class="buttons">
                        <a href="{{ url('news') }}" class="btn btn-default" >المزيد من الأخبار</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(isset($data['banners']) && !empty($data['banners']) && count($data['banners']->toArray()) > 0)
    <div class="banners">
        <div class="container">
            <div class="row">
                <div class="banners-containers">
                    @foreach($data['banners'] as $row)

                        <div class="col-sm-4">
                            <div class="one-banner">
                                <div class="item">
                                    <a href="{{ $row->link }}">
                                        <img src="{{ asset('uploads/banners/'.$row->image) }}" alt="">
                                        <div class="banner-title" style="right: 30px;">
                                            <span>{{ $row->name }}</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="appstore">
        <div class="container">
            <div class="row">
                <div class="appstore-containers">

                    <div class="col-sm-3">
                        <p class="app-name-logo"> تطبيق <img src="{{ asset('assets/frontend/images/logo-b.png') }}" alt=""></p>
                        <p class="app-available">متوفر على متجــري:</p>
                        <div class="text-center store-links">
                            @if(!empty(Cache::store('file')->get('settings.google_play')))
                                <a href="{{Cache::store('file')->get('settings.google_play')}}" class="" ><img src="{{ asset('assets/frontend/images/goggle-play.png') }}" alt=""></a>
                            @endif
                            @if(!empty(Cache::store('file')->get('settings.app_store')))
                                <a href="{{Cache::store('file')->get('settings.app_store')}}" class="" ><img src="{{ asset('assets/frontend/images/app-store.png') }}" alt=""></a>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-5">

                        <div class="app-mobile pull-left">
                            <img src="{{ asset('assets/frontend/images/appstore_mobile.png') }}" alt="">
                        </div>
                        <div class="app-screenshot pull-left">
                            <img src="{{ asset('assets/frontend/images/appstore_screenshot2.png') }}" alt="">
                        </div>

                    </div>

                    <div class="col-sm-4">
                        @if(isset($data['polls']) && !empty($data['polls']))
                        <div class="poll">

                            <div class="block-title block-title-reflex">
                                <div class="title-icon pull-left">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="title-text pull-left">
                                    <img src="{{ asset('assets/frontend/images/arrowleft-b.png') }}" class="arrow-left" alt="">
                                    <span>تصويت</span>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-sm-12 poll-block poll-title">
                                <span>{{ $data['polls']->question }}</span>
                                <input type="hidden" name="poll_id" value="{{ $data['polls']->id }}">
                                <ul class="poll-vote-list">
                                    <li class="clearfix">
                                        <input id="poll-choice1" type="radio" name="answer" value="1">
                                        <label for="poll-choice1">{{ $data['polls']->answer_1 }}</label>
                                        <div class="out-bar">
                                            <div class="inner-bar"></div>
                                        </div>
                                        <span class="percentage">{{ intval((intval($data['polls_answer'][1])/intval($data['polls_answer']['count']))*100) }}%</span>
                                    </li>
                                    <li class="clearfix">
                                        <input id="poll-choice2" type="radio" name="answer" value="2">
                                        <label for="poll-choice2">{{ $data['polls']->answer_2 }}</label>
                                        <div class="out-bar">
                                            <div class="inner-bar"></div>
                                        </div>
                                        <span class="percentage">{{ intval((intval($data['polls_answer'][2])/intval($data['polls_answer']['count']))*100) }}%</span>
                                    </li>
                                    <li class="clearfix">
                                        <input id="poll-choice3" type="radio" name="answer" value="3">
                                        <label for="poll-choice3">{{ $data['polls']->answer_3 }}</label>
                                        <div class="out-bar">
                                            <div class="inner-bar"></div>
                                        </div>
                                        <span class="percentage">{{ intval((intval($data['polls_answer'][3])/intval($data['polls_answer']['count']))*100) }}%</span>
                                    </li>
                                </ul>

                                <div class="vote-button clearfix text-right">
                                    <a class="results-trigger btn btn-custom btn btn-primary" href="#"><span>تصويت</span></a>
                                </div>
                            </div>


                        </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
